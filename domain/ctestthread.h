/***************************************************************************
                          ctestthread.h  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CTESTTHREAD_H
#define CTESTTHREAD_H

#include "rfw/utils/cthread.h"

class CTestThread :
    public utils::CThread
{
    /// this class can not be copied
    UNCOPYABLE(CTestThread);

public:
    CTestThread();

protected:
    virtual void* Run(void * args);

private:
    void process(std::vector<std::string>& strlist);
    void processVolumeMessage(std::vector<std::string>& strlist);
    void processChannel(std::vector<std::string>& strlist);
    void processPower(std::vector<std::string>& strlist);
    bool isNumber(const std::string& in, unsigned& number);
};

#endif // CTESTTHREAD_H
