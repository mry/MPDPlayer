/***************************************************************************
                          powerswitch.h  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef POWERSWITCH_H
#define POWERSWITCH_H

#include "rfw/utils/ctypes.h"
#include "extensions/cpowerids.h"
#include "drivers/outputdevice.h"

#include <map>

class PowerSwitch
{
public:
    PowerSwitch() = default;
    ~PowerSwitch() = default;

    bool addSwitch(ENPowerSwitch index, utils::uint32_t pin);
    boost::shared_ptr<Driver::OutputDevice> getDevice(ENPowerSwitch);

private:
    std::map<ENPowerSwitch, boost::shared_ptr<Driver::OutputDevice>> m_switchlist;
};

#endif // POWERSWITCH_H
