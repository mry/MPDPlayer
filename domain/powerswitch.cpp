/***************************************************************************
                          powerswitch.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "powerswitch.h"

/***********************************************************************//**
 @method : addSwitch
 @comment: add a switch, indexed by index. Pinout: pin
 @param  : index: index for referencing
 @param  : pin: gpio pin
 @return : true ok
 ***************************************************************************/
bool PowerSwitch::addSwitch(ENPowerSwitch index, utils::uint32_t pin)
{
    auto iter = m_switchlist.find(index);
    if (iter != m_switchlist.end()) {
        return false;
    }

    boost::shared_ptr<Driver::OutputDevice> device =
            boost::shared_ptr<Driver::OutputDevice>(new Driver::OutputDevice(pin));
    m_switchlist.insert(std::pair<ENPowerSwitch, boost::shared_ptr<Driver::OutputDevice>>(index,device));
    return true;
}

/***********************************************************************//**
 @method : getDevice
 @comment: get device indexed by index, or empty, if not found
 @param  : index: index for referencing
 @return : pointer to outputdevice or null, if not found
 ***************************************************************************/
boost::shared_ptr<Driver::OutputDevice> PowerSwitch::getDevice(ENPowerSwitch index)
{
    auto iter = m_switchlist.find(index);
    if (iter != m_switchlist.end()) {
        auto output = (*iter).second;
        return output;
    }

    // not found ...
    boost::shared_ptr<Driver::OutputDevice> devEmpty;
    return devEmpty;
}
