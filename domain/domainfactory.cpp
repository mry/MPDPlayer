/***************************************************************************
                          domainfactory.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "domainfactory.h"

#include "audioselector.h"
#include "volumeselector.h"
#include "powerswitch.h"

#include "rfw/utils/clogger.h"
#include "rfw/utils/cmessage.h"
#include "rfw/utils/cthreadmanager.h"
#include "rfw/utils/ceventmessage.h"
#include "rfw/utils/ctimer.h"

/***********************************************************************//**
 @method : DomainFactory
 @comment: constructor
 ***************************************************************************/
DomainFactory::DomainFactory():
    m_AudioSelector(nullptr),
    m_VolumeSelector(nullptr),
    m_PowerSwitch(nullptr)
{
}

/***********************************************************************//**
 @method : ~DomainFactory
 @comment: destructor
 ***************************************************************************/
DomainFactory::~DomainFactory()
{
    // needed for std::unique_ptr??? wtf???
}

/***********************************************************************//**
 @method : getAudioSelector
 @comment: get selector
 @return : pointer to AudioSelector
 ***************************************************************************/
AudioSelector* DomainFactory::getAudioSelector()
{
    if (!m_AudioSelector.get()) {
        initialize();
    }
    return m_AudioSelector.get();
}

/***********************************************************************//**
 @method : getVolumeSelector
 @comment: get Volume selector
 @return : pointer to VolumeSelector
 ***************************************************************************/
VolumeSelector* DomainFactory::getVolumeSelector()
{
    if (!m_VolumeSelector.get()) {
        initialize();
    }
    return m_VolumeSelector.get();
}

/***********************************************************************//**
 @method : getPowerSwitch
 @comment: get power switch
 @return : pointer to PowerSwitch
 ***************************************************************************/
PowerSwitch* DomainFactory::getPowerSwitch()
{
    if (!m_PowerSwitch.get()) {
        initialize();
    }
    return m_PowerSwitch.get();
}

/***********************************************************************//**
 @method : initialize
 @comment: setup aggregators
 ***************************************************************************/
void DomainFactory::initialize()
{
    std::unique_lock<std::mutex> lock(m_mutex);
    if (!m_AudioSelector.get()) {
        m_AudioSelector = std::unique_ptr<AudioSelector> (new AudioSelector);

        // TODO PIN assignment
        m_AudioSelector->addOutput(66);
        m_AudioSelector->addOutput(67);
        m_AudioSelector->addOutput(69);
        m_AudioSelector->addOutput(68);
        m_AudioSelector->addOutput(45);
    }

    if (!m_VolumeSelector.get()) {
        m_VolumeSelector = std::unique_ptr<VolumeSelector>(new VolumeSelector);
        m_VolumeSelector->initialize(0); // SPI 0
    }

    if (!m_PowerSwitch.get()) {
        m_PowerSwitch = std::unique_ptr<PowerSwitch>(new PowerSwitch);

        // TODO PIN assignment
        m_PowerSwitch->addSwitch(ENPowerSwitch::PS_PREAMP,26);
        m_PowerSwitch->addSwitch(ENPowerSwitch::PS_AMP,47);
        m_PowerSwitch->addSwitch(ENPowerSwitch::PS_DISPLAY,44);
        m_PowerSwitch->addSwitch(ENPowerSwitch::PS_OUT1,61);
        m_PowerSwitch->addSwitch(ENPowerSwitch::PS_OUT2,65);
        m_PowerSwitch->addSwitch(ENPowerSwitch::PS_OUT3,27);
        m_PowerSwitch->addSwitch(ENPowerSwitch::PS_OUT4,46);
    }
}
