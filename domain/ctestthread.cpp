/***************************************************************************
                          ctestthread.cpp  -  description
                             -------------------
    begin                : Thu Sep 08 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ctestthread.h"

#include "extensions/threadids.h"
#include "extensions/cvolumemessage.h"
#include "extensions/cselectormessage.h"
#include "extensions/cpowermessage.h"

#include <vector>
#include <iostream>

#include <boost/algorithm/string.hpp>
#include "rfw/utils/clogger.h"

enum class ENPowerContext :
        utils::uint32_t
{
    PWR_UNDEF = 0,
    PWR_ON    = 1,
    PWR_OFF   = 2,
};

CTestThread::CTestThread() :
    utils::CThread(threadIds::MPDTest)
{
}

void* CTestThread::Run(void * args)
{
    std::vector<std::string> strlist;
    std::string data;
    while (IsRunning()) {

        strlist.clear();
        std::getline(std::cin, data);

        boost::split(strlist, data, boost::is_any_of("\t "));

        process(strlist);
    }
    return nullptr;
}

void CTestThread::process(std::vector<std::string>& strlist)
{
    if (strlist.size() == 0) {
        ERR("Oops, size is:" << strlist.size());
        return;
    }

    static const std::string VOLUME = "VOLUME";
    static const std::string CHANNEL = "CHANNEL";
    static const std::string POWER = "POWER";

    if ( (VOLUME.compare(strlist.at(0).c_str())==0) &&
      (strlist.size() >= 2) ) {
        processVolumeMessage(strlist);
    } else if ( (CHANNEL.compare(strlist.at(0).c_str())==0) &&
      (strlist.size() >= 1) ) {
        processChannel(strlist);
    } else if (POWER.compare(strlist.at(0).c_str())==0) {
        processPower(strlist);
    }
}

void CTestThread::processVolumeMessage(std::vector<std::string>& strlist)
{
    INFO("Volume Left:" << strlist.at(1) << " Right:" << strlist.at(2));
    unsigned left = stoul(strlist.at(1));
    unsigned right = stoul(strlist.at(2));
    SMART_PTR(CVolumeMessage) pMsg = utils::CREATE_SMART_PTR(CVolumeMessage);
    pMsg->setVolume(left & 0xff, right & 0xff);
    SendTo(threadIds::MPDMain, pMsg);
}

void CTestThread::processChannel(std::vector<std::string>& strlist)
{
    INFO("Channel:" << strlist.at(1));
    unsigned channel = stoul(strlist.at(1));
    SMART_PTR(CSelectorMessage) pMsg = utils::CREATE_SMART_PTR(CSelectorMessage);
    pMsg->setSelector(channel & 0xff);
    SendTo(threadIds::MPDMain, pMsg);
}

void CTestThread::processPower(std::vector<std::string>& strlist)
{
    ENPowerContext context = ENPowerContext::PWR_UNDEF;
    static const std::string ON = "ON";

    std::vector<ENPowerSwitch> on;
    std::vector<ENPowerSwitch> off;
    for (utils::uint32_t i = 1; i < strlist.size(); ++i) {
        // check, set context
        if (ON.compare(strlist.at(i))==0) {
            context = ENPowerContext::PWR_ON;
            INFO ("Set Context ON");
            continue;
        }

        if (context == ENPowerContext::PWR_UNDEF) {
            continue;
        }

        unsigned number = 0;
        if (!isNumber (strlist.at(i), number)) {
            continue;
        }

        if (number <= static_cast<unsigned int>(ENPowerSwitch::PS_PREAMP)) {
            number = static_cast<unsigned int>(ENPowerSwitch::PS_PREAMP);
        }
        if (number >= static_cast<unsigned int>(ENPowerSwitch::PS_OUT4)) {
            number = static_cast<unsigned int>(ENPowerSwitch::PS_OUT4);
        }

        if (context == ENPowerContext::PWR_ON) {
            on.push_back(static_cast<ENPowerSwitch>(number));
        }

        INFO("at: " << i << " " << strlist.at(i));
    }
    SMART_PTR(CPowerMessage) pMsg = utils::CREATE_SMART_PTR(CPowerMessage);
    pMsg->setOn(on);
    SendTo(threadIds::MPDMain, pMsg);
}

bool CTestThread::isNumber (const std::string& in, unsigned& number)
{
    bool isNum = !in.empty() && std::find_if(in.begin(),
            in.end(), [](char c) { return !std::isdigit(c); }) == in.end();
    if (isNum) {
        number = stoul(in);
    }
    return isNum;
}
