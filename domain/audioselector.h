/***************************************************************************
                          audioselector.h  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef AUDIOSELECTOR_H
#define AUDIOSELECTOR_H

#include "rfw/utils/ctypes.h"

#include <vector>
#include <boost/shared_ptr.hpp>

#include "drivers/outputdevice.h"

class AudioSelector
{
public:
    AudioSelector();
    ~AudioSelector() = default;

    void addOutput(utils::uint32_t pin);
    utils::uint8_t getNumberOfInputs() const;
    utils::uint8_t getActiveInput() const;
    void setActiveInput(utils::uint8_t input);

private:
    utils::uint8_t m_inputDevice;
    std::vector<boost::shared_ptr<Driver::OutputDevice> > m_deviceList;
};

#endif // AUDIOSELECTOR_H
