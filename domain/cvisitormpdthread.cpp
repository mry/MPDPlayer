/***************************************************************************
                          cvisitormpdthread.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cvisitormpdthread.h"
#include "impdcontroller.h"
#include "extensions/cpowermessage.h"
#include "extensions/cselectormessage.h"
#include "extensions/cvolumemessage.h"

/***********************************************************************//**
 @method : CVisitorMpdThread
 @comment: constructor
 @param  :  pController IMPDController pointer
 ***************************************************************************/
CVisitorMpdThread::CVisitorMpdThread(IMPDController* pController) :
    m_pController(pController)
{
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void CVisitorMpdThread::visitPowerMessage       (CPowerMessage        * pMsg)
{
    m_pController->setPower(pMsg->getOn());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void CVisitorMpdThread::visitSelectorMessage    (CSelectorMessage     * pMsg)
{
    m_pController->setAudioSelector(pMsg->getSelector());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void CVisitorMpdThread::visitVolumeMessage      (CVolumeMessage       * pMsg)
{
    m_pController->setVolume(pMsg->getLeft(),pMsg->getRight());
}
