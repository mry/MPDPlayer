/***************************************************************************
                          mpdthread.h - description
                          -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MPDTHREAD_H
#define MPDTHREAD_H

#include "rfw/utils/cthread.h"

#include "impdcontroller.h"
#include "domainfactory.h"
#include "extensions/cpowerids.h"

class MPDThread :
    public IMPDController,
    public utils::CThread
{
    /// this class can not be copied
    UNCOPYABLE(MPDThread);

public:
    MPDThread();

    virtual void setVolume(utils::uint8_t left,utils::uint8_t right);
    virtual void setPower(std::vector<ENPowerSwitch> on);
    virtual void setAudioSelector(utils::uint8_t input);

protected:
  virtual void* Run(void * args);
  DomainFactory m_factory;

};

#endif // MPDTHREAD_H
