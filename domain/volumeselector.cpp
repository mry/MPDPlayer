/***************************************************************************
                          volumeselector.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "volumeselector.h"
#include "rfw/utils/clogger.h"

#if defined (X86HACK)
#include <alsa/asoundlib.h>
#endif

/***********************************************************************//**
 @method : VolumeSelector
 @comment: constructor
 @return : -
 ***************************************************************************/
VolumeSelector::VolumeSelector() :
    m_left(0),
    m_right(0)
{
}

/***********************************************************************//**
 @method : initialize
 @comment: initialize the volume selector
 @param  : spiIndex index of spi 0 or 1
 @return : -
 ***************************************************************************/
void VolumeSelector::initialize(uint8_t spiIndex)
{
    std::string spi;
    if (spiIndex == 0) {
        spi = "/dev/spidev1.0";
    } else {
        spi = "/dev/spidev2.0";
    }
    m_cspi = std::unique_ptr<Driver::CSPI> (new Driver::CSPI(90000, spi));
    m_cspi->setMode(3);
    m_cspi->setBPW(8);
}

/***********************************************************************//**
 @method : setVolume
 @comment: set the volume value for left and right channel
 @return : -
 ***************************************************************************/
void VolumeSelector::setVolume(utils::uint8_t left, utils::uint8_t right)
{

#if defined (X86HACK)
    // for x86 only
    m_left = left;
    m_right = right;
    long volume = m_left*100/255;
    long min, max;
    snd_mixer_t *handle;
    snd_mixer_selem_id_t *sid;
    const char *card = "default";
    const char *selem_name = "Master";
    snd_mixer_open(&handle, 0);
    snd_mixer_attach(handle, card);
    snd_mixer_selem_register(handle, NULL, NULL);
    snd_mixer_load(handle);
    snd_mixer_selem_id_alloca(&sid);
    snd_mixer_selem_id_set_index(sid, 0);
    snd_mixer_selem_id_set_name(sid, selem_name);
    snd_mixer_elem_t* elem = snd_mixer_find_selem(handle, sid);
    snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
    snd_mixer_selem_set_playback_volume_all(elem, volume * max / 100);
    //snd_mixer_selem_id_free(sid);
    free(elem);
    //snd_mixer_close(handle);
#else
    if (!m_cspi.get()) {
        ERR("Driver not initialized");
        return;
    }

    m_left = left;
    m_right = right;

    utils::uint8_t output[2];
    output[0]=left;
    output[1]=right;

    m_cspi->openBus();
    m_cspi->rwData(&output[0], 2);
    m_cspi->closeBus();
#endif

    INFO("SetVolume: l: " << (utils::uint16_t)m_left << " r: " << (utils::uint16_t)m_right);
}
