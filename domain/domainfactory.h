/***************************************************************************
                          domainfactory.h  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef DOMAINFACTORY_H
#define DOMAINFACTORY_H

#include "rfw/utils/ctypes.h"
#include <mutex>

class AudioSelector;
class VolumeSelector;
class PowerSwitch;

class DomainFactory
{

public:
    DomainFactory();
    ~DomainFactory();
    AudioSelector* getAudioSelector();
    VolumeSelector* getVolumeSelector();
    PowerSwitch* getPowerSwitch();
    void initialize();
    void test();

private:
    std::unique_ptr<AudioSelector> m_AudioSelector;
    std::unique_ptr<VolumeSelector> m_VolumeSelector;
    std::unique_ptr<PowerSwitch> m_PowerSwitch;

    std::mutex m_mutex;
};

#endif // DOMAINFACTORY_H
