/***************************************************************************
                          audioselector.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "audioselector.h"
#include "rfw/utils/clogger.h"

/***********************************************************************//**
 @method : AudioSelector
 @comment: constructor
 ***************************************************************************/
AudioSelector::AudioSelector() :
    m_inputDevice(0)
{
}

/***********************************************************************//**
 @method : addOutput
 @comment: add an output to the selector list. The order of insertion matters.
 @param  : pin input pin
 @return : -
 ***************************************************************************/
void AudioSelector::addOutput(utils::uint32_t pin)
{
    boost::shared_ptr<Driver::OutputDevice> device = boost::shared_ptr<Driver::OutputDevice>(new Driver::OutputDevice(pin));
    m_deviceList.push_back(device);
}

/***********************************************************************//**
 @method : getNumberOfInputs
 @comment: get number of created input
 @return : number of inputs
 ***************************************************************************/
utils::uint8_t AudioSelector::getNumberOfInputs() const
{
    return m_deviceList.size();
}

/***********************************************************************//**
 @method : getActiveInput
 @comment: get the index of the activated input
 @return : input index
 ***************************************************************************/
utils::uint8_t AudioSelector::getActiveInput() const
{
    return m_inputDevice;
}

/***********************************************************************//**
 @method : setActiveInput
 @comment: set the index of the input
 @return : -
 ***************************************************************************/
void AudioSelector::setActiveInput(utils::uint8_t input)
{
    if (input == m_inputDevice) {
        INFO("analog channel already active");
        return ;
    }
    m_inputDevice = input;
    if (m_inputDevice >= m_deviceList.size()) {
        m_inputDevice = m_deviceList.size() -1;
    }
    DEBUG2("1. set all analog channel off");
    for (auto it : m_deviceList) {
        it->setValue(false);
    }
    DEBUG2("1. set all analog channel off");
    m_deviceList.at(m_inputDevice)->setValue(true);
    INFO("set analog channel: " << (utils::uint16_t)m_inputDevice);
}
