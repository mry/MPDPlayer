/***************************************************************************
                          cvisitormpdthread.h  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CVisitorMpdThread_H
#define CVisitorMpdThread_H

#include "rfw/utils/ctypes.h"
#include "extensions/cvisitor.h"

class IMPDController;

class CVisitorMpdThread :
    public CVisitor
{
public:
    CVisitorMpdThread(IMPDController* pController);
    virtual ~CVisitorMpdThread() = default;
    virtual void visitPowerMessage(CPowerMessage* pMsg);
    virtual void visitSelectorMessage(CSelectorMessage* pMsg);
    virtual void visitVolumeMessage(CVolumeMessage* pMsg);

private:
    IMPDController* m_pController;
};

#endif // CVisitorMpdThread_H
