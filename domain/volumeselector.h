/***************************************************************************
                          volumeselector.h  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VOLUMESELECTOR_H
#define VOLUMESELECTOR_H

#include "rfw/utils/ctypes.h"
#include <memory>

#include "drivers/cspi.h"

class VolumeSelector
{
public:
    VolumeSelector();
    ~VolumeSelector() = default;
    void initialize(uint8_t spiIndex);
    void setVolume(utils::uint8_t left, utils::uint8_t right);

private:
    utils::uint8_t m_left;
    utils::uint8_t m_right;
    std::unique_ptr<Driver::CSPI> m_cspi;
};

#endif // VOLUMESELECTOR_H
