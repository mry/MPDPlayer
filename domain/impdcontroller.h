/***************************************************************************
                          impdcontroller.h  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IMPDCONTROLLER_H
#define IMPDCONTROLLER_H

#include "extensions/cpowerids.h"

class IMPDController
{
public:
    IMPDController() = default;
    virtual ~IMPDController() = default;
    virtual void setVolume(utils::uint8_t left,utils::uint8_t right) = 0;
    virtual void setPower(std::vector<ENPowerSwitch> on) = 0;
    virtual void setAudioSelector(utils::uint8_t input) = 0;
};

#endif // IMPDCONTROLLER_H
