/***************************************************************************
                          mpdthread.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mpdthread.h"
#include "extensions/threadids.h"

#include "audioselector.h"
#include "volumeselector.h"
#include "powerswitch.h"
#include "cvisitormpdthread.h"

#include "extensions/ivisitor.h"

#include "rfw/utils/ceventmessage.h"
#include "rfw/utils/clogger.h"
#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctimer.h"

MPDThread::MPDThread() :
    utils::CThread(threadIds::MPDMain)
{
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void* MPDThread::Run(void * args)
{
    m_factory.initialize();

    // loki: asymmetric visitor pattern?
    while (IsRunning()) {
        utils::smartMessagePtr pMsg = ReceiveMessage();
        if (pMsg) {
            DEBUG2(" data: " << pMsg->ToString());
            IVisitor* pVisit =dynamic_cast<IVisitor*>(pMsg.get());
            CVisitorMpdThread visitor(this);
            pVisit->visit(&visitor);
        }
    }
    return nullptr;
}

/***********************************************************************//**
 @method : setVolume
 @comment: set volume left and right channel
 @param  : left left channel
 @param  : right right channel
 ***************************************************************************/
void MPDThread::setVolume(utils::uint8_t left,utils::uint8_t right)
{
    m_factory.getVolumeSelector()->setVolume(left,right);
}

/***********************************************************************//**
 @method : setPower
 @comment: set power
 @param  : on vector of outputs
 ***************************************************************************/
void MPDThread::setPower(std::vector<ENPowerSwitch> on)
{
    for (auto indx = ENPowerSwitch::PS_PREAMP;
         indx < ENPowerSwitch::PS_INVAL;
         ++indx)
    {
        auto device = m_factory.getPowerSwitch()->getDevice(indx);
        auto it = std::find(on.begin(), on.end(), indx);
        bool  output = (it != on.end());
        DEBUG2("setPower: " << output);
        device->setValue(output);
    }
}

/***********************************************************************//**
 @method : setAudioSelector
 @comment: set audio selector
 @param  : input channel to activate
 ***************************************************************************/
void MPDThread::setAudioSelector(utils::uint8_t input)
{
    m_factory.getAudioSelector()->setActiveInput(input);
}
