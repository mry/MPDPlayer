/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <string>

#include "rfw/utils/ctypes.h"
#include "rfw/utils/clogger.h"
#include "rfw/utils/cutils.h"

#include "domain/mpdthread.h"
#include "domain/ctestthread.h"
#include "vdc/cvdcthread.h"

#include "webserver/webserverfactory.h"

/***********************************************************************//**
  @method :  print_usage
  @comment:  display startup parameters
***************************************************************************/
void print_usage()
{
    fprintf(stderr, "Usage: player [OPTION] \n");
    fprintf(stderr, "TODO:\n");
    fprintf(stderr, " -h      --  print this help\n");
    fprintf(stderr, " -d <n>  --  set debug level  n= [1..7] \n");
    fprintf(stderr, " -p <x>  --  path to persistent data directory (persistent and icon)\n");
    fprintf(stderr, " -f <x>  --  file name of data file \n");
}

/**************************************************************************/
int main(int argc, char *argv[])
{
    if (argc < 2) {
        print_usage();
        exit(-1);
    }

    /**********************************************************************/
    // setup configuration
    #define CFG_OPT "c:"
    std::string dataDir = "/home/mry/";
    std::string dataFile = "MPDdata.vdc";
    utils::LOG_LEVEL_e level = utils::LOG_DEBUG2_e;

    int c;
    while ((c = getopt(argc, argv, "hd:p:f:" CFG_OPT)) != -1) {
        switch (c) {
        case 'h':
            print_usage();
            exit(0);
        case 'd':
            level = static_cast<utils::LOG_LEVEL_e> (atoi(optarg));
            break;
        case 'f':
            dataFile = optarg;
            break;
        case 'p':
            dataDir = optarg;
            break;
        }
    }
    // log level
    utils::CLogger::GetInstance().SetDebugLevel(level);

    /**********************************************************************/
    // Hardware abstraction control thread
    MPDThread m_MPDThread;
    m_MPDThread.StartThread();

    // Test thread for console testing only
    //CTestThread m_test;
    //m_test.StartThread();

    // digitalSTROM vdc thread for communication with dSS
    CVDCThread m_vdcthread;
    m_vdcthread.initialize(dataDir, dataFile);
    m_vdcthread.StartThread();

    sleep(1);

    /**********************************************************************/
    // Web server thread and reactive elements
    std::string documentRoot =  dataDir + "/www";
    WebServerFactory wsFactory(m_vdcthread.getSceneItf(), documentRoot);
    wsFactory.start();

    /**********************************************************************/
    // wait for termination
    utils::CUtils::WaitForTermination();

    /**********************************************************************/
    // stop all threads
    wsFactory.stop();
    m_MPDThread.StopThread();
    m_vdcthread.StopThread();
    INFO("Good bye");
    sleep(1);

    return 0;
}
