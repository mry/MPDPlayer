/***************************************************************************
                          mpdconnector.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mpdconnector.h"

#include "rfw/utils/clogger.h"

#include <mpd/client.h>
#include <stdexcept>

/***********************************************************************//**
 @method : MPDSettingsWrapper
 @comment: constructor
 @param  : settings
 ***************************************************************************/
MPDSettingsWrapper::MPDSettingsWrapper(mpd_settings* settings) :
    m_settings(settings)
{
}

/***********************************************************************//**
 @method : ~MPDSettingsWrapper
 @comment: destructor
 ***************************************************************************/
MPDSettingsWrapper::~MPDSettingsWrapper()
{
    mpd_settings_free(m_settings);
}

/***********************************************************************//**
 @method : getSettings
 @comment: get access to settings
 @return : settings
 ***************************************************************************/
mpd_settings* MPDSettingsWrapper::getSettings()
{
    return m_settings;
}

/***********************************************************************//**
 @method : MPDConnector
 @comment: constructor
 ***************************************************************************/
MPDConnector::MPDConnector() :
    m_connector(nullptr)
{
}

/***********************************************************************//**
 @method : ~MPDConnector
 @comment: desctructor
 ***************************************************************************/
MPDConnector::~MPDConnector()
{
    disconnect();
}

/***********************************************************************//**
 @method : connect
 @comment: connect to mpd
 @param  : host
 @param  : port
 ***************************************************************************/
bool MPDConnector::connect(std::string& host, unsigned port)
{
    if (m_connector == nullptr) {

        m_settings = std::unique_ptr<MPDSettingsWrapper>(new MPDSettingsWrapper(mpd_settings_new(nullptr, 0, 3000, nullptr, nullptr)));
        mpd_settings* settings = m_settings->getSettings();
        m_connector =  ::mpd_connection_new(mpd_settings_get_host(settings),
                            mpd_settings_get_port(settings),
                            mpd_settings_get_timeout_ms(settings));

        if (::mpd_connection_get_error(m_connector) != MPD_ERROR_SUCCESS) {
            ::mpd_connection_free(m_connector);
            ERR("Failed to connect mpd");
            m_connector = nullptr;
            return false;
        }
        return true;
    }
    WARN("mpd already connected");
    return true;
}

/***********************************************************************//**
 @method : disconnect
 @comment: disconnect from mpd
 @return : success
 ***************************************************************************/
bool MPDConnector::disconnect()
{
    if (m_connector != nullptr) {
        mpd_connection_free(m_connector);
        m_connector = nullptr;
        return true;
    }
    return false;
}

/***********************************************************************//**
 @method : isConnected
 @comment: get connection state
 @return : true: connected
 ***************************************************************************/
bool MPDConnector::isConnected() const
{
    return (m_connector != nullptr);
}

/***********************************************************************//**
 @method : getConnector
 @comment: get direct access to connector
 @return : mpd_connection
 ***************************************************************************/
mpd_connection* MPDConnector::getConnector() throw()
{
    if (!isConnected()) {
        throw std::runtime_error("mpd not connected");
    }
    return (m_connector);
}

/***********************************************************************//**
 @method : getHost
 @comment: get host name of connection
 @return : host by name
 ***************************************************************************/
std::string MPDConnector::getHost()
{
    if (!isConnected()) {
        throw std::runtime_error("mpd not connected");
    }
    return mpd_settings_get_host(m_settings->getSettings());
}

/***********************************************************************//**
 @method : getPort
 @comment: get port
 @return : port
 ***************************************************************************/
unsigned MPDConnector::getPort()
{
    if (!isConnected()) {
        throw std::runtime_error("mpd not connected");
    }
    return mpd_settings_get_port(m_settings->getSettings());
}

/***********************************************************************//**
 @method : getTimeoutMS
 @comment: get configured timeout in [ms]
 @return : timeout
 ***************************************************************************/
unsigned MPDConnector::getTimeoutMS()
{
    if (!isConnected()) {
        throw std::runtime_error("mpd not connected");
    }
    return mpd_settings_get_timeout_ms(m_settings->getSettings());
}

/***********************************************************************//**
 @method : setTimeoutMS
 @comment: set timeout in [ms]
 @param  : timeout
 ***************************************************************************/
void MPDConnector::setTimeoutMS(unsigned time)
{
    if (!isConnected()) {
        throw std::runtime_error("mpd not connected");
    }
    mpd_connection_set_timeout(m_connector, 10000);
}
