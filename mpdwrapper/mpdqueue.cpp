/***************************************************************************
                          mpdqueue.cpp- description
                          -------------------
    begin                : Wed April 5 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mpdqueue.h"
#include <mpd/queue.h>

/***********************************************************************//**
 @method : deleteID
 @comment: delete id
 @param  : conn connector
 @param  : id song with given ind
 @return : true: success
 ***************************************************************************/
bool MPDQueue::deleteID(MPDConnector& conn, unsigned int id)
{
    return mpd_run_delete_id(conn.getConnector(), id);
}

/***********************************************************************//**
 @method : removeAll
 @comment: remove all from queue
 @param  : conn connector
 @return : true: success
 ***************************************************************************/
bool MPDQueue::removeAll(MPDConnector& conn)
{
    return mpd_run_clear(conn.getConnector());
}

/***********************************************************************//**
 @method : add
 @comment: add to queue
 @param  : conn connector
 @param  : data song
 @return : true: success
 ***************************************************************************/
bool MPDQueue::add(MPDConnector& conn, std::string& data)
{
    return mpd_run_add(conn.getConnector(),data.c_str());
}

/***********************************************************************//**
 @method : add_id
 @comment: add to queue
 @param  : conn connector
 @param  : data song by id
 @return : true: success
 ***************************************************************************/
int MPDQueue::add_id(MPDConnector& conn, std::string& data)
{
    return mpd_run_add_id(conn.getConnector(), data.c_str());
}

/***********************************************************************//**
 @method : load
 @comment: load
 @param  : conn connector
 @param  : data ???
 @return : true: success
 ***************************************************************************/
bool MPDQueue::load(MPDConnector& conn, std::string& data)
{
    return mpd_run_load(conn.getConnector(), data.c_str());
}
