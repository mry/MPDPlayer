/***************************************************************************
                           mpddatabase.h
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MPDDATABASE_H
#define MPDDATABASE_H

#include "mpdconnector.h"
#include <vector>


enum BrowseType {
    BTSong = 0,
    BTDirectory = 1,
    BTPlaylist = 2
};

struct MPDBrowseData {
    BrowseType type;
    unsigned long duration;
    std::string uri;
    std::string title;
    std::string directory;
};

class MPDDirectoryWrapper
{
public:
    MPDDirectoryWrapper(const mpd_directory* directory);
    ~MPDDirectoryWrapper();

    MPDDirectoryWrapper & operator=(const MPDDirectoryWrapper&) = delete;
    MPDDirectoryWrapper(const MPDDirectoryWrapper&) = delete;
    MPDDirectoryWrapper() = delete;

    mpd_directory* getDirectory();

private:
    const mpd_directory *m_directory;
};

/**************************************************************************/
class MPDDatabase
{
public:
    MPDDatabase() = delete;
    ~MPDDatabase() = delete;
    static unsigned int updateDB(MPDConnector& conn, std::string& path);
    static std::vector<MPDBrowseData> browseAll(MPDConnector& conn, std::string& path, unsigned int offset, unsigned int& entity_count);
    static std::vector<MPDBrowseData> browsePlaylist(MPDConnector& conn, std::string& path, unsigned int offset, unsigned int& entity_count);

};

#endif // MPDDATABASE_H
