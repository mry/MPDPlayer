/***************************************************************************
                          mpdqueue.h - description
                          -------------------
    begin                : Wed April 5 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef MPDQUEUE_H
#define MPDQUEUE_H

#include "mpdconnector.h"

class MPDQueue
{
public:
    MPDQueue() = delete;
    ~MPDQueue() = delete;

    static bool deleteID(MPDConnector& conn, unsigned int id);
    static bool removeAll(MPDConnector& conn);
    static bool add(MPDConnector& conn, std::string& data);
    static int add_id(MPDConnector& conn, std::string& data);
    static bool load(MPDConnector& conn, std::string& data);
};

#endif // MPDQUEUE_H
