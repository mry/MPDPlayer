/***************************************************************************
                          mpdsong.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mpdsong.h"
#include "rfw/utils/clogger.h"
#include <libgen.h>

/***********************************************************************//**
 @method : MPDSongWrapper
 @comment: constructor
 @param  : song
 ***************************************************************************/
MPDSongWrapper::MPDSongWrapper(mpd_song *song)
    : m_song(song)
{
}

/***********************************************************************//**
 @method : MPDSongWrapper
 @comment: constructor
 @param  : song
 ***************************************************************************/
MPDSongWrapper::MPDSongWrapper(const mpd_song *song)
{
    m_song = (mpd_song *)song;
}

/***********************************************************************//**
 @method : ~MPDSongWrapper
 @comment: destructor
 ***************************************************************************/
MPDSongWrapper::~MPDSongWrapper()
{
    if (m_song) {
        mpd_song_free(m_song);
    }
}

/***********************************************************************//**
 @method : getSong
 @comment: get raw song
 @return : mpd_song
 ***************************************************************************/
mpd_song * MPDSongWrapper::getSong()
{
    return m_song;
}

/***********************************************************************//**
 @method : getCurrentSong
 @comment: get the current song
 @param  : conn connector
 @return : MPDSongWrapper
 ***************************************************************************/
std::unique_ptr<MPDSongWrapper> MPDSong::getCurrentSong(MPDConnector& conn)
{
    std::unique_ptr<MPDSongWrapper> song(new MPDSongWrapper(mpd_run_current_song(conn.getConnector())));
    if (song.get() == nullptr) throw std::runtime_error("get song error");
    return song;
}

/***********************************************************************//**
 @method : getSongTag
 @comment: get the current song tag as string
 @param  : conn connector
 @param  : type
 @param  : label
 @return : song tag as string
 ***************************************************************************/
std::string MPDSong::getSongTag(mpd_song* song,
                                   enum mpd_tag_type type,
                                   const char *label)
{
    unsigned i = 0;
    const char *value;
    std::string output;
    while ((value = mpd_song_get_tag(song, type, i++)) != nullptr) {
        output += value;
    }
    DEBUG2("Label: "<< label << ": " << output);
    return output;
}

/***********************************************************************//**
 @method : getSongDescription
 @comment: get the song description
 @param  : conn connector
 @return : song info
 ***************************************************************************/
MPDSongInfo MPDSong::getSongDescription(MPDConnector& conn)
{
    struct MPDSongInfo info;

    auto song = getCurrentSong(conn);
    if (song->getSong())
    {
        info.uri = mpd_song_get_uri(song->getSong());

        info.artist = getSongTag(song->getSong(), MPD_TAG_ARTIST, "artist");
        info.album  = getSongTag(song->getSong(), MPD_TAG_ALBUM, "album");
        info.title  = getSongTag(song->getSong(), MPD_TAG_TITLE, "title");
        info.track  = getSongTag(song->getSong(), MPD_TAG_TRACK, "track");
        info.name   = getSongTag(song->getSong(), MPD_TAG_NAME, "name");
        info.date   = getSongTag(song->getSong(), MPD_TAG_DATE, "date");
    }
    return info;
}

/***********************************************************************//**
 @method : getDuration
 @comment: get the duration
 @param  : conn connector
 @return : time in secs?
 ***************************************************************************/
unsigned MPDSong::getDuration(MPDConnector& conn)
{
    auto song = getCurrentSong(conn);
    auto songInfo = song->getSong();
    return mpd_song_get_duration(songInfo);
}

/***********************************************************************//**
 @method : getSongPos
 @comment: get the song position
 @param  : conn connector
 @return : time
 ***************************************************************************/
unsigned MPDSong::getSongPos(MPDConnector& conn)
{
    auto song = getCurrentSong(conn);
    auto songInfo = song->getSong();
    return mpd_song_get_pos(songInfo);
}

/***********************************************************************//**
 @method : getStart
 @comment: Returns the start of the virtual song within the physical file in seconds.
 @param  : conn connector
 @return : time
 ***************************************************************************/
unsigned MPDSong::getStart(MPDConnector& conn)
{
    auto song = getCurrentSong(conn);
    auto songInfo = song->getSong();
    return mpd_song_get_start(songInfo);
}

/***********************************************************************//**
 @method : getStart
 @comment: Returns the end of the virtual song within the physical file in seconds.
           Zero means that the physical song file is played to the end.
 @param  : conn connector
 @return : time
 ***************************************************************************/
unsigned MPDSong::getEnd(MPDConnector& conn)
{
    auto song = getCurrentSong(conn);
    auto songInfo = song->getSong();
    return mpd_song_get_end(songInfo);
}

/***********************************************************************//**
 @method : setPosition
 @comment: set position of a song
 @param  : conn connector
 @param  : pos position
 ***************************************************************************/
void MPDSong::setPosition(MPDConnector& conn, unsigned pos)
{
    auto song = getCurrentSong(conn);
    auto songInfo = song->getSong();
    mpd_song_set_pos(songInfo, pos);
    DEBUG2("setPosition: " << pos);
}

/***********************************************************************//**
 @method : getPosition
 @comment: set position of a song
 @param  : conn connector
 @return : position
 ***************************************************************************/
unsigned MPDSong::getPosition(MPDConnector& conn)
{
    auto song = getCurrentSong(conn);
    auto songInfo = song->getSong();
    return mpd_song_get_pos(songInfo);
}

/***********************************************************************//**
 @method : getId
 @comment: get id of the current song
 @param  : conn connector
 @return : id of song
 ***************************************************************************/
unsigned MPDSong::getId(MPDConnector& conn)
{
    auto song = getCurrentSong(conn);
    auto songInfo = song->getSong();
    return mpd_song_get_id(songInfo);
}

/***********************************************************************//**
 @method : getPrio
 @comment: get priority
 @param  : conn connector
 @return : priority
 ***************************************************************************/
unsigned MPDSong::getPrio(MPDConnector& conn)
{
    auto song = getCurrentSong(conn);
    auto songInfo = song->getSong();
    return mpd_song_get_prio(songInfo);
}

/***********************************************************************//**
 @method : getSongId
 @comment: get song ind
 @param  : conn connector
 @param  : songname
 @return : song id
 ***************************************************************************/
int MPDSong::getSongId(MPDConnector& conn, std::string& songname)
{
    std::unique_ptr<MPDSongWrapper> song =find_songname_id(conn, songname);
    if (!song->getSong()) {
        WARN("Song: " << songname << " not found");
        return -1;
    }
    return mpd_song_get_id(song->getSong());
}

/***********************************************************************//**
 @method : playSong
 @comment: play given song
 @param  : conn connector
 @param  : songname
 @return : song id
 ***************************************************************************/
int MPDSong::playSong(MPDConnector& conn, std::string& songname)
{
    int id = getSongId(conn, songname);
    if (id < 0) {
        WARN("Song ID not valid:" << id);
        return id;
    }
    if (!mpd_run_play_id(conn.getConnector(), id)) {
        WARN("play song not started. Id:" << id);
    }
    return id;
}

/***********************************************************************//**
 @method : search
 @comment: search songs which match part of search string
 @param  : conn connector
 @param  : search string
 @return : vector of matches
 ***************************************************************************/
std::vector<MPDSongInfo> MPDSong::search(MPDConnector& conn, std::string& searchString)
{
    std::vector<MPDSongInfo> info;

    if(mpd_search_db_songs(conn.getConnector(), false) == false) {
        throw std::runtime_error("mpd_search_db_songs");
    }

    if(mpd_search_add_any_tag_constraint(conn.getConnector(), MPD_OPERATOR_DEFAULT, searchString.c_str()) == false) {
        throw std::runtime_error("mpd_search_add_any_tag_constraint");
    }

    if(mpd_search_commit(conn.getConnector()) == false) {
        throw std::runtime_error("mpd_search_commit");
    }

    struct mpd_song * tempSong = nullptr;
    while ((tempSong = mpd_recv_song(conn.getConnector())) != nullptr) {
        std::unique_ptr<MPDSongWrapper> song(new MPDSongWrapper(tempSong));
        MPDSongInfo infoX;
        infoX.uri = std::string(mpd_song_get_uri(song->getSong()));
        infoX.duration = mpd_song_get_duration(song->getSong());
        infoX.title = getTitle(song.get()->getSong());
        info.push_back(infoX);
    }
    return info;
}

/***********************************************************************//**
 @method : find_songname_id
 @comment: find songname by id
 @param  : conn connector
 @param  : songname
 @return : song
 ***************************************************************************/
std::unique_ptr<MPDSongWrapper> MPDSong::find_songname_id(MPDConnector& conn, std::string& songname)
{
    mpd_search_queue_songs(conn.getConnector(), false);
    mpd_search_add_any_tag_constraint(conn.getConnector(),
                                        MPD_OPERATOR_DEFAULT,
                                        songname.c_str());
    mpd_search_commit(conn.getConnector());
    std::unique_ptr<MPDSongWrapper> song(new MPDSongWrapper(mpd_recv_song(conn.getConnector())));
    mpd_response_finish(conn.getConnector());
    return song;
}

/***********************************************************************//**
 @method : getTitle
 @comment: get title of song
 @param  : song
 @return : title of song
 ***************************************************************************/
std::string MPDSong::getTitle(const struct mpd_song* song)
{
    std::string retVal;

    const char* value = mpd_song_get_tag(song, MPD_TAG_TITLE, 0);
    if (value) {
        retVal = std::string(value);
    } else {
        retVal = basename((char *)mpd_song_get_uri(song));
    }
    return retVal;
}
