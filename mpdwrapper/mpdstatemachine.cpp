/***************************************************************************
                          mpdstatemachine.cpp  -  description
                             -------------------
    begin                : Thu April 2 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mpdstatemachine.h"
#include "mpdwrapper.h"
#include "mpdstatus.h"
#include "mpdoutput.h"
#include <sstream>

#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"

#include "rfw/utils/clogger.h"

#include "webserver/mpdwebresponse.h"

using namespace rapidjson;

MPDStateMachine::MPDStateMachine(MPDConnector& connector,
                                 boost::shared_ptr<SceneDescriptionItf> sceneItf) :
    m_state(MPD_DISCONNECTED),
    m_connector(connector),
    m_sceneItf(sceneItf),
    m_oldSongId(0)
{
}

/***********************************************************************//**
  @method : checkEmitSongChange
  @comment: emit song has changed
  @param  : currentSongId
***************************************************************************/
void MPDStateMachine::checkEmitSongChange(int currentSongId, Mongoose::WebSockets & sockets)
{
    if (m_oldSongId != currentSongId) {
        m_oldSongId = currentSongId;
        MPDSongInfo song = MPDSong::getSongDescription(m_connector);
        MpdWebResponse::emitCurrentSong(sockets,
                                    song.duration,
                                    song.title,
                                    song.artist,
                                    song.album);
    }
}

/***********************************************************************//**
  @method : processTimeEvent
  @comment: process connection state machine
  @param  : socket
***************************************************************************/
void MPDStateMachine::processTimeEvent(Mongoose::WebSockets & sockets)
{
    switch (m_state)
    {
        case MPD_DISCONNECTED:
        {
            std::string host;
            m_connector.connect(host, 0);
            if (m_connector.getConnector() == nullptr) {
                m_state = MPD_FAILURE;
                return;
            }
            if (MPDWrapper::isInErrorState(m_connector)) {
                std::string err = "MPD Connection error";
                MpdWebResponse::emitError(sockets, err);
                m_state = MPD_FAILURE;
                return;
            }
            m_connector.setTimeoutMS(10000);
            std::vector <MPDOut>  outputs = MPDOutput::getOutputs(m_connector);
            MpdWebResponse::emitOutput(sockets, outputs, true);
            m_state =  MPD_CONNECTED;
            DEBUG2("process mpd stm: disconnected -> connected");
        }
        break;

        case MPD_CONNECTED:
        {
            try {
                MStatus status = MPDStatus::getStatus(m_connector);
                // get volume from hw.
                status.volume = m_sceneItf->getVolume();
                MpdWebResponse::emitStatus(sockets, status);

                checkEmitSongChange(status.songId, sockets);

            } catch (std::runtime_error& err) {
                ERR("mpd connection fail");
                std::string error("mpd connection fail");
                MpdWebResponse::emitError(sockets, error);
                m_state =  MPD_FAILURE;
            }
        }
        break;

        case MPD_FAILURE:
        case MPD_TERMINATED:
        {
            ERR("FAILURE");
            m_connector.disconnect();
            m_state = MPD_DISCONNECTED;
        }
        break;
    }
}
