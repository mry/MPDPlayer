/***************************************************************************
                          mpdmixer.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mpdmixer.h"

/***********************************************************************//**
 @method : setVolume
 @comment: set volume to mpd
 @param  : conn connector
 @param  : valueAbs value for volume
 @return : true on success
 ***************************************************************************/
bool MPDMixer::setVolume(MPDConnector& conn, unsigned valueAbs)
{
    return mpd_run_set_volume(conn.getConnector(), valueAbs);
}

/***********************************************************************//**
 @method : changeVolume
 @comment: relative volume change
 @param  : conn connector
 @param  : valueRel value for volume
 ***************************************************************************/
bool MPDMixer::changeVolume(MPDConnector& conn, int valueRel)
{
    return mpd_run_change_volume(conn.getConnector(), valueRel);
}


