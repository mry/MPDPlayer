/***************************************************************************
                          mpdmixer.h - description
                          -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MPDMIXER_H
#define MPDMIXER_H

#include "mpdconnector.h"

#include <mpd/client.h>

/**************************************************************************/
class MPDMixer
{
public:
    MPDMixer() = delete;
    ~MPDMixer() = delete;
    static bool setVolume(MPDConnector& conn, unsigned valueAbs);
    static bool changeVolume(MPDConnector& conn, int valueRel);
};

#endif // MPDMIXER_H
