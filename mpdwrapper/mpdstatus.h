/***************************************************************************
                          mpdstatus.h - description
                          -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MPDSTATUS_H
#define MPDSTATUS_H

#include <mpd/client.h>
#include <memory>

#include "mpdconnector.h"

/**************************************************************************/
enum PLAYERSTATE
{
    UNKNOWN = MPD_STATE_UNKNOWN,
    STOP    = MPD_STATE_STOP,
    PLAY    = MPD_STATE_PLAY,
    PAUSE   = MPD_STATE_PAUSE
};

/**************************************************************************/
struct SongInfo
{
    int song;
    int elapsedTime;
    int totalTime;
    int bitRate;
    int songId;

    SongInfo() :
        song(0),
        elapsedTime(0),
        totalTime(0),
        bitRate(0),
        songId(0)
    {
    }
};

/**************************************************************************/
struct MStatus
{
    mpd_state state;
    int volume;
    bool repeat;
    bool single;
    unsigned crossfade;
    bool consume;
    bool random;
    int songPos;
    unsigned elapsedTime;
    unsigned totalTime;
    int songId;
    int songStatus;
    unsigned queueVersion;
};

/**************************************************************************/
class MPDStatusWrapper
{
public:
    MPDStatusWrapper(mpd_status * m_status);
    ~MPDStatusWrapper();

    MPDStatusWrapper & operator=(const MPDStatusWrapper&) = delete;
    MPDStatusWrapper(const MPDStatusWrapper&) = delete;
    MPDStatusWrapper() = delete;

    mpd_status * getStatus();
private:
    mpd_status * m_status;
};

/**************************************************************************/
class MPDStatus
{

public:
    MPDStatus() = delete;
    ~MPDStatus() = delete;
    static int getVolume(MPDConnector& conn);
    static int getRepeat(MPDConnector& conn);
    static int getSingle(MPDConnector& conn);
    static int getConsume(MPDConnector& conn);
    static int getRandom(MPDConnector& conn);
    static unsigned getQueueVersion(MPDConnector& conn);
    static int getQueueLength(MPDConnector& conn);
    static SongInfo getSongInfo(MPDConnector& conn);
    static PLAYERSTATE getPlayerState(MPDConnector& conn);
    static MStatus getStatus(MPDConnector& conn);

private:
    static std::unique_ptr<MPDStatusWrapper> getRunStatus(MPDConnector& conn);
    static std::unique_ptr<MPDStatusWrapper> getReceiveStatus(MPDConnector& conn);
};

#endif // MPDSTATUS_H
