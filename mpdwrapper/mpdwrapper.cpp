/***************************************************************************
                          mpdwrapper.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mpdwrapper.h"

#include "rfw/utils/clogger.h"

#include <mpd/client.h>

#include <stdexcept>
#include <sstream>

/***********************************************************************//**
 @method : getVersion
 @comment: get version
 @param  : conn
 @return : version as string
 ***************************************************************************/
const std::string MPDWrapper::getVersion(MPDConnector& conn)
{
    std::stringstream ss( "", std::ios_base::app | std::ios_base::out);
    std::string output;
    for(int i = 0; i < 3; ++i) {
        ss << (mpd_connection_get_server_version(conn.getConnector())[i]);
        ss << ".";
    }
    mpd_response_finish(conn.getConnector());
    return ss.str();
}

/***********************************************************************//**
 @method : getErrorMessage
 @comment: get error message as string
 @param  : conn
 @return : error message
 ***************************************************************************/
std::string MPDWrapper::getErrorMessage(MPDConnector& conn)
{
    int err = mpd_connection_get_error(conn.getConnector());
    if (err ==  MPD_ERROR_SUCCESS) return std::string();

    const char * output =
    mpd_connection_get_error_message(conn.getConnector());

    mpd_connection_clear_error(conn.getConnector());

    std::string strOutput ="Error";
    if (output) {
        strOutput = output;
    }

    mpd_response_finish(conn.getConnector());
    return strOutput;
}

/***********************************************************************//**
 @method : checkClearError
 @comment: clear error message. return true if clearance was necessary
 @param  : conn
 @return : true: error was cleared, false: no error
 ***************************************************************************/
bool MPDWrapper::checkClearError(MPDConnector& conn)
{
    int err = mpd_connection_get_error(conn.getConnector());
    if (err !=  MPD_ERROR_SUCCESS) {
        mpd_connection_clear_error(conn.getConnector());
        mpd_response_finish(conn.getConnector());
        return true;
    }
    return false;
}

/***********************************************************************//**
 @method : isInErrorState
 @comment: check if connection is in error state
 @param  : conn
 @return : state
 ***************************************************************************/
int MPDWrapper::isInErrorState(MPDConnector& conn)
{
    int err = mpd_connection_get_error(conn.getConnector());
    return err;
}
