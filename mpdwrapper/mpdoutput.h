#ifndef MPDOUTPUT_H
#define MPDOUTPUT_H

#include "mpdconnector.h"

#include <mpd/output.h>
#include <string>
#include <vector>

/**************************************************************************/
struct MPDOut
{
    unsigned int id;
    bool enabled;
    std::string name;
};

/**************************************************************************/
struct MPDQueueData
{
    unsigned int id;
    unsigned int position;
    unsigned int duration;
    std::string title;
};

/**************************************************************************/
class MPDOutputWrapper
{
public:
    MPDOutputWrapper(mpd_output *out);
    ~MPDOutputWrapper();

    MPDOutputWrapper & operator=(const MPDOutputWrapper&) = delete;
    MPDOutputWrapper(const MPDOutputWrapper&) = delete;
    MPDOutputWrapper() = delete;
    mpd_output * getOutput();

private:
    struct mpd_output *m_out;
};

/**************************************************************************/
class MPDEntityWrapper
{
public:
    MPDEntityWrapper(mpd_entity* entity);
    ~MPDEntityWrapper();

    MPDEntityWrapper & operator=(const MPDEntityWrapper&) = delete;
    MPDEntityWrapper(const MPDEntityWrapper&) = delete;
    MPDEntityWrapper() = delete;
    mpd_entity * getEntity();

private:
    struct mpd_entity *m_entity;
};

/**************************************************************************/
class MPDOutput
{
public:
    MPDOutput() = delete;
    ~MPDOutput() = delete;

    static bool enableOutput(MPDConnector& conn, unsigned int buf);
    static bool disableOutput(MPDConnector& conn, unsigned int buf);

    static std::vector <MPDOut> getOutputs(MPDConnector& conn);
    static std::vector<MPDQueueData> getQueue(MPDConnector& conn, unsigned int offset);
    //static int getBrowse(MPDConnector& conn, std::string& buffer, std::string path, unsigned int offset);
};

#endif // MPDOUTPUT_H
