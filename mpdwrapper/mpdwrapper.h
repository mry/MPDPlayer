﻿/***************************************************************************
                          mpdwrapper.h - description
                          -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MPDWRAPPER_H
#define MPDWRAPPER_H

#include <mpd/client.h>
#include <string>
#include <memory>

#include "mpdconnector.h"

/**************************************************************************/
class MPDWrapper
{
public:
    MPDWrapper() = delete;
    ~MPDWrapper() = delete;

    static const std::string getVersion(MPDConnector& conn);
    static std::string getErrorMessage(MPDConnector& conn);
    static bool checkClearError(MPDConnector& conn);
    static int isInErrorState(MPDConnector& conn);
};

#endif // MPDWRAPPER_H
