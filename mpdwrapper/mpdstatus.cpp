/***************************************************************************
                          mpdstatus.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mpdstatus.h"

#include "rfw/utils/clogger.h"

#include <mpd/client.h>

#include <stdexcept>
#include <sstream>

/***********************************************************************//**
 @method : MPDStatusWrapper
 @comment: contructor
 @param  : status
 ***************************************************************************/
MPDStatusWrapper::MPDStatusWrapper(mpd_status * status)
    : m_status(status)
{
}

/***********************************************************************//**
 @method : ~MPDStatusWrapper
 @comment: destructor
 ***************************************************************************/
MPDStatusWrapper::~MPDStatusWrapper()
{
    if (m_status) {
        mpd_status_free(m_status);
    }
}

/***********************************************************************//**
 @method : getStatus
 @comment: get raw status
 @return : raw status
 ***************************************************************************/
mpd_status * MPDStatusWrapper::getStatus()
{
    return m_status;
}

/***********************************************************************//**
 @method : getRunStatus
 @comment: get run status
 @param  : conn
 @return : MPDStatusWrapper
 ***************************************************************************/
std::unique_ptr<MPDStatusWrapper> MPDStatus::getRunStatus(MPDConnector& conn)
{
    std::unique_ptr<MPDStatusWrapper> status(new MPDStatusWrapper(mpd_run_status(conn.getConnector())));
    if ((status.get() == nullptr) || (status->getStatus() == nullptr)) {
        ERR(mpd_connection_get_error_message(conn.getConnector()));
        throw std::runtime_error("get run status error");
    }
    return status;
}

/***********************************************************************//**
 @method : getReceiveStatus
 @comment: get receiver status
 @param  : conn
 @return : MPDStatusWrapper
 ***************************************************************************/
std::unique_ptr<MPDStatusWrapper> MPDStatus::getReceiveStatus(MPDConnector& conn)
{
    std::unique_ptr<MPDStatusWrapper> status(new MPDStatusWrapper(mpd_recv_status(conn.getConnector())));
    if ((status.get() == nullptr) || (status->getStatus() == nullptr)) {
        ERR(mpd_connection_get_error_message(conn.getConnector()));
        throw std::runtime_error("get recv status error");
    }
    return status;
}

/***********************************************************************//**
 @method : getVolume
 @comment: get volume
 @param  : conn
 @return : volume level
 ***************************************************************************/
int MPDStatus::getVolume(MPDConnector& conn)
{
    auto status = getRunStatus(conn);
    int retVal = mpd_status_get_volume(status->getStatus());
    mpd_response_finish(conn.getConnector());
    return retVal;
}

/***********************************************************************//**
 @method : getRepeat
 @comment: get repeat
 @param  : conn
 @return : get repeat status
 ***************************************************************************/
int MPDStatus::getRepeat(MPDConnector& conn)
{
    auto status = getRunStatus(conn);
    int retVal = mpd_status_get_repeat(status->getStatus());
    mpd_response_finish(conn.getConnector());
    return retVal;
}

/***********************************************************************//**
 @method : getSingle
 @comment: get single
 @param  : conn
 @return : get single status
 ***************************************************************************/
int MPDStatus::getSingle(MPDConnector& conn)
{
    auto status = getRunStatus(conn);
    int retVal =  mpd_status_get_single(status->getStatus());
    mpd_response_finish(conn.getConnector());
    return retVal;
}

/***********************************************************************//**
 @method : getConsume
 @comment: get consume status
 @param  : conn
 @return : get consume status
 ***************************************************************************/
int MPDStatus::getConsume(MPDConnector& conn)
{
    auto status = getRunStatus(conn);
    int retVal = mpd_status_get_consume(status->getStatus());
    mpd_response_finish(conn.getConnector());
    return retVal;
}

/***********************************************************************//**
 @method : getRandom
 @comment: get random status
 @param  : conn
 @return : get random status
 ***************************************************************************/
int MPDStatus::getRandom(MPDConnector& conn)
{
    auto status = getRunStatus(conn);
    int retVal = mpd_status_get_random(status->getStatus());
    mpd_response_finish(conn.getConnector());
    return retVal;
}

/***********************************************************************//**
 @method : getQueueVersion
 @comment: get queue version
 @param  : conn
 @return : version
 ***************************************************************************/
unsigned MPDStatus::getQueueVersion(MPDConnector& conn)
{
    auto status = getRunStatus(conn);
    unsigned retVal = mpd_status_get_queue_version(status->getStatus());
    mpd_response_finish(conn.getConnector());
    return retVal;
}

/***********************************************************************//**
 @method : getQueueLength
 @comment: get queue length
 @param  : conn
 @return : length
 ***************************************************************************/
int MPDStatus::getQueueLength(MPDConnector& conn)
{
    auto status = getRunStatus(conn);
    int retVal = mpd_status_get_queue_length(status->getStatus());
    mpd_response_finish(conn.getConnector());
    return retVal;
}

/***********************************************************************//**
 @method : getSongInfo
 @comment: get song info
 @param  : conn
 @return : info
 ***************************************************************************/
SongInfo MPDStatus::getSongInfo(MPDConnector& conn)
{
    SongInfo info;
    auto status = getRunStatus(conn);

    if (mpd_status_get_state(status->getStatus()) == MPD_STATE_PLAY ||
        mpd_status_get_state(status->getStatus()) == MPD_STATE_PAUSE) {
        info.song = mpd_status_get_song_pos(status->getStatus());
        info.elapsedTime = mpd_status_get_elapsed_time(status->getStatus());
        info.totalTime = mpd_status_get_total_time(status->getStatus());
        info.bitRate = mpd_status_get_kbit_rate(status->getStatus());
        info.songId  = mpd_status_get_song_id(status->getStatus());
    }
    mpd_response_finish(conn.getConnector());
    return info;
}

/***********************************************************************//**
 @method : getPlayerState
 @comment: get player state
 @param  : conn
 @return : player state
 ***************************************************************************/
PLAYERSTATE MPDStatus::getPlayerState(MPDConnector& conn)
{
    auto status = getRunStatus(conn);
    PLAYERSTATE retVal = static_cast<PLAYERSTATE>(mpd_status_get_state(status->getStatus()));
    mpd_response_finish(conn.getConnector());
    return retVal;
}

/***********************************************************************//**
 @method : getStatus
 @comment: get status
 @param  : conn
 @return : status
 ***************************************************************************/
MStatus MPDStatus::getStatus(MPDConnector& conn)
{
    MPDStatusWrapper status(mpd_run_status(conn.getConnector()));
    if (status.getStatus() == nullptr) {
        ERR(mpd_connection_get_error_message(conn.getConnector()));
        throw std::runtime_error("get recv status error");
    }

    MStatus retVal;

    retVal.state        = mpd_status_get_state(status.getStatus());
    retVal.volume       = mpd_status_get_volume(status.getStatus());
    retVal.repeat       = mpd_status_get_repeat(status.getStatus());
    retVal.single       = mpd_status_get_single(status.getStatus());
    retVal.crossfade    = mpd_status_get_crossfade(status.getStatus());
    retVal.consume      = mpd_status_get_consume(status.getStatus());
    retVal.random       = mpd_status_get_random(status.getStatus());
    retVal.songPos      = mpd_status_get_song_pos(status.getStatus());
    retVal.elapsedTime  = mpd_status_get_elapsed_time(status.getStatus());
    retVal.totalTime    = mpd_status_get_total_time(status.getStatus());
    retVal.songId       = mpd_status_get_song_id(status.getStatus());
    retVal.queueVersion = mpd_status_get_queue_version(status.getStatus());

    return retVal;
}
