/***************************************************************************
                          mpdstatemachine.h  -  description
                             -------------------
    begin                : Thu April 2 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MPDSTATEMACHINE_H
#define MPDSTATEMACHINE_H

#include <boost/shared_ptr.hpp>

#include "mpdconnector.h"
#include "vdc/scenedescription.h"

#include <mongoose/Server.h>

struct MStatus;
struct MPDOut;

class MPDStateMachine
{
private:
    enum mpdConnStates
    {
        MPD_DISCONNECTED,
        MPD_FAILURE,
        MPD_CONNECTED,
        MPD_TERMINATED
    };

public:
    MPDStateMachine(MPDConnector& connector, boost::shared_ptr<SceneDescriptionItf> sceneItf);
    ~MPDStateMachine() = default;
    void processTimeEvent(Mongoose::WebSockets& sockets);

private:
    void checkEmitSongChange(int currentSongId, Mongoose::WebSockets & sockets);

private:
    mpdConnStates m_state;
    MPDConnector& m_connector;
    boost::shared_ptr<SceneDescriptionItf> m_sceneItf;

    int m_oldSongId;
};

#endif // MPDSTATEMACHINE_H
