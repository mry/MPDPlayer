/***************************************************************************
                          mpdsong.h - description
                          -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MPDSONG_H
#define MPDSONG_H

#include <mpd/client.h>
#include "mpdconnector.h"
#include <vector>

/**************************************************************************/
struct MPDSongInfo
{
    std::string uri;
    std::string artist;
    std::string album;
    std::string title;
    std::string track;
    std::string name;
    std::string date;
    unsigned duration;
};

/**************************************************************************/
class MPDSongWrapper
{
public:
    MPDSongWrapper(mpd_song *song);
    MPDSongWrapper(const mpd_song *song);
    ~MPDSongWrapper();

    MPDSongWrapper & operator=(const MPDSongWrapper&) = delete;
    MPDSongWrapper(const MPDSongWrapper&) = delete;
    MPDSongWrapper() = delete;

    mpd_song * getSong();

private:
    mpd_song *m_song;
};

/**************************************************************************/
class MPDSong
{
public:
    MPDSong() = delete;
    ~MPDSong() = delete;

    static MPDSongInfo getSongDescription(MPDConnector& conn);
    static unsigned getDuration(MPDConnector& conn);
    static unsigned getSongPos(MPDConnector& conn);
    static unsigned getStart(MPDConnector& conn);
    static unsigned getEnd(MPDConnector& conn);
    static void setPosition(MPDConnector& conn, unsigned pos);
    static unsigned getPosition(MPDConnector& conn);
    static unsigned getId(MPDConnector& conn);
    static unsigned getPrio(MPDConnector& conn);
    static int getSongId(MPDConnector& conn, std::string& songname);
    static int playSong(MPDConnector& conn, std::string& songname);
    static std::vector<MPDSongInfo> search(MPDConnector& conn, std::string& searchString);

    static  std::string getTitle(const struct mpd_song* song);

private:
    static std::unique_ptr<MPDSongWrapper> getCurrentSong(MPDConnector& conn);
    static std::string getSongTag(mpd_song* song,
                           enum mpd_tag_type type,
                           const char *label);
    static std::unique_ptr<MPDSongWrapper> find_songname_id(MPDConnector& conn, std::string& songname);

};

#endif // MPDSONG_H
