/***************************************************************************
                          mpddatabase.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mpddatabase.h"
#include "mpdoutput.h"
#include "mpdsong.h"
#include "mpdplaylist.h"
#include "mpdwrapper.h"

#include "rfw/utils/clogger.h"

#define MAX_ELEMENTS_PER_PAGE 512

static const int MAX_LOOP = 3;

/***********************************************************************//**
 @method : MPDDirectoryWrapper
 @comment: contructor
 @param  : directory
 ***************************************************************************/
MPDDirectoryWrapper::MPDDirectoryWrapper(const mpd_directory* directory) :
    m_directory(directory)
{
}

/***********************************************************************//**
 @method : ~MPDDirectoryWrapper
 @comment: destructor
 ***************************************************************************/
MPDDirectoryWrapper::~MPDDirectoryWrapper()
{
    mpd_directory_free((mpd_directory*)m_directory);
}

/***********************************************************************//**
 @method : getDirectory
 @comment: get mpd directory structure
 @return : mpd_directory
 ***************************************************************************/
mpd_directory* MPDDirectoryWrapper::getDirectory()
{
    return (mpd_directory*)m_directory;
}

/***********************************************************************//**
 @method : updateDB
 @comment: update mpd database
 @param  : conn connector
 @param  : path database path
 @return : return a positive job id on success, 0 on error
 ***************************************************************************/
unsigned int MPDDatabase::updateDB(MPDConnector& conn, std::string& path)
{
    if (path.empty()) {
        return mpd_run_update(conn.getConnector(), nullptr);
    } else {
        return mpd_run_update(conn.getConnector(), path.c_str());
    }
}

/***********************************************************************//**
 @method : browseAll
 @comment: browse mpd path
 @param  : conn connector
 @param  : path
 @param  : offset
 @param  : entitiy count (out: number of entities)
 @return : vector of browse data elements
 ***************************************************************************/
std::vector<MPDBrowseData> MPDDatabase::browseAll(MPDConnector& conn,
                                               std::string& path,
                                               unsigned int offset,
                                               unsigned int& entity_count)
{
    std::vector<MPDBrowseData> output;
    if (!mpd_send_list_meta(conn.getConnector(), path.c_str())) {
        ERR ("mpd_send_list_meta");
        return output;
    }

    entity_count = 0;
    mpd_entity* temp;
    while ((temp = mpd_recv_entity(conn.getConnector())) != nullptr) {
        {
        MPDEntityWrapper entity(temp);
        if(offset > entity_count)
        {
            entity_count++;
            continue;
        }

        else if(offset + MAX_ELEMENTS_PER_PAGE - 1 < entity_count)
        {
            break;
        }

        switch (mpd_entity_get_type(entity.getEntity())) {
        case MPD_ENTITY_TYPE_UNKNOWN:
            break;

        case MPD_ENTITY_TYPE_SONG:
        {
            const struct mpd_song* song = mpd_entity_get_song(entity.getEntity());
            MPDBrowseData data;
            data.type = BTSong;
            data.uri = mpd_song_get_uri(song);
            data.duration = mpd_song_get_duration(song);
            data.title = MPDSong::getTitle(song);
            output.push_back(data);
            break;
        }

        case MPD_ENTITY_TYPE_DIRECTORY:
        {
            MPDBrowseData data;
            data.type = BTDirectory;
            data.directory = mpd_directory_get_path(mpd_entity_get_directory(entity.getEntity()));
            output.push_back(data);
            break;
        }

        case MPD_ENTITY_TYPE_PLAYLIST:
        {
            MPDBrowseData data;
            data.type = BTPlaylist;
            data.directory = mpd_playlist_get_path(mpd_entity_get_playlist(entity.getEntity()));
            output.push_back(data);
            break;
        }

        }
        }
        entity_count++;
    }
    mpd_response_finish(conn.getConnector());
    return output;
}

/***********************************************************************//**
 @method : browsePlaylist
 @comment: get playlist entries
 @param  : conn connector
 @param  : path
 @param  : offset
 @param  : entitiy count (out: number of entities)
 @return : vector of browse data elements
 ***************************************************************************/
std::vector<MPDBrowseData> MPDDatabase::browsePlaylist(MPDConnector& conn, std::string& path, unsigned int offset, unsigned int& entity_count)
{
    std::vector<MPDBrowseData> output;
    if (!mpd_send_list_meta(conn.getConnector(), path.c_str())) {
        ERR ("mpd_send_list_meta");
        return output;
    }
    entity_count = 0;
    mpd_entity* temp;
    DEBUG2("browsePlaylist1");
    while ((temp = mpd_recv_entity(conn.getConnector())) != nullptr) {
        {
            DEBUG2("browsePlaylist2");
            MPDEntityWrapper entity(temp);
            /*
            if(offset > entity_count)
            {
                entity_count++;
                continue;
            }
            else if(offset + MAX_ELEMENTS_PER_PAGE - 1 < entity_count)
            {
                break;
            }
            */

            if (MPD_ENTITY_TYPE_PLAYLIST == mpd_entity_get_type(entity.getEntity())) {
                MPDBrowseData data;
                data.type = BTPlaylist;
                data.directory = mpd_playlist_get_path(mpd_entity_get_playlist(entity.getEntity()));
                output.push_back(data);
            }
        }
        entity_count++;
    }
    mpd_response_finish(conn.getConnector());
    return output;
}
