/***************************************************************************
                          mpdplayer.h - description
                          -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MPDPLAYER_H
#define MPDPLAYER_H

#include "mpdconnector.h"

/**************************************************************************/
/**
 * Fetches the currently selected song (the song referenced by
 * status->song and status, songid).
 */
class MPDPlayer
{
public:
    MPDPlayer() = delete;
    ~MPDPlayer() = delete;

    static bool runPlay(MPDConnector& conn);
    static bool runPlayPos(MPDConnector& conn, unsigned song_pos);
    static bool runPlayId(MPDConnector& conn, unsigned song_id);
    static bool runStop(MPDConnector& conn); // do not use..
    static bool runClear(MPDConnector& conn);
    static bool runTogglePause(MPDConnector& conn);
    static bool runPause(MPDConnector& conn, bool mode);
    static bool runNext(MPDConnector& conn);
    static bool runPrevious(MPDConnector& conn);
    static bool runSeekPos(MPDConnector& conn, unsigned song_pos, unsigned t);
    static bool runSeekId(MPDConnector& conn, unsigned song_id, unsigned t);
    static bool runRepeat(MPDConnector& conn, bool mode);
    static bool runRandom(MPDConnector& conn,bool mode);
    static bool runSingle(MPDConnector& conn, bool mode);
    static bool runConsume(MPDConnector& conn, bool mode);
    static bool runCrossfade(MPDConnector& conn, unsigned seconds);
    static bool runMixrampdb(MPDConnector& conn,float db);
    static bool runMixrampDelay(MPDConnector& conn, float seconds);
    static bool runClearError(MPDConnector& conn);
};

#endif // MPDPLAYER_H
