/***************************************************************************
                          mpdconnector.h - description
                          -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MPDCONNECTOR_H
#define MPDCONNECTOR_H

#include <mpd/client.h>
#include <memory>

/**************************************************************************/
class MPDSettingsWrapper
{
public:
    MPDSettingsWrapper(mpd_settings* settings);
    ~MPDSettingsWrapper();
    mpd_settings* getSettings();

private:
    mpd_settings * m_settings;
};

/**************************************************************************/
class MPDConnector
{
public:
    MPDConnector();
    ~MPDConnector();

    bool connect(std::string& host, unsigned port);
    bool disconnect();
    bool isConnected() const;
    mpd_connection* getConnector() throw();

    std::string getHost();
    unsigned getPort();
    unsigned getTimeoutMS();
    void setTimeoutMS(unsigned time);

private:
    struct mpd_connection* m_connector;
    std::unique_ptr<MPDSettingsWrapper> m_settings;
};

#endif // MPDCONNECTOR_H
