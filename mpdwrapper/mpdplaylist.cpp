/***************************************************************************
                          mpdplaylist.cpp  -  description
                             -------------------
    begin                : Wed Nov 2 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mpdplaylist.h"
#include "mpdqueue.h"

#include "rfw/utils/clogger.h"

#include <mpd/playlist.h>

/***********************************************************************//**
 @method : MPDWrapperPlayList
 @comment: constructor
 @param  : list
 ***************************************************************************/
MPDWrapperPlayList::MPDWrapperPlayList(mpd_playlist* list) :
    m_playlist(list)
{}

/***********************************************************************//**
 @method : MPDWrapperPlayList
 @comment: constructor
 @param  : list
 ***************************************************************************/
MPDWrapperPlayList::MPDWrapperPlayList(const mpd_playlist* settings)
{
    m_playlist = (mpd_playlist*)settings;
}

/***********************************************************************//**
 @method : ~MPDWrapperPlayList
 @comment: destructor
 @param  : list
 ***************************************************************************/
MPDWrapperPlayList::~MPDWrapperPlayList()
{
    mpd_playlist_free(m_playlist);
}

/***********************************************************************//**
 @method : getPlaylist
 @comment: raw access to mpd_playlist
 @param  : playlist
 ***************************************************************************/
mpd_playlist* MPDWrapperPlayList::getPlaylist()
{
    return m_playlist;
}

/***********************************************************************//**
 @method : getPlaylist
 @comment: get playlist
 @param  : conn connector
 @return : vector of entries as a string
 ***************************************************************************/
std::vector<std::string> MPDPlayList::getPlayList(MPDConnector& conn)
{
    std::vector<std::string> playList;
    bool success = mpd_send_list_playlists(conn.getConnector());
    struct mpd_playlist* list;

    while (success && (list = mpd_recv_playlist(conn.getConnector())) != nullptr) {
        playList.push_back(std::string(mpd_playlist_get_path(list)));
        INFO("listEntry:" << mpd_playlist_get_path(list));
        mpd_playlist_free(list);
    }

    if (mpd_connection_get_error(conn.getConnector()) != MPD_ERROR_SUCCESS) {
        mpd_response_finish(conn.getConnector());
    }
    return playList;
}

/***********************************************************************//**
 @method : runSave
 @comment: save
 @param  : conn connector
 @param  : param save
 @return : true: success
 ***************************************************************************/
bool MPDPlayList::runSave(MPDConnector& conn, std::string& param)
{
    if (!mpd_run_save(conn.getConnector(), param.c_str())) {
        WARN("Playlist exist. Try to delete it first");
        mpd_connection_clear_error(conn.getConnector());
        if (!mpd_run_rm(conn.getConnector(), param.c_str())) {
            ERR("Playlist could not be deleted");
            return false;
        }
        return mpd_run_save(conn.getConnector(), param.c_str());
    }
    return true;
}

/***********************************************************************//**
 @method : runPlaylistAdd
 @comment: add playlist with given path
 @param  : conn connector
 @param  : param name
 @param  : param path
 @return : true: success
 ***************************************************************************/
bool MPDPlayList::runPlaylistAdd(MPDConnector& conn, std::string& name, std::string& path)
{
    // 1. clear queue
    if (!MPDQueue::removeAll(conn)) {
        mpd_connection_clear_error(conn.getConnector());
        DEBUG1("queue already empty");
    } else {
        DEBUG1("queue is now empty");
    }

    // 2. add path to queue
    if (!MPDQueue::add(conn, path)) {
        mpd_connection_clear_error(conn.getConnector());
        WARN("add to queue failed");
        return false;
    } else {
        DEBUG1("stream " << path << " added to queue is now empty");
    }

    // 3. save to playlist
    if (!runSave(conn, name)) {
        mpd_connection_clear_error(conn.getConnector());
        WARN("save to playlist failed");
        return false;
    }
    return true;
}

/***********************************************************************//**
 @method : deletePlaylist
 @comment: delete playlist
 @param  : conn connector
 @param  : param playlist to remove
 @return : true: success
 ***************************************************************************/
bool MPDPlayList::deletePlaylist(MPDConnector& conn, std::string& param)
{
    if (!mpd_run_rm(conn.getConnector(), param.c_str())) {
        ERR("Playlist could not be deleted");
        mpd_connection_clear_error(conn.getConnector());
        return false;
    }
    return true;
}

