/***************************************************************************
                          mpdoutput.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mpdoutput.h"
#include "mpdsong.h"

#include "rfw/utils/clogger.h"


#define MAX_ELEMENTS_PER_PAGE 512

/***********************************************************************//**
 @method : MPDOutputWrapper
 @comment: contructor
 @param  : out value to wrap
 ***************************************************************************/
MPDOutputWrapper::MPDOutputWrapper(mpd_output *out) :
    m_out(out)
{
}

/***********************************************************************//**
 @method : ~MPDOutputWrapper
 @comment: destructor
 ***************************************************************************/
MPDOutputWrapper::~MPDOutputWrapper()
{
    mpd_output_free(m_out);
}

/***********************************************************************//**
 @method : getOutput
 @comment: get access to wrapped element
 @param  : mpd_output
 ***************************************************************************/
mpd_output * MPDOutputWrapper::getOutput()
{
    return m_out;
}

/***********************************************************************//**
 @method : MPDEntityWrapper
 @comment: contructor entitiy wrapper
 @param  : entity
 ***************************************************************************/
MPDEntityWrapper::MPDEntityWrapper(mpd_entity* entity) :
    m_entity(entity)
{
}

/***********************************************************************//**
 @method : ~MPDEntityWrapper
 @comment: destructor entitiy wrapper
 ***************************************************************************/
MPDEntityWrapper::~MPDEntityWrapper()
{
    mpd_entity_free(m_entity);
}

/***********************************************************************//**
 @method : getEntity
 @comment: access to wrapped element
 @return : entity
 ***************************************************************************/
mpd_entity * MPDEntityWrapper::getEntity()
{
    return m_entity;
}

/***********************************************************************//**
 @method : enableOutput
 @comment: enable output
 @param  : conn connector
 @param  : true: success
 ***************************************************************************/
bool MPDOutput::enableOutput(MPDConnector& conn, unsigned int buf)
{
    return mpd_run_enable_output(conn.getConnector(), buf);
}

/***********************************************************************//**
 @method : disableOutput
 @comment: disable output
 @param  : conn connector
 @param  : buf buffer by id
 @param  : true: success
 ***************************************************************************/
bool MPDOutput::disableOutput(MPDConnector& conn, unsigned int buf)
{
    return mpd_run_disable_output(conn.getConnector(), buf);
}

/***********************************************************************//**
 @method : getOutputs
 @comment: get vector of outputs
 @param  : conn connector
 @return : vector of outputs
 ***************************************************************************/
std::vector <MPDOut> MPDOutput::getOutputs(MPDConnector& conn)
{
    std::vector <MPDOut> retVal;
    mpd_send_outputs(conn.getConnector());

    struct mpd_output * out;
    while ((out = mpd_recv_output(conn.getConnector())) != nullptr) {
        MPDOut put;
        {
            MPDOutputWrapper output(out);
            put.enabled = mpd_output_get_enabled(output.getOutput());
            put.id = mpd_output_get_id(output.getOutput());
            put.name = mpd_output_get_name(output.getOutput());
            retVal.push_back(put);
        }
    }
    if (!mpd_response_finish(conn.getConnector())) {
        ERR("MPD outputs: " <<  mpd_connection_get_error_message(conn.getConnector()));
        mpd_connection_clear_error(conn.getConnector());
    }
    return retVal;
}

/***********************************************************************//**
 @method : getQueue
 @comment: get vector of queue data
 @param  : conn connector
 @param  : offset
 @return : vector of queue data
 ***************************************************************************/
std::vector<MPDQueueData> MPDOutput::getQueue(MPDConnector& conn, unsigned int offset)
{
    std::vector<MPDQueueData> retVal;
    if (!mpd_send_list_queue_range_meta(conn.getConnector(), offset, offset+MAX_ELEMENTS_PER_PAGE)) {
        throw std::runtime_error(" MPD Failed");
    }

    struct mpd_entity *entityRaw;
    while((entityRaw = mpd_recv_entity(conn.getConnector())) != nullptr) {
        {
            MPDEntityWrapper entity(entityRaw);
            if(mpd_entity_get_type(entity.getEntity()) == MPD_ENTITY_TYPE_SONG) {
                MPDQueueData put;
                const struct mpd_song * song = mpd_entity_get_song(entity.getEntity());
                put.id = mpd_song_get_id(song);
                put.position = mpd_song_get_pos(song);
                put.duration = mpd_song_get_duration(song);
                put.title = MPDSong::getTitle(song);
                retVal.push_back(put);
            }
        }
    }
    return retVal;
}
