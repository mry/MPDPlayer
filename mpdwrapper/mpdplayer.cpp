/***************************************************************************
                          mpdplayer.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mpdplayer.h"
#include "rfw/utils/clogger.h"
#include <mpd/client.h>

/***********************************************************************//**
 @method : runPlay
 @comment: start playing
 @param  : conn connector
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runPlay(MPDConnector& conn)
{
    return mpd_run_play(conn.getConnector());
}

/***********************************************************************//**
 @method : runPlayPos
 @comment: start play at position
 @param  : conn connector
 @param  : song_pos
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runPlayPos(MPDConnector& conn, unsigned song_pos)
{
    return mpd_run_play_pos(conn.getConnector(),song_pos);
}

/***********************************************************************//**
 @method : runPlayId
 @comment: play song given by id
 @param  : conn connector
 @param  : song_id
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runPlayId(MPDConnector& conn, unsigned song_id)
{
    return mpd_run_play_id(conn.getConnector(), song_id);
}

/***********************************************************************//**
 @method : runStop
 @comment: stop playing
 @param  : conn connector
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runStop(MPDConnector& conn)
{
    return mpd_run_stop(conn.getConnector());
}

/***********************************************************************//**
 @method : runClear
 @comment: clear queue
 @param  : conn connector
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runClear(MPDConnector& conn)
{
    return mpd_run_clear(conn.getConnector());
}

/***********************************************************************//**
 @method : runTogglePause
 @comment: toggle pause
 @param  : conn connector
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runTogglePause(MPDConnector& conn)
{
    return mpd_run_toggle_pause(conn.getConnector());
}

/***********************************************************************//**
 @method : runPause
 @comment: set pause
 @param  : conn connector
 @param  : mode ??
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runPause(MPDConnector& conn, bool mode)
{
    return mpd_run_pause(conn.getConnector(), mode);
}

/***********************************************************************//**
 @method : runNext
 @comment: set next song
 @param  : conn connector
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runNext(MPDConnector& conn)
{
    return mpd_run_next(conn.getConnector());
}

/***********************************************************************//**
 @method : runPrevious
 @comment: set previous song
 @param  : conn connector
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runPrevious(MPDConnector& conn)
{
    return mpd_run_previous(conn.getConnector());
}

/***********************************************************************//**
 @method : runSeekPos
 @comment: seel position
 @param  : conn connector
 @param  : song_pos song position
 @param  : t ??
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runSeekPos(MPDConnector& conn, unsigned song_pos, unsigned t)
{
    return mpd_run_seek_pos(conn.getConnector(), song_pos, t);
}

/***********************************************************************//**
 @method : runSeekId
 @comment: set song by od
 @param  : conn connector
 @param  : song_id
 @param  : t ??
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runSeekId(MPDConnector& conn, unsigned song_id, unsigned t)
{
    bool retVal = mpd_run_seek_id(conn.getConnector(),song_id, t);
    if (!retVal) {
        runClearError(conn);
    }
    return retVal;
}

/***********************************************************************//**
 @method : runRepeat
 @comment: set repeat mode
 @param  : conn connector
 @param  : mode : true: repeat
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runRepeat(MPDConnector& conn, bool mode)
{
    return mpd_run_repeat(conn.getConnector(), mode);
}

/***********************************************************************//**
 @method : runRepeat
 @comment: set repeat mode
 @param  : conn connector
 @param  : mode : true: repeat
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runRandom(MPDConnector& conn, bool mode)
{
    return mpd_run_random(conn.getConnector(), mode);
}

/***********************************************************************//**
 @method : runSingle
 @comment: set previous song
 @param  : conn connector
 @param  : mode true:set
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runSingle(MPDConnector& conn, bool mode)
{
    return mpd_run_single(conn.getConnector(), mode);
}

/***********************************************************************//**
 @method : runConsume
 @comment: set consume
 @param  : conn connector
 @param  : mode true:set
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runConsume(MPDConnector& conn, bool mode)
{
    return mpd_run_consume(conn.getConnector(), mode);
}

/***********************************************************************//**
 @method : runCrossfade
 @comment: set crossfade
 @param  : conn connector
 @param  : seconds
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runCrossfade(MPDConnector& conn, unsigned seconds)
{
    return mpd_run_crossfade(conn.getConnector(), seconds);
}

/***********************************************************************//**
 @method : runMixrampdb
 @comment: set crossfade
 @param  : conn connector
 @param  : db
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runMixrampdb(MPDConnector& conn,float db)
{
    return mpd_run_mixrampdb(conn.getConnector(), db);
}

/***********************************************************************//**
 @method : runMixrampDelay
 @comment: set crossfade
 @param  : conn connector
 @param  : seconds
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runMixrampDelay(MPDConnector& conn, float seconds)
{
    return mpd_run_mixrampdelay(conn.getConnector(), seconds);
}

/***********************************************************************//**
 @method : runClearError
 @comment: clear error
 @param  : conn connector
 @return : true: success
 ***************************************************************************/
bool MPDPlayer::runClearError(MPDConnector& conn)
{
    return mpd_connection_clear_error(conn.getConnector());
}

