/***************************************************************************
                          mpdplaylist.h  -  description
                             -------------------
    begin                : Wed Nov 2 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MPDPLAYLIST_H
#define MPDPLAYLIST_H

#include <mpd/client.h>
#include "mpdconnector.h"
#include <vector>
#include <string>


/**************************************************************************/
class MPDWrapperPlayList
{
public:
    MPDWrapperPlayList(mpd_playlist* settings);
    MPDWrapperPlayList(const mpd_playlist* settings);
    ~MPDWrapperPlayList();
    MPDWrapperPlayList(const MPDWrapperPlayList&) = delete;
    MPDWrapperPlayList() = delete;
    mpd_playlist* getPlaylist();

private:
    mpd_playlist * m_playlist;
};

class MPDPlayList
{
public:
    MPDPlayList() = delete;
    ~MPDPlayList() = delete;
    static std::vector<std::string> getPlayList(MPDConnector& conn);
    static bool runSave(MPDConnector& conn, std::string& param);
    static bool runPlaylistAdd(MPDConnector& conn, std::string& name, std::string& path);
    static bool deletePlaylist(MPDConnector& conn, std::string& param);
};

#endif // MPDPLAYLIST_H
