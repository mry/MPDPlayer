#!/bin/bash

# set environment
DIR=$PWD

export BASE=$DIR
export DSS=/home/mry/DSS/DSS
export RFW=/home/mry/Projects/Framework_RFW/deploy
export DEPLOY=/home/mry/Projects/MPDPlayer/MPDPlayer/deploy
export PKG_CONFIG_PATH=$DIR/DSS/lib/pkgconfig
export LOCAL=/usr/local

echo "--- mpdplayer  ---------------------------------------------------------------"
cmake \
        -DCMAKE_BUILD_TYPE=Debug \
        -DMONGOOSE_INCLUDE_DIR=${LOCAL}/include \
	-DMONGOOSE_LIB_DIR=${LOCAL}/lib \
	-DRFW_INCLUDE_DIR=${RFW}/include \
	-DRFW_LIB_DIR=${RFW}/lib \
	-DDSS_INCLUDE_DIR=${DSS}/include \
	-DDSS_LIB_DIR=${DSS}/lib \
	-DX86AUDIO=ON \
        -DPLAYER_DEPLOY_DIR=${DEPLOY} \
	.
	
make -verbose
make install

# -DCMAKE_INCLUDE_DIR=${DSS}/include \
# -DPLAYER_DEPLOY_DIR=${DEPLOY}
# -DCMAKE_INCLUDE_PATH=${DSS}/include \
# -DDSS_INCLUDE_DIR=${DSS}/include \
# -DDSS_LIB_DIR
# -DDSS_LIB_DIR=${DSS}/lib \
#	-DCMAKE_INSTALL_PREFIX=${DIR}/DSS \ 

