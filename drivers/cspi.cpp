/***************************************************************************
                          cspi.cpp  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/ctypes.h"
#include "rfw/utils/clogger.h"

#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>



#include "cspi.h"

namespace Driver {

/***********************************************************************//**
 @method : CSPI
 @comment: constructor. construct an spi object on the file device
 with a default clock rate.
 @param  : fn filename to open
 @return : -
 ***************************************************************************/
CSPI::CSPI(const std::string& fn) {
    init(500000, fn);
}

/***********************************************************************//**
 @method : CSPI
 @comment: constructor. construct an spi object on the file device
 with a given clock rate.
 @param  : speed clock speed for the spi bus
 @param  : fn filename to open
 @return : -
 ***************************************************************************/
CSPI::CSPI(utils::int32_t speed, const std::string& fn) {
    init(speed, fn);
}

/***********************************************************************//**
 @method : isReady
 @comment: Indicates that the SPI object has a valid file handle.
 @return : 1 -has a valid file handle. 0 - otherwise
 ***************************************************************************/
utils::int32_t CSPI::isReady() {
    if (m_fd > 0) {
        return 1;
    }
    return 0;
}

/***********************************************************************//**
 @method : ~CSPI
 @comment: destructor
 @return : -
 ***************************************************************************/
CSPI::~CSPI() {
}

/***********************************************************************//**
 @method : init
 @comment: initialize the object with starting values.
 @param  : speed clock speed [Hz] for the spi bus
 @param  : fn device name
 @return : -
 ***************************************************************************/
void CSPI::init(utils::int32_t speed, const std::string& fn) {
    m_fname = fn;
    m_speed = speed;
    m_spiMode = 0;
    m_spiBPW = 8;
    m_spiDelay = 0;
    m_fd = 0;
}

/***********************************************************************//**
 @method : openBus
 @comment: Opens the device file for the bus and sets the parameters.
 @param  : -
 @return : -
 ***************************************************************************/
utils::int32_t CSPI::openBus() {
    if (m_fd > 0) {
        INFO("SPI::openBus: bus already opened: " << m_fd);
        return 0;
    }

    utils::int32_t fd = open(m_fname.c_str(), O_RDWR);
    if (fd < 0) {
        ERR("SPI::openBus:  fail 1");
        return -1;
    }

    // Setup the SPI bus with our current parameters
    if (ioctl(fd, SPI_IOC_WR_MODE, &m_spiMode) < 0) {
        ERR("SPI::openBus: 2");
        return -1;
    }

    if (ioctl(fd, SPI_IOC_RD_MODE, &m_spiMode) < 0) {
        ERR("SPI::openBus: 3");
        return -1;
    }

    if (ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &m_spiBPW) < 0) {
        ERR("SPI::openBus: 4");
        return -1;
    }

    if (ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &m_spiBPW) < 0) {
        ERR("SPI::openBus: 5");
        return -1;
    }

    if (ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &m_speed) < 0) {
        ERR("SPI::openBus: 6");
        return -1;
    }

    if (ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &m_speed) < 0) {
        ERR("SPI::openBus: 7");
        return -1;
    }

    m_fd = fd;

    return 0;
}

/***********************************************************************//**
 @method : closeBus
 @comment: Closes the device file for the bus and sets the parameters.
 @param  : -
 @return : -
 ***************************************************************************/
utils::int32_t CSPI::closeBus() {
    if (m_fd > 0) {
        close(m_fd);
        m_fd = 0;
        return 0;
    } else {
        return m_fd;
    }
}

/***********************************************************************//**
 @method : setBPW
 @comment: Sets the 'Bits per Word' parameter for subsequent SPI transfers.
 @param  : val the number of bits to transfer for each SPI 'word'
 @return : -
 ***************************************************************************/
utils::int32_t CSPI::setBPW(utils::int32_t val) {
    utils::int32_t result = m_spiBPW;
    m_spiBPW = val;
    return result;
}

/***********************************************************************//**
 @method : setSpeed
 @comment: Sets the bus clock speed to the requested value.  Note that
 only certain speeds are valid.  The speed is actually determined
 by divding the core frequency by powers of two.  The actual clock
 rate will be the closest value that does not exceed this parameter.
 for a 250MHz core ex:
 (61,032 | 122,064 | 244,128 | 488,256 | 976,512 | 1,953,024 | 3,906,048...)
 @param  : val the requested clock frequency in Hz.
 @return : old speed
 ***************************************************************************/
utils::int32_t CSPI::setSpeed(utils::int32_t val) {
    utils::int32_t result = m_speed;
    m_speed = val;
    return result;
}

/***********************************************************************//**
 @method : setMode
 @comment: Sets the SPI operation mode.
 Sets one of 4 modes allowed by SPI to determine the clock polarity
 and leading or trailing edge data bit latching.
 @param  : val The SPI mode to use
 @return : old mode
 ***************************************************************************/
utils::int32_t CSPI::setMode(utils::int32_t val) {
    if (val < 0)
        val = 0;
    if (val > 3)
        val = 3;
    utils::int32_t result = m_spiMode;
    m_spiMode = val;
    return result;
}

/***********************************************************************//**
 @method : rwData
 @comment: Shifts data out as well as in.
 Sends the data contained in the buffer to the bus and reads the incoming
 data from the bus.  The buffer is overwritten with the incoming data.
 @param  : data Pointer to a buffer of data to send.
 @param  : len Number of bytes to send from the buffer.
 @return : ioctl return value
 ***************************************************************************/
utils::int32_t CSPI::rwData(utils::uint8_t* data, utils::uint8_t len) {

    if (m_fd == 0) {
        ERR ("rwData: failed, no valid file descriptor");
        return -1;
    }
    struct spi_ioc_transfer spiCtrl;

    spiCtrl.tx_buf = (unsigned long) data;
    spiCtrl.rx_buf = (unsigned long) data;
    spiCtrl.len = len;
    spiCtrl.delay_usecs = m_spiDelay;
    spiCtrl.speed_hz = m_speed;
    spiCtrl.bits_per_word = m_spiBPW;

    utils::int32_t retVal = ioctl(m_fd, SPI_IOC_MESSAGE(1), &spiCtrl);
    if (retVal < 0) {
        ERR("rwData: failed. Er Code: " << retVal);
    }
    return retVal;
}

/***********************************************************************//**
 @method : rwByte
 @comment: Send and recieve one 8 bit byte of data.
 @param  : bt Data byte to send.
 @return : read byte
 ***************************************************************************/
utils::uint8_t CSPI::rwByte(utils::uint8_t bt) {
    setBPW(8);
    rwData(&bt, 1);
    return bt;
}

/***********************************************************************//**
 @method : rwWord
 @comment: Send and recieve one 16 bit word of data.
 @param  : wd Data word to send.
 @return : read word
 ***************************************************************************/
utils::uint16_t CSPI::rwWord(utils::uint16_t wd) {
    setBPW(8);
    rwData((utils::uint8_t*) &wd, 2);
    return wd;
}

}
