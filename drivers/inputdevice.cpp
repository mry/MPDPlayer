/***************************************************************************
                          inputdevice.h  -  description
                          -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/ctypes.h"
#include "rfw/utils/clogger.h"

#include "gpio.h"
#include "inputdevice.h"
#include "inputobserver.h"

#include <stdio.h>
#include <sstream>

namespace Driver
{

/***********************************************************************//**
 @method : InputDevice
 @comment: Constructor
 @param  : pin pin number
 ***************************************************************************/
InputDevice::InputDevice(utils::uint32_t pin) :
    m_pin(pin),
    m_fileDesc(-1),
    m_OldValue(0)
{
    Gpio::exportPin(m_pin);
    Gpio::setDir(m_pin,Gpio::INPUT_PIN);
    char both [] ="both";
    Gpio::setEdge(m_pin, both);
    m_fileDesc = Gpio::openFd(pin);
    getGPIOValue(m_OldValue);
}

/***********************************************************************//**
 @method : ~InputDevice
 @comment: Destructor
 ***************************************************************************/
InputDevice::~InputDevice()
{
    Gpio::closeFd(m_fileDesc);
}

/***********************************************************************//**
 @method : getPin
 @comment: get pin number
 @return : pin number
 ***************************************************************************/
utils::uint32_t InputDevice::getPin() const
{
    return m_pin;
}

/***********************************************************************//**
 @method : getValue
 @comment: get the value of the pin
 @param  : [out] value value of pin
 @return : status
 ***************************************************************************/
bool InputDevice::getValue(bool& value)
{
    utils::uint32_t data=0;
    int status = getGPIOValue(data);
    value = data;

    if (status==0){
        doUpdate(data);
    }
    return status;
}

/***********************************************************************//**
 @method : registerObserver
 @comment: register an observer
 @param  : observer obs. to register
 @return : true: success, false: already registered
 ***************************************************************************/
bool InputDevice::registerObserver(InputObserver* observer)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    std::list<InputObserver*>::iterator element =
            std::find(m_ObserverList.begin(), m_ObserverList.end(), observer);

    std::list<InputObserver*>::iterator end = m_ObserverList.end();
    if (element == end){
        m_ObserverList.push_back(observer);
        observer->updateInput(m_OldValue);
        return true;
    }
    return false;
}

/***********************************************************************//**
 @method : unregisterObserver
 @comment: unregister an observer
 @param  : observer obs. to unregister
 ***************************************************************************/
void InputDevice::unregisterObserver(InputObserver* observer)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    m_ObserverList.remove(observer);
}

/***********************************************************************//**
 @method : getFileDescriptor
 @comment: get the file descriptor of the device
 @return : file descriptor
 ***************************************************************************/
utils::int32_t InputDevice::getFileDescriptor() const
{
    return m_fileDesc;
}

/***********************************************************************//**
 @method : queryUpdate
 @comment: get input value. Update if different
 ***************************************************************************/
void InputDevice::queryUpdate()
{
    utils::uint32_t value;
    int status = getGPIOValue(value);

    if (status!=0){
        ERR("could not read pin");
        return;
    }

    doUpdate(value);
}

/***********************************************************************//**
 @method : doUpdate
 @comment: update all observers with new value
 @param  : value new value
 ***************************************************************************/
void InputDevice::doUpdate(utils::uint32_t value)
{
    if (m_OldValue == value){
        INFO("Value is equal");
        return;
    }

    m_OldValue = value;
    INFO("Do Update Pin: " << m_pin << " Value: " << value);

    std::unique_lock<std::mutex> lock(m_mutex);
    std::list<InputObserver*>::iterator end = m_ObserverList.end();
    for (std::list<InputObserver*>::iterator iter = m_ObserverList.begin(); iter!=end; ++iter)
    {
        (*iter)->updateInput(m_OldValue);
    }
}

/***********************************************************************//**
 @method : getGPIOValue
 @comment: get input value
 @param  : [out] value of input
 @return : -1 fail, 0 ok
 ***************************************************************************/
utils::int32_t InputDevice::getGPIOValue(utils::uint32_t& value)
{
    if (m_fileDesc>=0)
    {
        char ch;
        read(m_fileDesc, &ch, 1);
        lseek(m_fileDesc, 0, SEEK_SET);
        if (ch != '0') {
            value = 1;
        } else {
            value = 0;
        }
        return 0;
    }
    ERR("File descriptor invalid, Value invalid!");
    return -1;
}

/***********************************************************************//**
 @method : getNumOfObservers
 @comment: get number of registered observer
 @return : number of observers
 ***************************************************************************/
size_t InputDevice::getNumOfObservers()
{
    return m_ObserverList.size();
}

};
