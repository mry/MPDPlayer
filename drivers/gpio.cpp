/***************************************************************************
                          gpio.cpp  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "gpio.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>

#include "rfw/utils/clogger.h"

namespace Driver
{

static const int MAX_BUF = 64;
#define SYSFS_GPIO_DIR "/sys/class/gpio"

/***********************************************************************//**
 @method : export Pin
 @comment: export the given gpio.(enable access)
 @param  : gpio number of gpio to expose
 ***************************************************************************/
int Gpio::exportPin(unsigned int gpio)
{
    int fd, len;
    char buf[MAX_BUF];

    fd = open(SYSFS_GPIO_DIR "/export", O_WRONLY);
    if (fd < 0) {
        ERR("gpio/export pin: " << gpio);
        return fd;
    }

    len = snprintf(buf, sizeof(buf), "%d", gpio);
    write(fd, buf, len);
    close(fd);

    return 0;
}

/***********************************************************************//**
 @method : unexportPin
 @comment: unexport the given gpio. (disable access)
 @param  : gpio number of gpio to expose
 ***************************************************************************/
int Gpio::unexportPin(unsigned int gpio)
{
    int fd, len;
    char buf[MAX_BUF];

    fd = open(SYSFS_GPIO_DIR "/unexport", O_WRONLY);
    if (fd < 0) {
        ERR("gpio/export pin:" << gpio);
        return fd;
    }

    len = snprintf(buf, sizeof(buf), "%d", gpio);
    write(fd, buf, len);
    close(fd);
    return 0;
}

/***********************************************************************//**
 @method : setDir
 @comment: set direction of pin (input or output)
 @param  : gpio number of pin
 @param  : out_flag direction
 @return : 0: assigned, <0: error
 ***************************************************************************/
int Gpio::setDir(unsigned int gpio, PIN_DIRECTION out_flag)
{
    int fd;
    char buf[MAX_BUF];

    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/direction", gpio);

    fd = open(buf, O_WRONLY);
    if (fd < 0) {
        ERR("gpio/direction pin: " << gpio << " direction: " << out_flag );
        return fd;
    }

    if (out_flag == OUTPUT_PIN){
        write(fd, "out", 4);
    } else {
        write(fd, "in", 3);
    }

    close(fd);
    return 0;
}

/***********************************************************************//**
 @method : setValue
 @comment: set value on gpio
 @param  : gpio number of pin
 @param  : value value to set (0, 1)
 @return : 0: assigned, <0: error
 ***************************************************************************/
int Gpio::setValue(unsigned int gpio, PIN_VALUE value)
{
    int fd;
    char buf[MAX_BUF];

    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);

    fd = open(buf, O_WRONLY);
    if (fd < 0) {
        ERR("gpio/set-value");
        return fd;
    }

    if (value == LOW) {
        write(fd, "0", 2);
    } else {
        write(fd, "1", 2);
    }

    close(fd);
    return 0;
}

/***********************************************************************//**
 @method : getValue
 @comment: Get value of gpio
 @param  : gpio number of pin
 @param  : value value to get (0, 1)
 @return : 0: assigned, <0: error
 ***************************************************************************/
int Gpio::setValue(unsigned int gpio, unsigned int *value)
{
    int fd;
    char buf[MAX_BUF];
    char ch;

    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);

    fd = open(buf, O_RDONLY);
    if (fd < 0) {
        ERR("gpio/get-value");
        return fd;
    }

    read(fd, &ch, 1);

    if (ch != '0') {
        *value = 1;
    } else {
        *value = 0;
    }

    close(fd);
    return 0;
}

/***********************************************************************//**
 @method : setValue
 @comment: Set edge
 @param  : gpio number of pin
 @param  : edge type (none, both, raising, falling)
 @return : 0: assigned, <0: error
 ***************************************************************************/
int Gpio::setEdge(unsigned int gpio, char *edge)
{
    int fd;
    char buf[MAX_BUF];

    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/edge", gpio);

    fd = open(buf, O_WRONLY);
    if (fd < 0) {
        ERR("gpio/set-edge");
        return fd;
    }

    write(fd, edge, strlen(edge) + 1);
    close(fd);
    return 0;
}

/***********************************************************************//**
 @method : openFd
 @comment: open file descriptor
 @param  : gpio number of pin
 @return : 0: assigned, <0: error
 ***************************************************************************/
int Gpio::openFd(unsigned int gpio)
{
    int fd;
    char buf[MAX_BUF];
    memset(buf, 0, MAX_BUF);
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);
    fd = open(buf, O_RDWR | O_NONBLOCK);

    if (fd < 0) {
        ERR("gpio/fd_open");
    }
    return fd;
}

/***********************************************************************//**
 @method : closeFd
 @comment: close file descriptor
 @param  : fd file descriptor to close
 @return : 0: assigned, <0: error
 ***************************************************************************/
int Gpio::closeFd(int fd)
{
    return close(fd);
}

} //namespace Driver

