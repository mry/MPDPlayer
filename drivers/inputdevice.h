/***************************************************************************
                          inputdevice.h  -  description
                          -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef INPUTDEVICE_H
#define INPUTDEVICE_H

#include "rfw/utils/ctypes.h"

#include <list>
#include <mutex>

namespace Driver
{

class InputObserver;

class InputDevice
{
    public:
    InputDevice( utils::uint32_t pin);
    ~InputDevice();

    utils::uint32_t getPin() const;
    bool getValue(bool& value);

    void queryUpdate();

    bool registerObserver(InputObserver* observer);
    void unregisterObserver(InputObserver* observer);

    utils::int32_t getFileDescriptor() const;

    size_t getNumOfObservers();

    private:
    void doUpdate(utils::uint32_t value);
    utils::int32_t getGPIOValue(utils::uint32_t& value);

    private:
    std::mutex m_mutex;
    utils::uint32_t m_pin;
    utils::uint32_t m_fileDesc;
    utils::uint32_t m_OldValue;
    std::list<InputObserver*> m_ObserverList;
};

};
#endif
