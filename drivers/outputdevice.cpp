/***************************************************************************
                          outputdevice.cpp  -  description
                          -------------------
    begin                : Mon Jul 25 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/ctypes.h"
#include "rfw/utils/clogger.h"

#include "gpio.h"
#include "outputdevice.h"

namespace Driver
{

/***********************************************************************//**
 @method : OutputDevice
 @comment: constructor
 @param  : pin pin by number
 ***************************************************************************/
OutputDevice::OutputDevice(utils::uint32_t pin) :
    m_pin(pin)
{
    Gpio::exportPin(m_pin);
    Gpio::setDir(m_pin,Gpio::OUTPUT_PIN);
    //Gpio::SetEdge(m_pin,"both");
    m_fileDesc = Gpio::openFd(m_pin);
}

/***********************************************************************//**
 @method : ~OutputDevice
 @comment: destructor
 ***************************************************************************/
OutputDevice::~OutputDevice()
{
    Gpio::closeFd(m_fileDesc);
}

/***********************************************************************//**
 @method : setValue
 @comment: set value to 0 or 1
 @param  : value
 @return : true: successful
 ***************************************************************************/
bool OutputDevice::setValue(bool value)
{
    if (setGPIOValue(value)) return true;
    ERR ("SetValue failed: " << m_pin);
    return false;
}

/***********************************************************************//**
 @method : setGPIOValue
 @comment: set value to 0 or 1
 @param  : value
 @return : true: successful
 ***************************************************************************/
bool OutputDevice::setGPIOValue(utils::uint32_t value)
{
    if (m_fileDesc>=0)
    {
        char ch;
        if (value == 0) {
            ch = '0';
        } else {
            ch = '1';
        }
        int retVal = write(m_fileDesc, &ch, 1);
        return (retVal >= 0);
    }
    ERR("file descriptor invalid");
    return false;
}

};
