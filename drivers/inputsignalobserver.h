/***************************************************************************
                           inputsignalobserver.h
                           -------------------
    begin                : Thu Sep 7 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef INPUTSIGNALOBSERVER_H_
#define INPUTSIGNALOBSERVER_H_

#include "rfw/utils/ctypes.h"
#include "drivers/inputobserver.h"

namespace Driver
{
class InputSignalObserver :
    public Driver::InputObserver
{
    /// this class can not be copied
    UNCOPYABLE(InputSignalObserver);

    public:
    InputSignalObserver(utils::int32_t commandset, utils::int32_t commandreset, utils::int32_t threadid);
    virtual ~InputSignalObserver();
    virtual void updateInput( bool bSet);
    utils::int32_t getCommandSet() const;
    utils::int32_t getCommandUnSet() const;

    private:
    utils::int32_t m_command_set;
    utils::int32_t m_command_reset;
    utils::int32_t m_threadId;
};
};
#endif /* INPUTSIGNALOBSERVER_H_ */
