/***************************************************************************
                          outputdevice.h  -  description
                          -------------------
    begin                : Mon Jul 25 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef OUTPUTDEVICE_H
#define OUTPUTDEVICE_H

#include "rfw/utils/ctypes.h"

namespace Driver
{

class OutputDevice
{
public:
    OutputDevice(utils::uint32_t pin);
    ~OutputDevice();
    bool setValue(bool value);

private:
    bool setGPIOValue(utils::uint32_t value);

private:
    utils::uint32_t m_pin;
    utils::int32_t m_fileDesc;
};

};
#endif // OUTPUTDEVICE_H
