/***************************************************************************
                          gpio.h  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef INPUTINTERFACE_H
#define INPUTINTERFACE_H

#include "rfw/utils/ctypes.h"

namespace Driver
{

class InputObserver;

class InputInterface
{
    public:
    virtual ~InputInterface(){};
    virtual bool registerDevice(utils::uint32_t device, InputObserver* pObserver) = 0;
    virtual bool unregisterDevice(utils::uint32_t device, InputObserver* pObserver) = 0;
    virtual bool getValue(utils::uint32_t device, bool& bset) = 0;
};

};

#endif
