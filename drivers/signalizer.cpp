/***************************************************************************
                          signalizer.cpp  -  description
                          -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "signalizer.h"
#include "rfw/utils/clogger.h"

#include <string.h>

namespace Driver
{

/***********************************************************************//**
 @method : Signalizer
 @comment: Constructor
 ***************************************************************************/
Signalizer::Signalizer()
{
    if (pipe(m_fileDesc)==-1) ERR("Failed to create pipe");
}

/***********************************************************************//**
 @method : ~Signalizer
 @comment: Destructor
 ***************************************************************************/
Signalizer::~Signalizer()
{
    close(m_fileDesc[0]);
    close(m_fileDesc[1]);
}

/***********************************************************************//**
 @method : getReadFd
 @comment: Get file descriptor for read (poll useage)
 @return : read file descriptor
 ***************************************************************************/
utils::int32_t Signalizer::getReadFd()
{
    return m_fileDesc[0];
}

/***********************************************************************//**
 @method : reset
 @comment: reset all file descriptors
 ***************************************************************************/
void Signalizer::reset()
{
    lseek(m_fileDesc[1], 0, SEEK_SET);
    lseek(m_fileDesc[0], 0, SEEK_SET);
}

/***********************************************************************//**
 @method : wakeup
 @comment: signalize a wakeup
 ***************************************************************************/
void Signalizer::wakeup()
{
    char data[] = "STOP";
    write(m_fileDesc[1], data, strlen(data));
}

}
