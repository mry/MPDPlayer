/***************************************************************************
                          inputobserver.h  -  description
                          -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef INPUTOBSERVER_H
#define INPUTOBSERVER_H

#include "rfw/utils/ctypes.h"
#include "rfw/utils/clogger.h"

namespace Driver
{

class InputObserver
{
    public:
    virtual ~InputObserver(){};
    virtual void updateInput( bool bSet) = 0;
};

/*
class TestInputObserver :
    public InputObserver
{
    public:
        virtual ~TestInputObserver(){};

        virtual void UpdateInput( bool bSet)
        {
            if (bSet)
            {
                INFO("Set");
            }
            else
            {
                INFO ("Unset");
            }
        }
};
*/


};
#endif
