/***************************************************************************
                          ispi.h  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CSPI_H
#define CSPI_H

#include <string>
#include <stdint.h>

#include "rfw/utils/ctypes.h"

#include "ispi.h"

namespace Driver
{

class CSPI: public ISPI
{
protected:
    utils::int32_t m_fd;
    std::string m_fname;
    utils::uint16_t m_spiDelay; // Don't know what this is for

public:
    CSPI(const std::string& fn);
    CSPI(utils::int32_t speed, const std::string& fn);
    virtual ~CSPI();

    utils::int32_t openBus();
    utils::int32_t closeBus();
    utils::int32_t isReady();
    utils::int32_t setBPW(int val);
    utils::int32_t setSpeed(int val);
    utils::int32_t setMode(int val);

    utils::int32_t rwData(utils::uint8_t *data, utils::uint8_t len);
    utils::uint8_t rwByte(utils::uint8_t bt);
    utils::uint16_t rwWord(utils::uint16_t wd);

protected:
    void init(utils::int32_t speed, const std::string& fn);

}; //  class CSPI

} // namespace Driver

#endif /* ISPI_H */
