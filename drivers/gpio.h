/***************************************************************************
                          gpio.h  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SIMPLEGPIO_H_
#define SIMPLEGPIO_H_

#include "rfw/utils/ctypes.h"

namespace Driver
{

class Gpio
{
    UNCOPYABLE(Gpio)

private:
    Gpio(){}; // can not be instantiated

public:
    ~Gpio(){};

    enum PIN_DIRECTION
    {
            INPUT_PIN=0,
            OUTPUT_PIN=1
    };

    enum PIN_VALUE
    {
            LOW=0,
            HIGH=1
    };

    static int exportPin(unsigned int gpio);
    static int unexportPin(unsigned int gpio);

    static int setDir(unsigned int gpio, PIN_DIRECTION out_flag);
    static int setEdge(unsigned int gpio, char *edge);

    static int setValue(unsigned int gpio, PIN_VALUE value);
    static int setValue(unsigned int gpio, unsigned int *value);

    static int openFd(unsigned int gpio);
    static int closeFd(int fd);

}; // class Cpio

} // namespace Driver

#endif /* SIMPLEGPIO_H_ */
