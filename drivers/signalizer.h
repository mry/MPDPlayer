/***************************************************************************
                          signalizer.h  -  description
                          -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SIGNALIZER_H
#define SIGNALIZER_H

#include "rfw/utils/ctypes.h"

#include <unistd.h>

namespace Driver
{

class Signalizer
{
    UNCOPYABLE(Signalizer)

public:
    Signalizer();
    ~Signalizer();

    utils::int32_t getReadFd();
    void reset();
    void wakeup();

private:
    utils::int32_t m_fileDesc[2];
};

}
#endif
