/***************************************************************************
                          inputobserver.h  -  description
                          -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "inputthread.h"

#include "inputdevice.h"
#include "inputobserver.h"

#include "extensions/cthreadids.h"

#include "rfw/utils/clogger.h"

#include <iostream>
#include <string.h>

namespace Driver
{

/***********************************************************************//**
 @method : InputThread
 @comment: constructor
 ***************************************************************************/
InputThread::InputThread() :
    CThread(CThreadIds::THREAD_INPUT),
    m_pollSize(0)
{
}

/***********************************************************************//**
 @method : ~InputThread
 @comment: destructor
 ***************************************************************************/
InputThread::~InputThread()
{
    std::unique_lock<std::mutex> lock(m_mutex);
    m_InputDevice.clear();
}

/***********************************************************************//**
 @method : registerDevice
 @comment: register a device
 @param  : device pin number
 @param  : oberserver
 @return : true: observer registered, false: not registered
 ***************************************************************************/
bool InputThread::registerDevice(utils::uint32_t device, InputObserver* pObserver)
{
    if (m_InputDevice.size() >= MAX_DEVICE)  return false;

    std::unique_lock<std::mutex> lock(m_mutex);
    MapInputDevice::const_iterator search = m_InputDevice.find(device);

    InputDevice* pDevice = 0;
    if(search != m_InputDevice.end())
    {
        pDevice = search->second;
    }
    else
    {
        pDevice = new InputDevice(device);
        m_InputDevice.insert(MapInputDevice::value_type(device, pDevice));
        m_signalizer.wakeup();
    }

    return pDevice->registerObserver(pObserver);
}

/***********************************************************************//**
 @method :unregisterDevice
 @comment: unregister observer
 @param  : device pin number
 @param  : observer to unregister
 @return : true: success, false: failed
 ***************************************************************************/
bool InputThread::unregisterDevice(utils::uint32_t device, InputObserver* pObserver)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    MapInputDevice::iterator search = m_InputDevice.find(device);
    if(search == m_InputDevice.end())
    {
        return false;
    }
    else
    {
        InputDevice* pDevice = search->second;
        pDevice->unregisterObserver(pObserver);

        if (pDevice->getNumOfObservers() == 0)
        {
            m_InputDevice.erase(search);
            m_signalizer.wakeup();
        }
    }
    return true;
}

/***********************************************************************//**
 @method : getValue
 @comment: get value of pin
 @param  : device
 @param  : [out] bSet
 @return : status
 ***************************************************************************/
bool InputThread::getValue(utils::uint32_t device, bool& bSet)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    MapInputDevice::const_iterator search = m_InputDevice.find(device);
    if(search == m_InputDevice.end())
    {
        return false;
    }

    InputDevice* pDevice = search->second;
    return pDevice->getValue(bSet);
}

/***********************************************************************//**
 @method : Run
 @comment: main thread function
 @return : number of observers
 ***************************************************************************/
void* InputThread::Run(void * args)
{
    while(IsRunning())
    {
        preparePoll();
        poll();
        updateAllDevices();
    }
    DEBUG1("thread stopped");
    return nullptr;
}

/***********************************************************************//**
 @method : preparePoll
 @comment: setup polling
 ***************************************************************************/
void InputThread::preparePoll()
{
    memset(m_fdpoll, 0, sizeof(m_fdpoll));

    m_signalizer.reset();
    m_fdpoll[0].fd =  m_signalizer.getReadFd();
    m_fdpoll[0].events = POLLPRI;
    m_fdpoll[0].revents = 0;

    int index = 1;
    for (MapInputDevice::iterator iter = m_InputDevice.begin(); iter !=m_InputDevice.end(); ++iter)
    {
        m_fdpoll[index].fd = iter->second->getFileDescriptor();
        m_fdpoll[index].events = POLLPRI;
        m_fdpoll[index].revents = 0;
        ++index;
    }
    m_pollSize = index;
}

/***********************************************************************//**
 @method : poll
 @comment: poll the filedescriptors (blocking wait until something changes)
 ***************************************************************************/
void InputThread::poll()
{
    ::poll(m_fdpoll, m_pollSize, -1);
}

/***********************************************************************//**
 @method : updateAllDevices
 @comment: Update all items
 ***************************************************************************/
void InputThread::updateAllDevices()
{
    DEBUG1("Update devices");
    for (utils::uint32_t i=0; i < m_pollSize; ++i){
        if (m_fdpoll[i].revents & POLLPRI) {
            for (MapInputDevice::const_iterator iter = m_InputDevice.begin(); iter != m_InputDevice.end(); ++iter){
                if (iter->second->getFileDescriptor() == m_fdpoll[i].fd){
                    iter->second->queryUpdate();
                    break;
                }
            }
        }
    }
}

};
