/***************************************************************************
                           inputsignalobserver.cpp  -  description
                           -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/ceventmessage.h"
#include "rfw/utils/cthreadmanager.h"
#include "inputsignalobserver.h"
#include "extensions/cthreadids.h"

namespace Driver
{

/***********************************************************************//**
 @method : InputSignalObserver
 @comment: constructor
 ***************************************************************************/
InputSignalObserver::InputSignalObserver(utils::int32_t commandset,
                                         utils::int32_t commandreset,
                                         utils::int32_t threadid) :
    m_command_set(commandset),
    m_command_reset(commandreset),
    m_threadId(threadid)
{
}

/***********************************************************************//**
 @method : ~InputSignalObserver
 @comment: destructor
 ***************************************************************************/
InputSignalObserver::~InputSignalObserver()
{
}

/***********************************************************************//**
 @method : getCommandSet
 @comment: get command set
 @return : command set
 ***************************************************************************/
utils::int32_t InputSignalObserver::getCommandSet() const
{
    return m_command_set;
}

/***********************************************************************//**
 @method : getCommandUnSet
 @comment: get command unset
 @return : command unset
 ***************************************************************************/
utils::int32_t InputSignalObserver::getCommandUnSet() const
{
    return m_command_reset;
}

/***********************************************************************//**
 @method : updateInput
 @comment: update input
 @param  : bSet: true: set
 ***************************************************************************/
void InputSignalObserver::updateInput( bool bSet)
{
    SMART_PTR(utils::CEventMessage) pMsg = utils::CREATE_SMART_PTR(utils::CEventMessage);

    if (bSet){
        pMsg->SetCommand(m_command_set);
    } else {
        pMsg->SetCommand(m_command_reset);
    }

    utils::CThreadManager::GetInstance().Send(m_threadId, m_threadId, pMsg);
}
} /* namespace Driver */

