#ifndef SCENEDESCRIPTION_H
#define SCENEDESCRIPTION_H

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include "rfw/utils/ctypes.h"


typedef enum
{
    SCP_NOP_e = 0,
    SCP_PLAY_e = 1,
    SCP_STOP_e = 2,

} SCENE_PLAYER_CMD_e;

struct SceneDescription {
    utils::uint8_t m_volume;
    utils::uint8_t m_channel;
    utils::uint8_t m_power;
    SCENE_PLAYER_CMD_e m_mpdCommand;
    std::string m_playlist;
    std::string m_song;
    // MPDSetting

    SceneDescription() :
        m_volume(0),
        m_channel(0),
        m_power(0),
        m_mpdCommand(SCP_NOP_e)
    {
    }

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int /*version*/)
    {
        ar & m_volume;
        ar & m_channel;
        ar & m_power;
        ar & m_mpdCommand;
        ar & m_playlist;
        ar & m_song;
    };
};


class SceneDescriptionItf
{
public:
    SceneDescriptionItf(){};
    virtual ~SceneDescriptionItf(){};
    virtual void setScene(utils::uint8_t scene, utils::uint8_t volume, utils::uint8_t channel, utils::uint8_t power) = 0;
    virtual void setPlayerScene(utils::uint8_t scene, utils::uint8_t command, std::string playlist, std::string song) = 0;
    virtual SceneDescription getScene(utils::uint8_t scene) const = 0;
    virtual utils::int32_t getActiveScene() const = 0;
    virtual std::vector<SceneDescription> getAllScenes() const = 0;
    virtual void setActiveScene(utils::uint8_t scene) = 0;
    virtual void setVolume(utils::uint8_t volume) = 0;
    virtual utils::uint8_t getVolume() const = 0;
    virtual void setChannel(utils::uint8_t volume) = 0;
    virtual utils::uint8_t getChannel() const = 0;
    virtual void setPower(utils::uint8_t power) = 0;
    virtual utils::uint8_t getPower() const = 0;
};

#endif // SCENEDESCRIPTION_H
