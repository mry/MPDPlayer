/***************************************************************************
                         modelsubject.h  -  description
                         -------------------
    begin                : Fri Sep 23 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MODELSUBJECT_H
#define MODELSUBJECT_H

#include <vector>

class ModelObserver;

class ModelSubject
{
public:
    ModelSubject();
    virtual ~ModelSubject();
    void notifyModelChange();
    void attach(ModelObserver* observer);
    void detach(ModelObserver* observer);

private:
    std::vector<ModelObserver*> m_list;
};

#endif // MODELSUBJECT_H
