﻿/***************************************************************************
                          vdcthread.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "extensions/threadids.h"
#include "extensions/ivisitor.h"

#include "cvdcthread.h"
#include "cvisitorvdcthread.h"
#include "mpdvdc.h"

#include "rfw/utils/ceventmessage.h"
#include "rfw/utils/cmessage.h"
#include "rfw/utils/clogger.h"

#include <iostream>
#include <fstream>

CVDCThread::CVDCThread() :
    utils::CThread(threadIds::MPDVdc)
{
}

/***********************************************************************//**
  @method :  Run
  @comment:  thread rum method
  @param  :  args input parameters
  @param  :  output parameters
***************************************************************************/
void* CVDCThread::Run(void * args)
{
    // start thread which cyclically runs m_access.run

    loadData();
    if(checkCreateVdc()) {
        saveData();
    }

    m_access.initializeLib();
    m_access.registerCallbacks();

    while (IsRunning()) {
       m_access.run();
       //DEBUG2("run");

       while(GetPendingMessages()) {
           utils::smartMessagePtr pMsg = ReceiveMessage();
           IVisitor* pVisit =dynamic_cast<IVisitor*>(pMsg.get());
           CVisitorVdcThread visitor(this);
           pVisit->visit(&visitor);
       }
    }

    m_access.unregisterCallbacks();
    return nullptr;
}

/***********************************************************************//**
  @method :  initialize
  @comment:  initialize thread class
  @param  :  dataDir directory of files
  @param  :  file persistent data file
***************************************************************************/
void CVDCThread::initialize(std::string& dataDir, std::string& file)
{
    m_dataDirectory = dataDir;
    m_persistentFile = file;
    std::string filename = m_dataDirectory +"/mpdplayer.png";
    try {
        std::ifstream file;
        file.open (filename, std::ios::in|std::ios::binary|std::ios::ate);
        m_size = file.tellg();
        m_memblock = new uint8_t [m_size];
        file.seekg (0, std::ios::beg);
        file.read ((char*)m_memblock, m_size);
        file.close();

        std::string defaultName = "MPDOutput";
        MPDVdc::setGlobalIcon(m_memblock, m_size, defaultName);
        delete [] m_memblock;

    } catch (std::bad_alloc& e) {
        ERR("can not load image file " << filename << " does not exist:" << e.what());
    }

    IVDSD::setModelUpdater(&m_model);
    IVDC::setModelUpdater(&m_model);
    m_model.attach(this);
}

/***********************************************************************//**
  @method :  getSceneItf
  @comment:  get scene itf
  @return :  SceneDescriptionItf shared_ptr
***************************************************************************/
boost::shared_ptr<SceneDescriptionItf> CVDCThread::getSceneItf()
{
    return m_access.getSceneItf();
}


/***********************************************************************//**
  @method :  Update
  @comment:  observer pattern
***************************************************************************/
void CVDCThread::Update()
{
    saveData();
}

/***********************************************************************//**
  @method :  saveData
  @comment:  save persistent data
***************************************************************************/
void CVDCThread::saveData()
{
    std::unique_lock<std::mutex> lock(m_CreationMutex);
    // create and open a character archive for output

    std::string persistentData =m_dataDirectory + "/" + m_persistentFile;
    std::ofstream ofs(persistentData);
    boost::archive::text_oarchive oa(ofs);

    // write class instance to archive
    oa << m_access;
    // archive and stream closed when destructors are called
}

/***********************************************************************//**
  @method :  loadData
  @comment:  load persistent data and initialize vdc and vdcds
***************************************************************************/
void CVDCThread::loadData()
{
    std::unique_lock<std::mutex> lock(m_CreationMutex);
    // load data

    // create and open an archive for input
    std::string persistentData =m_dataDirectory + "/" + m_persistentFile;
    std::ifstream ifs(persistentData);
    if (!ifs.good()) {
        ERR( "can not load data. File " << persistentData << " does not exist");
        return;
    }
    boost::archive::text_iarchive ia(ifs);

    // read class state from archive
    ia >> m_access;
    // archive and stream closed when destructors are called
}

/***********************************************************************//**
  @method :  checkCreateVdc
  @comment:  check if vdc and vdcd are persistently stored.
             If not, create them.
  @return :  true: creation was necessary
***************************************************************************/
bool CVDCThread::checkCreateVdc()
{
  IVDSD::registerVdcAcces(&m_access);

  bool creation = false;
  if (!m_access.getVDC()) {
    boost::shared_ptr<VDC> myVdc(new VDC());

    dsuid_t vdcDsuid;
    dsuid_generate_v4_random(&vdcDsuid);
    myVdc->setDsuid(vdcDsuid);

    std::string vdcName = "MusicPlayer vDC";
    myVdc->setName(vdcName);
    m_access.registerVDC(myVdc);
    creation = true;
  }

  if (!m_access.hasDevices()) {
    dsuid_t vdcdDsuid;
    dsuid_generate_v4_random(&vdcdDsuid);

    boost::shared_ptr<MPDVdc> vdsd(new MPDVdc());
    vdsd->setDsuid(vdcdDsuid);
    m_access.registerVDSD(vdsd);

    creation = true;
  }
  return creation;
}
