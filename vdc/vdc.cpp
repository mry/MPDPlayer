/***************************************************************************
                         vdc.cpp  -  description
                         -------------------
    begin                : Fri Sep 23 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "rfw/utils/clogger.h"
#include "vdc.h"
#include <dsvdc/dsvdc.h>
#include "interfacehelper.h"


/***********************************************************************//**
  @method :  VDC
  @comment:  constructor
***************************************************************************/
VDC::VDC() :
    IVDC()
{
}

/***********************************************************************//**
  @method :  ~VDC
  @comment:  destructor
***************************************************************************/
VDC::~VDC()
{
}

/***********************************************************************//**
  @method :  handleVDCProperties
  @comment:  TODO modify for your own needs.
             Example code only
***************************************************************************/
void VDC::handleGetVDCProperties(dsvdc_property_t *property, const dsvdc_property_t *query)
{
    char *name = nullptr;
    for (unsigned int i = 0; i < dsvdc_property_get_num_properties(query); ++i) {

        int ret = dsvdc_property_get_name(query, i, &name);
        if (ret != DSVDC_OK) {
            WARN("handleVDCProperties: error getting property name, abort");
            return;
        }
        if (!name) {
            WARN("handleVDCProperties: not yet handling wildcard properties");
            return;
        }
        INFO("**** request name: " << name);

        if (strcmp(name, "hardwareGuid") == 0) {
            char info[256];
            char buffer[32];

            // TODO
            strcpy(info, "macaddress:");
            sprintf(buffer, "%02x:%02x:%02x:%02x:%02x:%02x",1,2,3,4,5,6); // TODO

            strcat(info, buffer);
            dsvdc_property_add_string(property, name, info);

        } else if (strcmp(name, "modelGuid") == 0) {
            dsvdc_property_add_string(property, name, "MPD VDC Prototype by mry [modelGuid]");

        } else if (strcmp(name, "vendorId") == 0) {
            dsvdc_property_add_string(property, name, "test@test.com");

        } else if (strcmp(name, "name") == 0) {
            dsvdc_property_add_string(property, name, "MPD VDC");

        } else if (strcmp(name, "model") == 0) {
            dsvdc_property_add_string(property, name, "MPD VDC Prototype by mry [model]");

        } else if (strcmp(name, "capabilities") == 0) {
            dsvdc_property_t *reply;
            ret = dsvdc_property_new(&reply);
            if (ret != DSVDC_OK) {
                INFO("failed to allocate reply property for" << name);
                free(name);
                continue;
            }
            dsvdc_property_add_bool(reply, "metering", false);

        } else if (strcmp(name, "configURL") == 0) {
            constexpr size_t LEN = 20;
            char data[LEN];
            InterfaceHelper::GetPrimaryIp(data, LEN);
            std::string output = "http://" + std::string(data) + ":8090";
            dsvdc_property_add_string(property, name, output.c_str());

        } else if (strcmp(name, "zoneID") == 0) {
            dsvdc_property_add_int(property, name, getZoneID());
        } else if (strcmp(name, "modelVersion") == 0) {
            dsvdc_property_add_string(property, name, "0.0.1");
        } else if (strcmp(name, "hardwareVersion") == 0) {
            dsvdc_property_add_string(property, name, "MPD_0.1");
        } else if (strcmp(name, "hardwareModelGuid") == 0) {
            dsvdc_property_add_string(property, name, "MPD_0.1");
        } else if (strcmp(name, "modelUID") == 0) {
            dsvdc_property_add_string(property, name, "MPD_0.1");
        } else if (strcmp(name, "vendorGuid") == 0) {
            dsvdc_property_add_string(property, name, "MPD_0x1");
        } else if (strcmp(name, "oemGuid") == 0) {
            dsvdc_property_add_string(property, name, "TODO");
        } else {
            WARN("Property not supported " << name);
        }

        free(name);
    }

    return;
}

/***********************************************************************//**
  @method :  handleSetVDCProperties
  @comment:  TODO modify for your own needs.
             Example code only
***************************************************************************/
uint8_t VDC::handleSetVDCProperties(dsvdc_property_t * /*property*/, const dsvdc_property_t *properties)
{
    char *name = nullptr;
    for (unsigned int i = 0; i < dsvdc_property_get_num_properties(properties); ++i) {
        int ret = dsvdc_property_get_name(properties, i, &name);

        if (ret != DSVDC_OK) {
            WARN("handleSetVDCProperties: error getting property name, abort");
            free (name);
            return DSVDC_ERR_MISSING_DATA;
        }

        if (!name) {
            WARN("handleSetVDCProperties: not yet handling wildcard properties");
            free (name);
            return DSVDC_ERR_MISSING_DATA;
        }

        INFO("**** request name: " << name);

        if (strcmp(name, "name") == 0) {
            char * out;
            if (DSVDC_OK == dsvdc_property_get_string(properties, i, &out)) {
                std::string newName(out);
                setName(newName);
            }
        } else if (strcmp(name, "model") == 0) {

        } else if (strcmp(name, "zoneID") == 0) {
            int64_t zoneId = 0;
            if (DSVDC_OK == dsvdc_property_get_int(properties, 0, &zoneId)) {
                setZoneID(zoneId);
            }
        } else {
            WARN("Property not supported" << name);
        }
        free (name);
        return DSVDC_OK;
    }
    WARN("Property not supported" << name);
    return DSVDC_ERR_NOT_IMPLEMENTED;
}

/***********************************************************************//**
  @method :  handleIdentify
  @comment:  TODO modify for your own needs.
             Example code only
***************************************************************************/
void VDC::handleIdentify(int32_t group, int32_t zone_id)
{
    INFO("handle identify group: " << group << " zone:" << zone_id);
}
