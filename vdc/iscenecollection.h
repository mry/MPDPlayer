/***************************************************************************
                          iscenecollection.h  -  description
                             -------------------
    begin                : Tue April 18 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ISCENECOLLECTION_H
#define ISCENECOLLECTION_H

#include "scenedescription.h"

class ISceneCollection
{
public:
    ISceneCollection() = default;
    virtual ~ISceneCollection() = default;

    //-----------------------------------------------------------------------
    virtual SceneDescription getSceneDescriptor(uint8_t scene) = 0;
    virtual void setSceneDescriptor(SceneDescription& sceneDesc, uint8_t scene) = 0;
};

#endif // ISCENECOLLECTION_H
