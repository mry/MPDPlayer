/***************************************************************************
                         ivdsd.h  -  description
                         -------------------
    begin                : Fri Sep 23 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************

 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IVDSD_H
#define IVDSD_H

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include <string>
#include <memory>

#include <digitalSTROM/dsuid.h>
#include <dsvdc/dsvdc.h>

#include "dsuidutils.h"
#include "modelsubject.h"

// forward declaration
class IVDCAccess;

class IVDSD
{
    //---------------------------------------------------------------------------
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int /*version*/)
    {
        ar & m_dsuid;
        ar & m_zoneID;
    }
    //---------------------------------------------------------------------------


public:
    typedef enum
    {
        CON_OFFLINE    = 0,
        CON_ANNOUNCING = 1,
        CON_ANNOUNCED  = 2
    } CONNECTION_STATE;

public:
    IVDSD();
    virtual ~IVDSD();

    void modelUpdate();

    void setDsuid(const dsuid_t& dsuid);
    dsuid_t getDsuid() const;

    void setIcon(const uint8_t* icon16png, size_t size, std::string &iconName);

    //callback
    void announceDevice(dsvdc_t *handle, int code, void *arg);

    void checkAnnounce();

    void sigOffline();
    void sigAnnounce();

    CONNECTION_STATE getConnectionState() const;

    static void registerVdcAcces(IVDCAccess* ivdc);
    IVDCAccess* getVdcAccess() const;

    void handleGetVDSDProperties(dsvdc_property_t *property, const dsvdc_property_t * query);
    uint8_t handleSetVDSDProperties(dsvdc_property_t *property, const dsvdc_property_t * properties);

    virtual void handleIdentify(int32_t group, int32_t zone_id);
    virtual void handleSetControl(int32_t value, int32_t group, int32_t zone);
    virtual void handleSetChannelValue(bool apply, double value, int32_t channel);
    virtual void handleSetCallScene(int32_t scene, bool force, int32_t group, int32_t zone);
    virtual void handleSetSaveScene(int32_t scene, int32_t group, int32_t zone);
    virtual void handleSetMinScene(int32_t group, int32_t zone);

    static void setModelUpdater(ModelSubject* updateSubject);

protected:
    void notifyModelChange();
    virtual void process() = 0;

public:
    void vanish();

protected:
    // get property
    virtual void handleGetPrimaryGroup(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetModelFeatures(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetButtonInputDescriptions(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetButtonInputSettings(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetOutputDescription(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetOutputSettings(dsvdc_property_t *property,const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetChannelDescriptions(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetChannelSettings(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetChannelStates(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetBinaryInputDescriptions(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetBinaryInputSettings(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetSensorDescriptions(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetSensorSettings(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetSensorStates(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetBinaryInputStates(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetName(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetModel(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetModelGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetVersionGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetHardwareVersion(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetConfigURL(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetHardwareGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetHardwareModelGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
    virtual void handleGetModelUID(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};

    // set property
    virtual uint8_t handleSetZoneID(dsvdc_property_t* property, const dsvdc_property_t* properties, char* name);
    virtual uint8_t handleSetButtonInputSettings(dsvdc_property_t *property, const dsvdc_property_t * properties, char* name){(void) property, (void)properties; (void) name; return DSVDC_OK;};
    virtual uint8_t handleSetOutputSettings(dsvdc_property_t *property, const dsvdc_property_t * properties, char* name){(void) property, (void)properties; (void) name; return DSVDC_OK;};
    virtual uint8_t handleSetName(dsvdc_property_t *property, const dsvdc_property_t * properties, char* name){(void) property, (void)properties; (void) name; return DSVDC_OK;};

protected:
    virtual void handleDeviceIcon(dsvdc_property_t *property, char* name);
    virtual void handleDeviceIconName(dsvdc_property_t *property, char* name);
    virtual void handleDeviceZoneId(dsvdc_property_t *property, char* name);

private:
    dsuid_t m_dsuid;

    uint8_t * m_icon;
    size_t m_iconSize;
    static IVDCAccess *m_VDCAccess;
    std::string m_iconName;
    CONNECTION_STATE m_conState;
    CONNECTION_STATE m_conState_old;
    bool m_trigAnnounce;
    time_t m_announcingStart;
    int64_t m_zoneID;

    static ModelSubject* m_sModelUpdater;
};

BOOST_SERIALIZATION_ASSUME_ABSTRACT(IVDSD);
BOOST_SERIALIZATION_SHARED_PTR(IVDSD);

#endif // IVDSD_H
