/***************************************************************************
                          cvdcthread.h  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CVDCTHREAD_H
#define CVDCTHREAD_H

#include "rfw/utils/cthread.h"
#include "modelobserver.h"
#include "vdcaccess.h"


class CVDCThread :
        public utils::CThread,
        public ModelObserver
{
    /// this class can not be copied
    UNCOPYABLE(CVDCThread);

public:
    CVDCThread();
    virtual void Update();
    void initialize(std::string& dataDir, std::string& file);

    boost::shared_ptr<SceneDescriptionItf> getSceneItf();

protected:
    virtual void* Run(void * args);

private:
    bool checkCreateVdc();
    void saveData();
    void loadData();

    VDCAccess m_access;
    std::string m_dataDirectory;
    std::string m_persistentFile;
    uint8_t* m_memblock;
    std::streampos m_size;
    ModelSubject m_model;
    std::mutex  m_CreationMutex;
};

#endif // CVDCTHREAD_H
