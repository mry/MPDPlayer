/***************************************************************************
                          cvisitorvdcthread.h  -  description
                             -------------------
    begin                : Mon Jan 23 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CVISITORVDCTHREAD_H
#define CVISITORVDCTHREAD_H

#include "rfw/utils/ctypes.h"
#include "extensions/cvisitor.h"

class ModelObserver;

class CVisitorVdcThread :
    public CVisitor
{
public:
    CVisitorVdcThread(ModelObserver* observer);
    virtual ~CVisitorVdcThread() = default;
    virtual void visitSaveMessage(CSaveMessage * pMsg);

private:
    ModelObserver* m_observer;
};

#endif // CVISITORVDCTHREAD_H
