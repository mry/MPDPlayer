/***************************************************************************
                         vdcaccess.h  -  description
                         -------------------
    begin                : Fri Sep 23 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VDCAccess_H
#define VDCAccess_H

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/list.hpp>
#include <boost/noncopyable.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include <map>
#include <mutex>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <digitalSTROM/dsuid.h>
#include <dsvdc/dsvdc.h>

#include "rfw/utils/clogger.h"

#include "dsuidutils.h"
#include "ivdcaccess.h"
#include "ivdc.h"
#include "vdc.h"
#include "ivdsd.h"
#include "mpdvdc.h"

class VDCAccess :
        public IVDCAccess,
        public boost::noncopyable
{
    //---------------------------------------------------------------------------
    friend class boost::serialization::access;

    template<class Archive>
    void save(Archive & ar, const unsigned int /*version*/) const
    {
        ar.template register_type<MPDVdc>();
        ar.template register_type<VDC>();
        try {
            ar & m_vdc;
            ar & m_vdsd;
        } catch (std::exception& ex) {
            ERR(ex.what());
        }
        INFO("save data succcess");
    }

    template<class Archive>
    void load(Archive & ar, const unsigned int /*version*/)
    {
        ar.template register_type<MPDVdc>();
        ar.template register_type<VDC>();
        try {
            ar & m_vdc;
            ar & m_vdsd;
            INFO("number of devices: " << m_vdsd.size());
        } catch (std::exception& ex) {
            ERR("Exception:" << ex.what());
        }
        if (m_vdc.get()!= nullptr)
        {
            INFO("vdc loaded " << m_vdc->getName());
        }
        INFO("load data. Number of devices:" << m_vdsd.size());
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()

    //---------------------------------------------------------------------------

    public:
    VDCAccess();
    bool initializeLib();
    void registerCallbacks();
    void unregisterCallbacks();

    void sessionnew(dsvdc_t* handle);
    void ping(dsvdc_t* handle , const char* dsuid);
    void sessionend(dsvdc_t* handle);
    void setidentify(dsvdc_t *handle, char **dsuid, size_t n_dsuid, int32_t group, int32_t zone_id);
    void getprop(dsvdc_t* handle, const char* dsuid, dsvdc_property_t* property, const dsvdc_property_t* query);
    void setprop(dsvdc_t* handle, const char* dsuid, dsvdc_property_t* property, const dsvdc_property_t* properties);
    void setcontrol(dsvdc_t* handle, const char* dsuid, int32_t value, int32_t group, int32_t zone);
    void setChannelValue(dsvdc_t* handle, const char* dsuid, bool apply, double value, int32_t channel);
    void setCallScene(dsvdc_t* handle, const char* dsuid , int32_t scene, bool force, int32_t group, int32_t zone_id);
    void setSaveScene(dsvdc_t* handle, const char*  dsuid, int32_t scene, int32_t  group, int32_t  zone_id);
    void setMinScene(dsvdc_t* handle, const char*  dsuid, int32_t group, int32_t zone_id);

public:
    void registerVDC(boost::shared_ptr<IVDC> vdsm);
    virtual boost::shared_ptr<IVDC> getVDC();

    void registerVDSD(boost::shared_ptr<IVDSD> vdc);
    void unregisterVDSD(dsuid_t& vdsdDsuid);

    std::vector<boost::shared_ptr<IVDSD> > getDevices();
    boost::shared_ptr<SceneDescriptionItf> getSceneItf();

    bool hasDevices() const;

    virtual dsvdc_t* getHandle() const;

    void run();
    void shutdown();

private:
    void resetConnectionState();
    void processAnnouncement();

private:
    dsvdc_t* m_handle;
    bool m_ready;

    typedef  std::map<dsuid_t, boost::shared_ptr<IVDSD> > VDSD_COLLECTION;
    typedef  std::map<dsuid_t, boost::shared_ptr<IVDSD> >::iterator VDSD_ITERATOR;
    typedef  std::map<dsuid_t, boost::shared_ptr<IVDSD> >::const_iterator VDSD_CONST_ITERATOR;

    VDSD_COLLECTION m_vdsd;

    std::mutex  m_AccessMutex;
    boost::shared_ptr<IVDC> m_vdc; // only one instance??
};

#endif // VDCAccess_H
