/***************************************************************************
                          mpdvdc.h  -  description
                          -------------------
    begin                : Mon Sep 26 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MPDVDC_H
#define MPDVDC_H

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include <mutex>

#include "rfw/utils/ctypes.h"
#include "rfw/utils/cmessage.h"

#include "audiostatemachine.h"
#include "iscenecollection.h"
#include "ivdsd.h"
#include "scenedescription.h"

#include "extensions/cvolumemessage.h"
#include "extensions/cselectormessage.h"
#include "extensions/cpowermessage.h"
#include "extensions/csavemessage.h"
#include "extensions/cmpdmessage.h"


namespace utils {
typedef SMART_PTR(CMessage) smartMessagePtr;
}

class MPDVdc :
    public IVDSD,
    public SceneDescriptionItf,
    public ISceneCollection
{
    UNCOPYABLE(MPDVdc);

public:
    MPDVdc();
    virtual ~MPDVdc() = default;

    /*-----------------------------------------------------------------------*/
    // class SceneDescriptionItf
    virtual void setScene(utils::uint8_t scene, utils::uint8_t volume, utils::uint8_t channel, utils::uint8_t power);
    virtual void setPlayerScene(utils::uint8_t scene, utils::uint8_t command, std::string playlist, std::string song);
    virtual SceneDescription getScene(utils::uint8_t scene) const;
    virtual utils::int32_t getActiveScene() const;
    virtual void setActiveScene(utils::uint8_t scene);
    virtual std::vector<SceneDescription> getAllScenes() const;
    virtual void setVolume(utils::uint8_t volume);
    virtual utils::uint8_t getVolume() const;
    virtual void setChannel(utils::uint8_t volume);
    virtual utils::uint8_t getChannel() const;
    virtual void setPower(utils::uint8_t power);
    virtual utils::uint8_t getPower() const;

    /*-----------------------------------------------------------------------*/
    static void setGlobalIcon(const uint8_t* icon16png, size_t size, std::string &iconName);
    int getMaxScene() const;

    /*-----------------------------------------------------------------------*/
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int /*version*/)
    {
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(IVDSD);

        for(int i = 0; i < MAX_SCENES; ++i) {
            ar & m_sceneDescription[i];
        }

        ar & m_name;
        ar & m_function;
        ar & m_group;
        ar & m_mode;

        for(int i = 0; i < MAX_GROUPS; ++i) {
            ar & m_groupMember[i];
        }
    }
    //---------------------------------------------------------------------------

protected:
    // get property
    virtual void handleGetPrimaryGroup(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
    virtual void handleGetModelFeatures(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);


    virtual void handleGetOutputDescription(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
    virtual void handleGetOutputSettings(dsvdc_property_t *property,const dsvdc_property_t * query, char* name);
    virtual void handleGetChannelDescriptions(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
    virtual void handleGetChannelSettings(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
    virtual void handleGetChannelStates(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);






    virtual void handleGetName(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
    virtual void handleGetModel(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
    virtual void handleGetModelGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
    virtual void handleGetVersionGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
    virtual void handleGetHardwareVersion(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
    virtual void handleGetConfigURL(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
    virtual void handleGetHardwareGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
    virtual void handleGetHardwareModelGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
    virtual void handleGetModelUID(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);

    // set  property
    virtual uint8_t handleSetButtonInputSettings(dsvdc_property_t *property, const dsvdc_property_t * properties, char* name);
    virtual uint8_t handleSetOutputSettings(dsvdc_property_t *property, const dsvdc_property_t * properties, char* name);
    virtual uint8_t handleSetName(dsvdc_property_t *property, const dsvdc_property_t * properties, char* name);

    virtual void handleSetControl(int32_t value, int32_t group, int32_t zone);
    virtual void handleSetChannelValue(bool apply, double value, int32_t channel);

    virtual void handleSetCallScene(int32_t scene, bool force, int32_t group, int32_t zone);
    virtual void handleSetSaveScene(int32_t scene, int32_t group, int32_t zone);
    virtual void handleSetMinScene(int32_t group, int32_t zone);
    virtual void handleIdentify(int32_t group, int32_t zone_id);

    //---------------------------------------------------------------------------------------------
    virtual SceneDescription getSceneDescriptor(uint8_t scene);
    virtual void setSceneDescriptor(SceneDescription& sceneDesc, uint8_t scene);

protected:
    void initialize();
    virtual void process();
    virtual void handleDeviceIcon(dsvdc_property_t *property, char* name);
    virtual void handleDeviceIconName(dsvdc_property_t *property, char* name);

    void callScene(int scene);

protected:
    void sendSave();

    bool sendTo(utils::uint32_t threadId, utils::smartMessagePtr pMessage);

private:
    static uint8_t* m_icon;
    static size_t m_iconSize;
    static std::string m_iconName;

private:
    enum {
        MAX_SCENES = 70,
        MAX_GROUPS = 64
    };

private:
    std::mutex m_sceneMutex;
    SceneDescription m_sceneDescription[MAX_SCENES];

    std::string m_name;
    uint64_t m_mode;
    uint64_t m_group;
    uint64_t m_function;
    bool m_groupMember[MAX_GROUPS];

    AudioStatemachine m_stm;
};

#endif // MPDVDC_H
