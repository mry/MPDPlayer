/***************************************************************************
                         ivdcaccess.h  -  description
                         -------------------
    begin                : Fri Sep 23 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IVDCACCESS_H
#define IVDCACCESS_H

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <stdlib.h>
#include <memory>

#include <dsvdc/dsvdc.h>

class IVDC;

class IVDCAccess
{
    //---------------------------------------------------------------------------
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        // nop
    }
    //---------------------------------------------------------------------------

public:
    IVDCAccess() = default;
    virtual ~IVDCAccess() = default;
    virtual dsvdc_t* getHandle() const = 0;
    virtual boost::shared_ptr<IVDC> getVDC() = 0;
};

BOOST_SERIALIZATION_ASSUME_ABSTRACT( IVDCAccess );

#endif // IVDCACCESS_H


