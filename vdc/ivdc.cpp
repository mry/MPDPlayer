/***************************************************************************
                         ivdc.cpp  -  description
                         -------------------
    begin                : Fri Sep 23 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ivdc.h"
#include "rfw/utils/clogger.h"

namespace //anonymous namespace
{

/***********************************************************************//**
  @method :  announce_container_cb
  @comment:  static callback function.
             Maps args to IVDC*.
             dsvdc_announce_container must pass this as argument.
  @param  :  handle dsvdc handle
  @param  :  code ?
  @param  :  arg this pointer. See also dsvdc_announce_container
  @param  :  userdata ?
***************************************************************************/
static void announce_container_cb(dsvdc_t *handle, int code, void *arg, void *userdata)
{
    DEBUG1("IVDC.::container announcement returned code:" << code);
    IVDC* vdc = reinterpret_cast<IVDC*>(arg);
    vdc->announceContainer(handle, code, userdata);
}

}; //anonymous namespace

/***********************************************************************//**
  @comment:  static initializer
***************************************************************************/
ModelSubject* IVDC::m_sModelUpdater = nullptr;

/***********************************************************************//**
  @method :  IVDC
  @comment:  constructor
***************************************************************************/
IVDC::IVDC() :
    m_name("default"),
    m_conState(CON_OFFLINE),
    m_conState_old(CON_OFFLINE),
    m_zoneID(0)
{
}

/***********************************************************************//**
  @method :  ~IVDC
  @comment:  destructor
***************************************************************************/
IVDC::~IVDC()
{
}

/***********************************************************************//**
  @method :  announceContainer
  @comment:  called when registration in progress
             see also announce_container_cb
  @param  :  handle dsvdc handle
  @param  :  code ?
  @param  :  userdata ?
***************************************************************************/
void IVDC::announceContainer(dsvdc_t */*handle*/, int /*code*/, void */*arg*/)
{
    DEBUG1("IVDC.::container ANNOUNCED Callback");
    m_conState = CON_ANNOUNCED;
}

/***********************************************************************//**
  @method :  setDsuid
  @comment:  set dsuid of vdc
  @param  :  dsuid input
***************************************************************************/
void IVDC::setDsuid(const dsuid_t& dsuid)
{
    m_dsuid = dsuid;
    notifyModelChange();
}

/***********************************************************************//**
  @method :  getDsuid
  @comment:  get dsuid of vdc
  @return :  dsuid of vdc
***************************************************************************/
dsuid_t IVDC::getDsuid() const
{
    return m_dsuid;
}

/***********************************************************************//**
  @method :  isEqual
  @comment:  compare given dsuid to own dsuid.
  @param  :  dsuid foreign dsuid
  @return :  true if equal
***************************************************************************/
bool IVDC::isEqual(const dsuid_t& dsuid) const
{
    return dsuid_equal(&m_dsuid, &dsuid);
}

/***********************************************************************//**
  @method :  setName
  @comment:  set name of vdc
  @param  :  name input
***************************************************************************/
void IVDC::setName(std::string &name)
{
    m_name = name;
    notifyModelChange();
}

/***********************************************************************//**
  @method :  getName
  @comment:  get name of vdc
  @return :  name of vdc
***************************************************************************/
std::string IVDC::getName() const
{
    return m_name;
}

/***********************************************************************//**
  @method :  getZoneID
  @comment:  get default zone ID of vdc
  @return :  default zone ID of vdc
***************************************************************************/
int IVDC::getZoneID() const
{
    return m_zoneID;
}

/***********************************************************************//**
  @method :  setZoneID
  @comment:  set default zone ID of vdc
  @param  :  default zone ID
***************************************************************************/
void IVDC::setZoneID(int zoneID)
{
    m_zoneID = zoneID;
}

/***********************************************************************//**
  @method :  checkAnnounce
  @comment:  called cyclycally to check announce state
  @param  :  handle handle to pass when registering
***************************************************************************/
void IVDC::checkAnnounce(dsvdc_t *handle)
{
    if (m_conState == CON_OFFLINE) {
        DEBUG1("IVDC.::container OFFLINE");
        char dsuidstring[DSUID_STR_LEN];
        ::dsuid_to_string(&m_dsuid, dsuidstring);
        if (dsvdc_announce_container(handle, dsuidstring, this, announce_container_cb) == DSVDC_OK) {
            m_conState = CON_ANNOUNCING;
        }
    } else if (m_conState == CON_ANNOUNCING) {

        if (m_conState_old !=  m_conState) {
            DEBUG1("IVDC.::container ANNOUNCING");
            m_conState_old = m_conState;
        }
    } else {
        if (m_conState_old !=  m_conState) {
            DEBUG1("IVDC.::container ANNOUNCED");
            m_conState_old = m_conState;
        }
    }
}

/***********************************************************************//**
  @method :  resetAnnounced
  @comment:  reset announce state to unannounced
***************************************************************************/
void IVDC::resetAnnounced()
{
    DEBUG1("IVDC.::container OFFLINE");
    m_conState = CON_OFFLINE;
}

/***********************************************************************//**
  @method :  notifyModelChange
  @comment:  notify that model has changed
***************************************************************************/
void IVDC::notifyModelChange()
{
    DEBUG1("IVDC.::notifyModelChange");
    if (m_sModelUpdater) {
        m_sModelUpdater->notifyModelChange();
    }
}

/***********************************************************************//**
  @method :  setModelUpdater
  @comment:  set model updater
  @param  :  updateSubject model updater
***************************************************************************/
void IVDC::setModelUpdater(ModelSubject* updateSubject)
{
    m_sModelUpdater = updateSubject;
}

