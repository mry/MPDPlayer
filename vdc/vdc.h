/***************************************************************************
                         vdc.h  -  description
                         -------------------
    begin                : Fri Sep 23 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VDC_H
#define VDC_H

#include <boost/noncopyable.hpp>

#include "ivdc.h"

class VDC :
        public IVDC,
        public boost::noncopyable
{
    //---------------------------------------------------------------------------
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int /*version*/)
    {
        ar.template register_type< VDC >();
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(IVDC);
    }
    //---------------------------------------------------------------------------

public:
    VDC();
    virtual ~VDC();
    virtual void handleGetVDCProperties(dsvdc_property_t *property, const dsvdc_property_t *query);
    virtual uint8_t handleSetVDCProperties(dsvdc_property_t *property, const dsvdc_property_t *properties);
    virtual void handleIdentify(int32_t group, int32_t zone_id);
};

#endif // VDC_H
