/***************************************************************************
                          mpdvdc.cpp  -  description
                          -------------------
    begin                : Mon Sep 26 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mpdvdc.h"
#include "vdcgroup.h"
#include "ivdcaccess.h"
#include "interfacehelper.h"
#include "rfw/utils/clogger.h"
#include "rfw/utils/cthreadmanager.h"

#include "extensions/threadids.h"
#include "extensions/cvolumemessage.h"
#include "extensions/cselectormessage.h"
#include "extensions/cpowermessage.h"
#include "extensions/csavemessage.h"
#include "extensions/cmpdmessage.h"

/***********************************************************************//**
  @comment:  static initializer
***************************************************************************/
uint8_t* MPDVdc::m_icon = nullptr;
size_t MPDVdc::m_iconSize = 0;
std::string MPDVdc::m_iconName = "default";

/***********************************************************************//**
  @method :  MPDVdc
  @comment:  constructor
***************************************************************************/
MPDVdc::MPDVdc():
    m_name("ds-player"),
    m_mode(0),
    //m_group(vdcgroups::DEVICE_CLASS_TK), // audio
    m_group(vdcgroups::DEVICE_CLASS_MG), // audio
    m_function(0),
    m_stm(this)
{
    initialize();
}

/***********************************************************************//**
  @method :  setScene
  @comment:  set the scene description for given scene
  @param  :  scene
  @param  :  vomule
  @param  :  channel
  @param  :  power
***************************************************************************/
void MPDVdc::setScene(utils::uint8_t scene,
                      utils::uint8_t volume,
                      utils::uint8_t channel,
                      utils::uint8_t power)
{
    if (scene < MAX_SCENES) {
        std::unique_lock<std::mutex> lock(m_sceneMutex);
        m_sceneDescription[scene].m_volume  = volume;
        m_sceneDescription[scene].m_channel = channel;
        m_sceneDescription[scene].m_power   = power;
    }
    INFO("scene: "    << (int)scene <<
         " volume: "  << (int)m_sceneDescription[scene].m_volume <<
         " channel: " << (int)m_sceneDescription[scene].m_volume <<
         " power: "   << (int)m_sceneDescription[scene].m_power);

    sendSave();
}

/***********************************************************************//**
  @method :  setPlayerScene
  @comment:  set scene to player
  @param  :  scene
  @param  :  command
  @param  :  playlst
  @param  :  song
***************************************************************************/
void MPDVdc::setPlayerScene(utils::uint8_t scene, utils::uint8_t command, std::string playlist, std::string song)
{
    if (scene < MAX_SCENES) {
        std::unique_lock<std::mutex> lock(m_sceneMutex);
        m_sceneDescription[scene].m_playlist  = playlist;
        m_sceneDescription[scene].m_song = song;

        SCENE_PLAYER_CMD_e SCP_Cmd;
        if (command > SCP_STOP_e) {
            SCP_Cmd = SCP_NOP_e;
        } else {
            SCP_Cmd = (SCENE_PLAYER_CMD_e) command;
        }
        m_sceneDescription[scene].m_mpdCommand = SCP_Cmd;
    }

    INFO("scene: "      << (int)scene <<
         " playlist: "  << m_sceneDescription[scene].m_playlist <<
         " song: "      << m_sceneDescription[scene].m_song <<
         " command: "   << (int)m_sceneDescription[scene].m_mpdCommand);
    sendSave();
}

/***********************************************************************//**
  @method :  getScene
  @comment:  get the scene description for given scene
  @param  :  scene
  @param  :  scene description
***************************************************************************/
SceneDescription MPDVdc::getScene(utils::uint8_t scene) const
{
    if (scene >= MAX_SCENES) {
        scene = MAX_SCENES-1;
    }
    //std::unique_lock<std::mutex> lock(m_sceneMutex);
    return m_sceneDescription[scene];
}

/***********************************************************************//**
  @method :  getActiveScene
  @comment:  get active scene
  @param  :  scene
  @param  :  scene description
***************************************************************************/
utils::int32_t MPDVdc::getActiveScene() const
{
    return m_stm.getActiveScene();
}

/***********************************************************************//**
  @method :  setActiveScene
  @comment:  set active scene
  @param  :  scene
***************************************************************************/
void MPDVdc::setActiveScene(utils::uint8_t scene)
{
    m_stm.setActiveScene(scene);
}

/***********************************************************************//**
  @method :  getAllScenes
  @comment:  get all scenes
  @return :  array of all scenes
***************************************************************************/
std::vector<SceneDescription> MPDVdc::getAllScenes() const
{
    std::vector<SceneDescription> scenes;
    for (int i = 0; i < MAX_SCENES; ++i) {
        scenes.push_back(m_sceneDescription[i]);
    }
    return scenes;
}

/***********************************************************************//**
  @method :  setVolume
  @comment:  set volume
  @param  :  volume
***************************************************************************/
void MPDVdc::setVolume(utils::uint8_t volume)
{
    m_stm.setVolume(volume);
}

/***********************************************************************//**
  @method :  getVolume
  @comment:  get volume
  @return :  volume
***************************************************************************/
utils::uint8_t MPDVdc::getVolume() const
{
    return m_stm.getVolume();
}

/***********************************************************************//**
  @method :  setChannel
  @comment:  set channel
  @param  :  channel
***************************************************************************/
void MPDVdc::setChannel(utils::uint8_t channel)
{
    m_stm.setChannel(channel);
}

/***********************************************************************//**
  @method :  getChannel
  @comment:  get channel
  @return :  channel
***************************************************************************/
utils::uint8_t MPDVdc::getChannel() const
{
    return m_stm.getChannel();
}

/***********************************************************************//**
  @method :  setPower
  @comment:  set power
  @param  :  power id
***************************************************************************/
void MPDVdc::setPower(utils::uint8_t power)
{
    m_stm.setPower(power);
}

/***********************************************************************//**
  @method :  getPower
  @comment:  get power
  @return :  power id
***************************************************************************/
utils::uint8_t MPDVdc::getPower() const
{
    return m_stm.getPower();
}

/***********************************************************************//**
  @method :  setGlobalIcon
  @comment:  set the global icon for this vdc client
  @param  :  icon16png pointer to array
  @param  :  size size of array
  @param  : iconName name of icon
***************************************************************************/
void MPDVdc::setGlobalIcon(const uint8_t* icon16png, size_t size, std::string &iconName)
{
    if (m_icon) {
        free (m_icon);
        m_icon = nullptr;
    }
    m_icon = (uint8_t*)malloc(size);
    memcpy(m_icon, icon16png, size);
    m_iconSize = size;
    m_iconName = iconName;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::process()
{
    //DEBUG2("process");
}

#if 0
/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
std::string MPDVdc::getOutputSceneValue(int scene) const
{
    if ((scene < MAX_SCENES) && (scene>=0)) {
        return m_value[scene];
    }
    std::string data;
    return data;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::setOutputSceneValue(std::string& data, int scene)
{
    if ((scene < MAX_SCENES) && (scene>=0)) {
        m_value[scene] = data;
    }
}
#endif

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::callScene(int scene)
{
    if ((scene>=0) && (scene < MAX_SCENES)) {
        time_t now = time(nullptr);
        dsvdc_property_t* pushEnvelope;
        dsvdc_property_t* propState;
        dsvdc_property_t* prop;

        if (dsvdc_property_new(&prop) != DSVDC_OK) {
            WARN("process failed to alloceate property");
            return;
        }
        dsvdc_property_new(&pushEnvelope);
        dsvdc_property_new(&propState);

        dsvdc_property_add_uint(prop,   "actionId", scene);
        dsvdc_property_add_uint(prop,   "actionMode", 1); // ACT_FORCE = 1,
        dsvdc_property_add_double(prop, "age", now);
        dsvdc_property_add_uint(prop,   "error", 0);

        dsvdc_property_add_property(propState, "0", &prop);

        dsvdc_property_add_property(pushEnvelope, "buttonInputStates", &propState);

        char dsuidstringVdcd[DSUID_STR_LEN];
        dsuid_t temp  = getDsuid();
        ::dsuid_to_string(&temp , dsuidstringVdcd);

        dsvdc_push_property(getVdcAccess()->getHandle(), dsuidstringVdcd, pushEnvelope);
        dsvdc_property_free(pushEnvelope);
    }
}


/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
int MPDVdc::getMaxScene() const
{
    return MPDVdc::MAX_SCENES;
}


/***********************************************************************//**
  @method :  initialize
  @comment:  called from constructors
***************************************************************************/
void MPDVdc::initialize()
{
    for (int i = 0; i < MAX_GROUPS; ++i) {
        m_groupMember[i] = false;
    }
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleGetPrimaryGroup(dsvdc_property_t* property, const dsvdc_property_t* query, char* name)
{
    UNUSED(query);
    //dsvdc_property_add_uint(property, name, vdcgroups::DEVICE_CLASS_TK); // audio
    dsvdc_property_add_uint(property, name, vdcgroups::DEVICE_CLASS_SW); // audio
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleGetModelFeatures(dsvdc_property_t* property, const dsvdc_property_t* query, char* name)
{
    UNUSED(query);
    dsvdc_property_t *reply;
    int ret = dsvdc_property_new(&reply);
    if (ret != DSVDC_OK) {
        WARN("failed to allocate reply property for " << name);
        return;
    }

    dsvdc_property_add_bool(reply, "blink" ,      true);
    //dsvdc_property_add_bool(reply, "outmodegeneric", true);
    //dsvdc_property_add_bool(reply, "jokerconfig", true);
    dsvdc_property_add_bool(reply, "dontcare",    true);
    dsvdc_property_add_bool(reply, "outmode",     true);
    dsvdc_property_add_bool(reply, "outvalue8",   true);
    dsvdc_property_add_bool(reply, "transt",      true);

    dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleGetVersionGuid(dsvdc_property_t* property, const dsvdc_property_t* query, char* name)
{
    UNUSED(query);
    dsvdc_property_add_string(property, name, "0.0.1");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleGetHardwareVersion(dsvdc_property_t* property, const dsvdc_property_t* query, char* name)
{
    UNUSED(query);
    dsvdc_property_add_string(property, name, "1.0.0");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleGetConfigURL(dsvdc_property_t* property, const dsvdc_property_t* query, char* name)
{
    UNUSED(property);
    UNUSED(query);
    UNUSED(name);

    static const size_t LEN = 20;
    char data[LEN];
    InterfaceHelper::GetPrimaryIp(data, LEN);
    std::string output = "http://" + std::string(data) + ":8090";
    dsvdc_property_add_string(property, name, output.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleGetHardwareGuid(dsvdc_property_t* property, const dsvdc_property_t* query, char* name)
{
    UNUSED(query);

    std::string guid = "guid:";
    dsuid_t dsuid = getDsuid();
    char data[DSUID_STR_LEN];
    dsuid_to_string(&dsuid,data);
    guid += data;
    dsvdc_property_add_string(property, name, guid.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleGetHardwareModelGuid(dsvdc_property_t* property, const dsvdc_property_t* query, char* name)
{
    UNUSED(query);

    std::string guid = "MPD Vdc:";
    dsuid_t dsuid = getDsuid();
    char data[DSUID_STR_LEN];
    dsuid_to_string(&dsuid,data);
    guid += data;
    dsvdc_property_add_string(property, name, guid.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleGetModelUID(dsvdc_property_t* property, const dsvdc_property_t* query, char* name)
{
    UNUSED(query);

    dsuid_t dsuid = getDsuid();
    char data[DSUID_STR_LEN];
    dsuid_to_string(&dsuid, data);
    dsvdc_property_add_string(property, name, data); // TODO
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleGetName(dsvdc_property_t* property, const dsvdc_property_t* query, char* name)
{
    UNUSED(query);

    dsvdc_property_add_string(property, name, m_name.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleGetModel(dsvdc_property_t* property, const dsvdc_property_t* query, char* name)
{
    UNUSED(query);

    std::string info("Output");
    dsvdc_property_add_string(property, name, info.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleGetModelGuid(dsvdc_property_t* property, const dsvdc_property_t* query, char* name)
{
    UNUSED(query);

    dsvdc_property_add_string(property, name, "MPD Vdc:Output");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleGetOutputDescription(dsvdc_property_t* property, const dsvdc_property_t* query, char* name)
{
    UNUSED(query);

    dsvdc_property_t *reply;
    int ret = dsvdc_property_new(&reply);
    if (ret != DSVDC_OK) {
        WARN("failed to allocate reply property for " << name);
        return;
    }

    dsvdc_property_add_string(reply, "name",         "audioPlayer");
    dsvdc_property_add_int(reply,    "function",     1);        // 0: on off only
    dsvdc_property_add_int(reply,    "outputUsage",  0);        // 0: undefined
    dsvdc_property_add_bool(reply,   "variableRamp", false);    // no ramp
    dsvdc_property_add_string(reply, "type",         "output"); // really needed?

    dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleGetOutputSettings(dsvdc_property_t *property,const dsvdc_property_t * query, char* name)
{
    UNUSED(query);

    dsvdc_property_t *reply;
    int ret = dsvdc_property_new(&reply);
    if (ret != DSVDC_OK) {
        WARN("failed to allocate reply property for " << name);
        return;
    }

    dsvdc_property_t *nGroup;
    if (dsvdc_property_new(&nGroup) != DSVDC_OK) {
        WARN("failed to allocate reply property for " << name);
        dsvdc_property_free(reply);
        return;
    }

    dsvdc_property_add_uint(reply, "mode", m_mode);
    dsvdc_property_add_bool(reply, "pushChanges", false);

    // groups
    for (unsigned int i = 0; i < MAX_GROUPS; ++i) {
        if (m_groupMember[i]) {
            dsvdc_property_add_bool(nGroup, std::to_string(i).c_str(), true); // light  //TODO
        } else {
            if (m_group == i) {
                dsvdc_property_add_bool(nGroup, std::to_string(i).c_str(), true);
            }
        }
    }
    dsvdc_property_add_property(reply, "groups", &nGroup);

    dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleGetChannelDescriptions(dsvdc_property_t* property, const dsvdc_property_t* query, char* name)
{
    UNUSED(query);

    dsvdc_property_t *reply;
    int ret = dsvdc_property_new(&reply);
    if (ret != DSVDC_OK) {
        WARN("failed to allocate reply property for " << name);
        return;
    }

    dsvdc_property_t *nProp;
    if (dsvdc_property_new(&nProp) != DSVDC_OK) {
        WARN("failed to allocate reply property for " << name);
        dsvdc_property_free(reply);
        return;
    }
    dsvdc_property_add_uint(nProp,     "channelIndex", 0);
    dsvdc_property_add_double(nProp,   "max",          100.0);
    dsvdc_property_add_double(nProp,   "min",          0.0);
    dsvdc_property_add_string(nProp,   "name",         "MPDVdc");
    dsvdc_property_add_double(nProp,   "resolution" ,  1.0);
    dsvdc_property_add_property(reply, "0",            &nProp);
    dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleGetChannelSettings(dsvdc_property_t *property, const dsvdc_property_t * query, char* name)
{
    UNUSED(property);
    UNUSED(query);
    UNUSED(name);
    /* nop: no channel settings defined */
    INFO("handleGetChannelSettings");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleGetChannelStates(dsvdc_property_t *property, const dsvdc_property_t * query, char* name)
{
    dsvdc_property_t *reply;
    int ret = dsvdc_property_new(&reply);
    if (ret != DSVDC_OK) {
        WARN("failed to allocate reply property for " << name);
        return;
    }

    dsvdc_property_t *nProp;
    if (dsvdc_property_new(&nProp) != DSVDC_OK) {
        WARN("failed to allocate reply property for " << name);
        dsvdc_property_free(reply);
        return;
    }
    //dsvdc_property_add_double(nProp, "age", 0);
    dsvdc_property_add_double(nProp, "value", m_stm.getVolume());

    dsvdc_property_add_property(reply, "0", &nProp);
    dsvdc_property_add_property(property, name, &reply);
    INFO("Request channel value: " << static_cast<utils::uint32_t>(m_stm.getVolume()));
}


/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
uint8_t MPDVdc::handleSetButtonInputSettings(dsvdc_property_t *property, const dsvdc_property_t * properties, char* name)
{
    UNUSED(property);

    dsvdc_property_t *buttonInputProp;
    dsvdc_property_get_property_by_name(properties, name, &buttonInputProp);

    dsvdc_property_t *indexProperty;
    dsvdc_property_get_property_by_name(buttonInputProp, "0", &indexProperty);

    for (size_t i = 0; i < dsvdc_property_get_num_properties(indexProperty); i++) {
        char* propName = nullptr;
        if (DSVDC_OK != dsvdc_property_get_name(indexProperty, i, &propName)) {
            continue;
        }
        if (0 == strcmp(propName, "function")) {
            dsvdc_property_get_uint(indexProperty, i, &m_function);
        }
        else if (0 == strcmp(propName, "group")) {
            dsvdc_property_get_uint(indexProperty, i, &m_group);
        }
    }
    notifyModelChange();
    return DSVDC_OK;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
uint8_t MPDVdc::handleSetOutputSettings(dsvdc_property_t *property, const dsvdc_property_t * properties, char* name)
{
    UNUSED(property);

    dsvdc_property_t *outputSettings;
    dsvdc_property_get_property_by_name(properties, name, &outputSettings);

    dsvdc_property_t *modeProperty;

    int retVal = dsvdc_property_get_property_by_name(outputSettings, "mode", &modeProperty);
    if (DSVDC_OK == retVal) {
        dsvdc_property_get_uint(outputSettings, 0, &m_mode);
    }

    dsvdc_property_t *groupsProperty;
    int groupIndx = 0;
    bool groupSet = false;
    retVal = dsvdc_property_get_property_by_name(outputSettings, "groups", &groupsProperty);
    if (DSVDC_OK == retVal) {
        for (size_t i = 0; i < dsvdc_property_get_num_properties(groupsProperty); i++) {

            char* propName = nullptr;
            if (DSVDC_OK != dsvdc_property_get_name(groupsProperty, i, &propName)) {
                continue;
            }
            groupIndx = std::stoi(propName);

            if (groupIndx >= MAX_GROUPS) {
                continue;
            }

            if (DSVDC_OK != dsvdc_property_get_name(groupsProperty, i, &propName)) {
                continue;
            }

            if (DSVDC_OK == dsvdc_property_get_bool(groupsProperty, i, &groupSet)) {
                m_groupMember[groupIndx] = groupSet;
            }
        }
    }
    INFO("MPDVdc::handleSetOutputSettings mode: " << m_mode <<
         " group: " << groupIndx << " value: " << groupSet);
    notifyModelChange();
    return retVal;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
uint8_t MPDVdc::handleSetName(dsvdc_property_t *property, const dsvdc_property_t * properties, char* name)
{
    UNUSED(property);
    UNUSED(name);

    char* data;
    dsvdc_property_get_string(properties,0,&data);
    m_name = data;
    DEBUG2("MPDVdc::handleSetName: "  << m_name);

    notifyModelChange();
    return DSVDC_OK;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleSetControl(int32_t value, int32_t group, int32_t zone)
{
    // TODO
    INFO("setControl:value: " << value <<
         "group:"  << group << " zone: " << zone);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleSetChannelValue(bool apply, double value, int32_t channel)
{
    m_stm.handleSetChannelValue(apply, value, channel);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleSetCallScene(int32_t scene, bool force, int32_t group, int32_t zone)
{
    m_stm.handleSetCallScene(scene, force, group,zone);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleSetSaveScene(int32_t scene, int32_t group, int32_t zone)
{
    m_stm.handleSetSaveScene(scene, group, zone);
    notifyModelChange();
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleSetMinScene(int32_t group, int32_t zone)
{
    m_stm.handleSetMinScene(group,zone);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleIdentify(int32_t /*group*/, int32_t /*zone_id*/)
{
    DEBUG2("MPDVdc::handleIdentify");
    m_stm.setVolume(m_stm.getVolume());
}

SceneDescription MPDVdc::getSceneDescriptor(uint8_t scene)
{
    if (scene < MAX_SCENES) {
        std::unique_lock<std::mutex> lock(m_sceneMutex);
        return m_sceneDescription[scene];
    }
    ERR("scene out of range");
    return SceneDescription();
}

void MPDVdc::setSceneDescriptor(SceneDescription& sceneDesc, uint8_t scene)
{
    if (scene >= MAX_SCENES) {
        ERR("scene out of range");
        return;
    }
    std::unique_lock<std::mutex> lock(m_sceneMutex);
    m_sceneDescription[scene] = sceneDesc;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleDeviceIcon(dsvdc_property_t *property, char* name)
{
    DEBUG2("MPDVdc::handleDeviceIcon");
    dsvdc_property_add_bytes(property, name, m_icon, m_iconSize);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void MPDVdc::handleDeviceIconName(dsvdc_property_t *property, char* name)
{
    DEBUG2("MPDVdc::handleDeviceIconName");
    dsvdc_property_add_string(property, name, m_iconName.c_str());
}

/***********************************************************************//**
  @method :  sendSave
  @comment:  send save
***************************************************************************/
void MPDVdc::sendSave()
{
    SMART_PTR(CSaveMessage) pMsg = utils::CREATE_SMART_PTR(CSaveMessage);
    sendTo(threadIds::MPDVdc, pMsg);
}

/***********************************************************************//**
  @method :  sendTo
  @comment:  send message to thread
  @param  :  threadId receiver
  @param  :  pMessage message to send
***************************************************************************/
bool MPDVdc::sendTo(utils::uint32_t threadId, utils::smartMessagePtr pMessage)
{
    return utils::CThreadManager::GetInstance().Send(0, threadId, pMessage);
}
