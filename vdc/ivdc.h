/***************************************************************************
                         ivdc.h  -  description
                         -------------------
    begin                : Fri Sep 23 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IVDSM_H
#define IVDSM_H

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include <string>
#include <memory>

#include <digitalSTROM/dsuid.h>
#include <dsvdc/dsvdc.h>

#include "modelsubject.h"

class IVDC :
        public std::enable_shared_from_this <IVDC>
{
    //---------------------------------------------------------------------------
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int /*version*/)
    {
        ar & m_name;
        ar & m_dsuid;
        ar & m_zoneID;
    }
    //---------------------------------------------------------------------------


public:
    typedef enum
    {
        CON_OFFLINE    = 0,
        CON_ANNOUNCING = 1,
        CON_ANNOUNCED  = 2
    } CONNECTION_STATE;

    IVDC();
    virtual ~IVDC();

    std::shared_ptr<IVDC> getptr()
    {
        return shared_from_this();
    };

    void setDsuid(const dsuid_t& dsuid);
    dsuid_t getDsuid() const;

    bool isEqual(const dsuid_t& dsuid) const;

    void setName(std::string &name);
    std::string getName() const;

    void announceContainer(dsvdc_t *handle, int code, void *arg);
    void checkAnnounce(dsvdc_t *handle);
    void resetAnnounced();

    void setZoneID(int zoneID);
    int getZoneID() const;

    virtual void handleGetVDCProperties(dsvdc_property_t *property, const dsvdc_property_t *query) = 0;
    virtual uint8_t handleSetVDCProperties(dsvdc_property_t *property, const dsvdc_property_t *properties) = 0;
    virtual void handleIdentify(int32_t group, int32_t zone_id) = 0;

    static void setModelUpdater(ModelSubject* updateSubject);

protected:
    void notifyModelChange();

private:
    dsuid_t m_dsuid;
    std::string m_name;

    CONNECTION_STATE m_conState;
    CONNECTION_STATE m_conState_old;

    int m_zoneID;

    static ModelSubject* m_sModelUpdater;
};

BOOST_SERIALIZATION_ASSUME_ABSTRACT(IVDC);

#endif // IVDC_H
