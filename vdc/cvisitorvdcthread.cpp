/***************************************************************************
                          cvisitorvdcthread.cpp  -  description
                             -------------------
    begin                : Mon Jan 23 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cvisitorvdcthread.h"
#include "modelobserver.h"
#include "rfw/utils/clogger.h"

/***********************************************************************//**
 @method : CVisitorVdcThread
 @comment: constructor
 @param  : observer
 ***************************************************************************/
CVisitorVdcThread::CVisitorVdcThread(ModelObserver* observer) :
m_observer(observer)
{}

/***********************************************************************//**
 @method : visitSaveMessage
 @comment: save message
 @param  : pMsg
 ***************************************************************************/
void CVisitorVdcThread::visitSaveMessage(CSaveMessage * pMsg)
{
    DEBUG2("visitSaveMessage");
    m_observer->Update();
}
