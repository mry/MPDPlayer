/***************************************************************************
                         dsuidutils.h  -  description
                         -------------------
    begin                : Fri Sep 23 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef DSUIDSERIALIZE_H
#define DSUIDSERIALIZE_H

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <string.h>

#include <digitalSTROM/dsuid.h>

#define UNUSED(expr) do { (void)(expr); } while (0)

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive& ar, dsuid_t& g, const unsigned int /*version*/)
{
    for(int i = 0; i < DSUID_SIZE; ++i) {
        ar & g.id[i];
    }
}

} // namespace serialization
} // namespace boost

namespace std
{
template<> struct less<dsuid_t>
{
    bool operator() (const dsuid_t& lhs, const dsuid_t& rhs)
    {
        char temp1[DSUID_STR_LEN];
        ::dsuid_to_string(&lhs, temp1);

        char temp2[DSUID_STR_LEN];
        ::dsuid_to_string(&rhs, temp2);

        return (strcmp(temp1, temp2) > 0);
    }
};
};

#endif // DSUIDSERIALIZE_H

