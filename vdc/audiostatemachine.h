/***************************************************************************
                          audiostatemachine.h  -  description
                             -------------------
    begin                : Fri April 14 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef AUDIOSTATEMACHINE_H
#define AUDIOSTATEMACHINE_H

#include "rfw/utils/ctypes.h"

#include "iscenecollection.h"
#include "scenedescription.h"

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"

namespace utils {
typedef SMART_PTR(CMessage) smartMessagePtr;
}

class AudioStatemachine
{
public:
    AudioStatemachine(ISceneCollection* sceneDescIf);
    virtual ~AudioStatemachine() = default;

    void setScene(int32_t scene);
    int32_t getScene() const;

    void handleSetCallScene(utils::int32_t scene, bool force,
                            utils::int32_t group, utils::int32_t zone);

    void handleSetChannelValue(bool apply, double value,
                               utils::int32_t channel);

    void handleSetSaveScene(utils::int32_t scene, utils::int32_t group,
                            utils::int32_t zone);

    void handleSetMinScene(utils::int32_t group, utils::int32_t zone);

    void setActiveScene(utils::uint8_t scene);
    utils::int32_t getActiveScene() const;

    void setVolume(utils::uint8_t volume);
    utils::uint8_t getVolume() const;
    void setChannel(utils::uint8_t channel);
    utils::uint8_t getChannel() const;
    void setPower(utils::uint8_t power);
    utils::uint8_t getPower() const;

private:
    void sendVolume(utils::uint8_t vol);
    void sendChannel(utils::uint8_t channel);
    void sendPower(uint8_t pwrBitset);
    void sendStartSong(std::string playlist, std::string song);
    void sendStopSong();
    void sendPrevSong();
    void sendNextSong();
    void sendPlay();
    void sendPause();

    bool sendTo(utils::uint32_t threadId, utils::smartMessagePtr pMessage);

    bool isCommand(utils::int32_t scene);
    void processCommand(int32_t scene, bool force, int32_t group, int32_t zone);

    void incVolume();
    void decVolume();

private:
    int32_t m_scene;
    int32_t m_oldCommand;

    utils::uint8_t m_tempVolumeValue;
    utils::uint8_t m_tempChannelValue;
    utils::uint8_t m_tempPowerValue;

    ISceneCollection* m_IfMpdVdc;
};

#endif // AUDIOSTATEMACHINE_H
