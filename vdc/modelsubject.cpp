/***************************************************************************
                         modelsubject.cpp  -  description
                         -------------------
    begin                : Fri Sep 23 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "modelsubject.h"
#include "modelobserver.h"

#include <algorithm>
#include <boost/foreach.hpp>

/***********************************************************************//**
  @method : Factory
  @comment: constructor
***************************************************************************/
ModelSubject::ModelSubject()
{
}

/***********************************************************************//**
  @method : Factory
  @comment: constructor
***************************************************************************/
ModelSubject::~ModelSubject()
{
}

/***********************************************************************//**
  @method : Factory
  @comment: constructor
***************************************************************************/
void ModelSubject::notifyModelChange()
{
    BOOST_FOREACH (ModelObserver* observer, m_list){
        observer->Update();
    }
}

/***********************************************************************//**
  @method : attach
  @comment: add observer
  @param  : observer
***************************************************************************/
void ModelSubject::attach(ModelObserver* observer)
{
    m_list.push_back(observer);
}

/***********************************************************************//**
  @method : detach
  @comment: remove element
  @param  : observer
***************************************************************************/
void ModelSubject::detach(ModelObserver* observer)
{
    m_list.erase(std::remove(m_list.begin(), m_list.end(), observer), m_list.end());
}
