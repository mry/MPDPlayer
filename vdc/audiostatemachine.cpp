/***************************************************************************
                          audiostatemachine.cpp  -  description
                             -------------------
    begin                : Fri April 14 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "audiostatemachine.h"

#include "rfw/utils/clogger.h"
#include "rfw/utils/cthreadmanager.h"

#include "extensions/threadids.h"
#include "extensions/cvolumemessage.h"
#include "extensions/cselectormessage.h"
#include "extensions/cpowermessage.h"
#include "extensions/csavemessage.h"
#include "extensions/cmpdmessage.h"

/***********************************************************************//**
  enum special commands
  @comment: scene call with special meaning
***************************************************************************/
enum
{
    CMD_REPEAT_OFF  = 7,
    CMD_REPEAT      = 8,
    CMD_REPEAT_ALL  = 9,
    CMD_DIM_CONT    = 10,
    CMD_DIM_UP      = 11,
    CMD_DIM_DOWN    = 12,
    CMD_MIN         = 13,
    CMD_MAX         = 14,
    CMD_STOP        = 15,
    CMD_PREV        = 42,
    CMD_NEXT        = 43,
    CMD_PREV_CHAN   = 44,
    CMD_NEXT_CHAN   = 45,
    CMD_MUTE        = 46,
    CMD_UNMUTE      = 47,
    CMD_PLAY        = 48,
    CMD_PAUSE       = 49,
    CMD_SHUFFLE_OFF = 52,
    CMD_SHUFFLE_ON  = 53,
    CMD_RESUME_OFF  = 54,
    CMD_RESUME_ON   = 55,
};

/***********************************************************************//**
  @method : AudioStatemachine
  @comment: constructor
  @param  :  sceneDescIf
***************************************************************************/
AudioStatemachine::AudioStatemachine(ISceneCollection* sceneDescIf) :
    m_scene(0),
    m_oldCommand(0),
    m_tempVolumeValue(0),
    m_tempChannelValue(0),
    m_tempPowerValue(0),
    m_IfMpdVdc(sceneDescIf)
{
}

/***********************************************************************//**
  @method : setScene
  @comment: set scene
  @param  :  scene
***************************************************************************/
void AudioStatemachine::setScene(int32_t scene)
{
    m_scene = scene;
}

/***********************************************************************//**
  @method : getScene
  @comment: get scene
  @return : scene
***************************************************************************/
int32_t AudioStatemachine::getScene() const
{
    return m_scene;
}

/***********************************************************************//**
  @method : handleSetCallScene
  @comment: set call scene
  @param :  scene
  @param : force
  @param : group
  @param : zone
***************************************************************************/
void AudioStatemachine::handleSetCallScene(int32_t scene, bool force, int32_t group, int32_t zone)
{
    if (!isCommand(scene)) {
        m_scene = scene;
        SceneDescription sceneDesc = m_IfMpdVdc->getSceneDescriptor(m_scene);

        m_tempVolumeValue  = sceneDesc.m_volume;
        m_tempChannelValue = sceneDesc.m_channel;
        m_tempPowerValue   = sceneDesc.m_power;

        INFO("handleSetCallScene: scene: " << scene <<
               " force: " << force << " group: " << group <<
               " zone: " << zone <<
               " volume: "  << static_cast<utils::uint32_t>(m_tempVolumeValue) <<
               " channel: " << static_cast<utils::uint32_t>(m_tempChannelValue) <<
               " power: "   << static_cast<utils::uint32_t>(m_tempPowerValue));

        sendPower(m_tempPowerValue);
        sendChannel(m_tempChannelValue);
        sendVolume(m_tempVolumeValue);

        // TODO stm
        SCENE_PLAYER_CMD_e cmd = sceneDesc.m_mpdCommand;
        if (cmd == SCP_PLAY_e) {
            sendStartSong(sceneDesc.m_playlist, sceneDesc.m_song);
        } else if (cmd == SCP_STOP_e) {
            sendStopSong();
        }
    } else {
        processCommand(scene, force, group, zone);
    }
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void AudioStatemachine::handleSetChannelValue(bool apply, double value, int32_t channel)
{
    INFO("handleSetChannelValue:apply: " << apply <<
         " value: " << static_cast<utils::uint32_t>(value) <<
         " channel: "  << channel);
    m_tempVolumeValue = static_cast<utils::uint8_t>(value);
    if (apply) {
        sendVolume(m_tempVolumeValue);
    }
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void AudioStatemachine::handleSetSaveScene(int32_t scene, int32_t group, int32_t zone)
{
    m_scene = scene;

    SceneDescription sceneDesc = m_IfMpdVdc->getSceneDescriptor(m_scene);
    sceneDesc.m_volume = m_tempVolumeValue;
    m_IfMpdVdc->setSceneDescriptor(sceneDesc, m_scene);

    DEBUG1("handleSetSaveScene: scene: " << scene <<
           " group: " << group <<
           " zone: " << zone <<
           "value: " << static_cast<utils::uint32_t>(m_tempVolumeValue));

    sendVolume(m_tempVolumeValue);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void AudioStatemachine::handleSetMinScene(int32_t group, int32_t zone)
{
    m_scene = 0;
    SceneDescription sceneDesc = m_IfMpdVdc->getSceneDescriptor(m_scene);
    m_tempVolumeValue = sceneDesc.m_volume;
    DEBUG1("handleSetMinScene: " << group <<
           " zone: " << zone <<
           "value: " << static_cast<utils::uint32_t>(m_tempVolumeValue));
    sendVolume(m_tempVolumeValue);
}

/***********************************************************************//**
  @method :  setActiveScene
  @comment:  set active scene
  @param  :  scene
***************************************************************************/
void AudioStatemachine::setActiveScene(utils::uint8_t scene)
{
    m_scene = scene;

    SceneDescription sceneDesc = m_IfMpdVdc->getSceneDescriptor(m_scene);
    m_tempVolumeValue = sceneDesc.m_volume;
    m_tempChannelValue = sceneDesc.m_channel;
    m_tempPowerValue = sceneDesc.m_volume;

    sendPower(m_tempPowerValue);
    sendChannel(m_tempChannelValue);
    sendVolume(m_tempVolumeValue);
}

/***********************************************************************//**
  @method : getActiveScene
  @comment: get active scene
  @return : scene
***************************************************************************/
utils::int32_t AudioStatemachine::getActiveScene() const
{
    return m_scene;
}

/***********************************************************************//**
  @method :  setVolume
  @comment:  set volume
  @param  :  volume
***************************************************************************/
void AudioStatemachine::setVolume(utils::uint8_t volume)
{
    m_tempVolumeValue = volume;
    sendVolume(m_tempVolumeValue);
}

/***********************************************************************//**
  @method :  getVolume
  @comment:  get volume
  @return :  volume
***************************************************************************/
utils::uint8_t AudioStatemachine::getVolume() const
{
    return m_tempVolumeValue;
}

/***********************************************************************//**
  @method :  setChannel
  @comment:  set channel
  @param  :  channel
***************************************************************************/
void AudioStatemachine::setChannel(utils::uint8_t channel)
{
    m_tempChannelValue = channel;
    sendChannel(m_tempChannelValue);
}

/***********************************************************************//**
  @method :  getChannel
  @comment:  get channel
  @return :  channel
***************************************************************************/
utils::uint8_t AudioStatemachine::getChannel() const
{
    return m_tempChannelValue;
}

/***********************************************************************//**
  @method :  setPower
  @comment:  set power
  @param  :  power id
***************************************************************************/
void AudioStatemachine::setPower(utils::uint8_t power)
{
    m_tempPowerValue = power;
    sendPower(m_tempPowerValue);
}

/***********************************************************************//**
  @method :  getPower
  @comment:  get power
  @return :  power id
***************************************************************************/
utils::uint8_t AudioStatemachine::getPower() const
{
    return m_tempPowerValue;
}

/***********************************************************************//**
  @method :  sendVolume
  @comment:  send volume
  @param  :  vol input{0..100} will be stretched to {0..255}
***************************************************************************/
void AudioStatemachine::sendVolume(utils::uint8_t vol)
{
    double temp = vol*255/100;
    utils::uint8_t data = static_cast<utils::uint8_t>(temp);
    SMART_PTR(CVolumeMessage) pMsg = utils::CREATE_SMART_PTR(CVolumeMessage);
    pMsg->setVolume(data, data);
    sendTo(threadIds::MPDMain, pMsg);
}

/***********************************************************************//**
  @method :  sendChannel
  @comment:  send channel
  @param  :  channel to set
***************************************************************************/
void AudioStatemachine::sendChannel(utils::uint8_t channel)
{
    SMART_PTR(CSelectorMessage) pMsg = utils::CREATE_SMART_PTR(CSelectorMessage);
    pMsg->setSelector(channel);
    sendTo(threadIds::MPDMain, pMsg);
}

/***********************************************************************//**
  @method : sendPower
  @comment: send power
  @param  : pwrBitset
***************************************************************************/
void AudioStatemachine::sendPower(uint8_t pwrBitset)
{
    std::vector<ENPowerSwitch> on;
    for (uint8_t i = ENPowerSwitch::PS_PREAMP; i < ENPowerSwitch::PS_INVAL; ++i) {
        bool setOn = ((pwrBitset & (1 << i)) > 0);
        if (setOn) {
            DEBUG2("MPDVdc::setPowerSwitch on: "  << static_cast<uint32_t>(i));
            on.push_back(static_cast<ENPowerSwitch>(i));
        } else {
            DEBUG2("MPDVdc::setPowerSwitch off: "  << static_cast<uint32_t>(i));
        }
    }
    SMART_PTR(CPowerMessage) pMsg = utils::CREATE_SMART_PTR(CPowerMessage);
    pMsg->setOn(on);
    sendTo(threadIds::MPDMain, pMsg);
}

/***********************************************************************//**
  @method : sendStartSong
  @comment: start playing song from playlist
  @param  : playlist
  @param  : song
***************************************************************************/
void AudioStatemachine::sendStartSong(std::string playlist, std::string song)
{
    SMART_PTR(CMPDMessage) pMsg = utils::CREATE_SMART_PTR(CMPDMessage);
    pMsg->setPlay(playlist, song);
    sendTo(threadIds::MPDWeb, pMsg);
}

/***********************************************************************//**
  @method : sendStopSong
  @comment: send stop song
***************************************************************************/
void AudioStatemachine::sendStopSong()
{
    SMART_PTR(CMPDMessage) pMsg = utils::CREATE_SMART_PTR(CMPDMessage);
    pMsg->setStop();
    sendTo(threadIds::MPDWeb, pMsg);
}

/***********************************************************************//**
  @method : sendPrevSong
  @comment: send previous song
***************************************************************************/
void AudioStatemachine::sendPrevSong()
{
    SMART_PTR(CMPDMessage) pMsg = utils::CREATE_SMART_PTR(CMPDMessage);
    pMsg->setNext();
    sendTo(threadIds::MPDWeb, pMsg);
}

/***********************************************************************//**
  @method : sendNextSong
  @comment: send next song
***************************************************************************/
void AudioStatemachine::sendNextSong()
{
    SMART_PTR(CMPDMessage) pMsg = utils::CREATE_SMART_PTR(CMPDMessage);
    pMsg->setPrev();
    sendTo(threadIds::MPDWeb, pMsg);
}

/***********************************************************************//**
  @method : sendPlay
  @comment: send resume playing
***************************************************************************/
void AudioStatemachine::sendPlay()
{
    SMART_PTR(CMPDMessage) pMsg = utils::CREATE_SMART_PTR(CMPDMessage);
    pMsg->resume();
    sendTo(threadIds::MPDWeb, pMsg);
}

/***********************************************************************//**
  @method : sendPause
  @comment: send pause
***************************************************************************/
void AudioStatemachine::sendPause()
{
    SMART_PTR(CMPDMessage) pMsg = utils::CREATE_SMART_PTR(CMPDMessage);
    pMsg->pause();
    sendTo(threadIds::MPDWeb, pMsg);
}

/***********************************************************************//**
  @method :  sendTo
  @comment:  send message to thread
  @param  :  threadId receiver
  @param  :  pMessage message to send
***************************************************************************/
bool AudioStatemachine::sendTo(utils::uint32_t threadId, utils::smartMessagePtr pMessage)
{
    return utils::CThreadManager::GetInstance().Send(0, threadId, pMessage);
}

/***********************************************************************//**
  @method : incVolume
  @comment: increment volume by 2
***************************************************************************/
void AudioStatemachine::incVolume()
{
    if (m_tempVolumeValue > 250) {
        m_tempVolumeValue  = 255;
    } else {
        m_tempVolumeValue += 2;
    }
}

/***********************************************************************//**
  @method : decVolume
  @comment: decrement volume by 2
***************************************************************************/
void AudioStatemachine::decVolume()
{
    if (m_tempVolumeValue < 5) {
        m_tempVolumeValue = 0;
    } else {
        m_tempVolumeValue -= 2;
    }
}

/***********************************************************************//**
  @method : isCommand
  @comment: check if scene call is special command
  @param  : Scene
  @return : true: command
***************************************************************************/
bool AudioStatemachine::isCommand(utils::int32_t scene)
{
    switch (scene)
    {
        case CMD_REPEAT_OFF:
        case CMD_REPEAT:
        case CMD_REPEAT_ALL:
        case CMD_DIM_CONT:
        case CMD_DIM_UP:
        case CMD_DIM_DOWN:
        case CMD_MIN:
        case CMD_MAX:
        case CMD_STOP:
        case CMD_PREV:
        case CMD_NEXT:
        case CMD_PREV_CHAN:
        case CMD_NEXT_CHAN:
        case CMD_MUTE:
        case CMD_UNMUTE:
        case CMD_PLAY:
        case CMD_PAUSE:
        case CMD_SHUFFLE_OFF:
        case CMD_SHUFFLE_ON:
        case CMD_RESUME_OFF:
        case CMD_RESUME_ON:
            return true;
        default:
            return false;
    }
    return false;
}

/***********************************************************************//**
  @method : processCommand
  @comment: process command (scene checked to be a special command)
  @param  : scene
  @param  : force
  @param  : group
  @param  : zone
***************************************************************************/
void AudioStatemachine::processCommand(int32_t scene, bool force, int32_t group, int32_t zone)
{
    switch (scene)
    {
    case CMD_REPEAT_OFF:
    case CMD_REPEAT:
    case CMD_REPEAT_ALL:
        // TODO...
    break;

    case CMD_DIM_CONT:
    {
        if (m_oldCommand == CMD_DIM_UP) {
            decVolume();
        } else {
            incVolume();
        }
        sendVolume(m_tempVolumeValue);
        break;
    }

    case CMD_DIM_UP:
    {
        m_oldCommand = CMD_DIM_UP;
        decVolume();
        sendVolume(m_tempVolumeValue);
        break;
    }

    case CMD_DIM_DOWN:
    {
        m_oldCommand = CMD_DIM_DOWN;
        incVolume();
        sendVolume(m_tempVolumeValue);
        break;
    }

    case CMD_MIN:
    {
        m_tempVolumeValue = 0;
        sendVolume(m_tempVolumeValue);
        break;
    }

    case CMD_MAX:
    {
        m_tempVolumeValue = 100;
        sendVolume(m_tempVolumeValue);
        break;
    }

    case CMD_STOP:
    {
        sendStopSong();
        break;
    }

    case CMD_PREV:
    {
        sendPrevSong();
        break;
    }
    case CMD_NEXT:
    {
        sendNextSong();
        break;
    }

    case CMD_PREV_CHAN:
    case CMD_NEXT_CHAN:
        break;

    case CMD_MUTE:
    {
        sendVolume(0);
        break;
    }
    case CMD_UNMUTE:
    {
        sendVolume(m_tempVolumeValue);
        break;
    }

    case CMD_PLAY:
    {
        sendPlay();
        break;
    }
    case CMD_PAUSE:
    {
        sendPause();
        break;
    }

    case CMD_SHUFFLE_OFF:
    case CMD_SHUFFLE_ON:
        break;

    case CMD_RESUME_OFF:
    {
        sendPause();
        break;
    }
    case CMD_RESUME_ON:
    {
        sendPlay();
        break;
    }
    }
}
