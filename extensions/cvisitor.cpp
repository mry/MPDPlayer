/***************************************************************************
                          cvisitor.cpp  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cvisitor.h"
#include "rfw/utils/clogger.h"

/***********************************************************************//**
 @method : visitPowerMessage
 @comment: process power message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitPowerMessage       (CPowerMessage        * pMsg)
{
    WARN("Not implemented");
}

/***********************************************************************//**
 @method : visitPowerMessage
 @comment: process power message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitSelectorMessage    (CSelectorMessage     * pMsg)
{
    WARN("Not implemented");
}

/***********************************************************************//**
 @method : visitVolumeMessage
 @comment: process power message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitVolumeMessage      (CVolumeMessage       * pMsg)
{
    WARN("Not implemented");
}

/***********************************************************************//**
 @method : visitSaveMessage
 @comment: process power message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitSaveMessage        (CSaveMessage       * pMsg)
{
    WARN("Not implemented");
}

/***********************************************************************//**
 @method : visitMPDMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitMPDMessage         (CMPDMessage          * pMsg)
{
    WARN("Not implemented");
}
