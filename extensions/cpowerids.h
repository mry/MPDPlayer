/***************************************************************************
                         cpowerids.h - description
                         -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CPOWERIDS_H
#define CPOWERIDS_H

enum ENPowerSwitch
{
    PS_PREAMP  = 0,
    PS_AMP     = 1,
    PS_DISPLAY = 2,
    PS_OUT1    = 3,
    PS_OUT2    = 4,
    PS_OUT3    = 5,
    PS_OUT4    = 6,
    PS_INVAL   = 7
};

ENPowerSwitch& operator++(ENPowerSwitch& f);


#endif // CPOWERIDS_H
