/***************************************************************************
                          csavemessage.h  -  description
                             -------------------
    begin                : Sun  22 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "csavemessage.h"

#include "cmessageids.h"
#include "cvisitor.h"

#include <sstream>

/***********************************************************************//**
 @method : CSaveMessage
 @comment: message to save data
 ***************************************************************************/
CSaveMessage::CSaveMessage()
{
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
utils::int32_t CSaveMessage::GetMessageId() const
{
  return MESSAGE_SAVE_e;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
std::string CSaveMessage::ToString() const
{
  std::ostringstream output;
  output << "CSaveMessage";
  return output.str();
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void CSaveMessage::visit(CVisitor* visitor)
{
  visitor->visitSaveMessage(this);
}

