/***************************************************************************
                          ivisitor.h  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IVISITOR_H
#define IVISITOR_H

class CVisitor;

class IVisitor
{
public:
    virtual ~IVisitor(){};
    virtual void visit(CVisitor* visitor) = 0;
};

#endif // IVISITOR_H
