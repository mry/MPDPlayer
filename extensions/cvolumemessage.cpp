/***************************************************************************
                          cvolumemessage.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cvolumemessage.h"
#include "cmessageids.h"
#include <sstream>

#include "cvisitor.h"

/***************************************************************************
 @method : CVolumeMessage
 @comment: constructor
 ***************************************************************************/
CVolumeMessage::CVolumeMessage() :
    m_left(0),
    m_right(0)
{
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
utils::int32_t CVolumeMessage::GetMessageId() const
{
    return MESSAGE_VOLUME_e;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
std::string CVolumeMessage::ToString() const
{
    std::ostringstream output;
    output << "CVolumeMessage left: " << (utils::uint32_t)m_left
           << " right: " << (utils::uint32_t)m_right;
    return output.str();
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void CVolumeMessage::visit(CVisitor* visitor)
{
    visitor->visitVolumeMessage(this);
}

/***********************************************************************//**
 @method : setVolume
 @comment: set volume
 @param  : left left channel
 @param  : right right channel
 ***************************************************************************/
void CVolumeMessage::setVolume(utils::uint8_t left, utils::uint8_t right)
{
    m_left = left;
    m_right = right;
}

/***********************************************************************//**
 @method : getLeft
 @comment: get volume
 @return : left channel
 ***************************************************************************/
utils::uint8_t CVolumeMessage::getLeft() const
{
    return m_left;
}

/***********************************************************************//**
 @method : getRight
 @comment: get volume
 @return : right channel
 ***************************************************************************/
utils::uint8_t CVolumeMessage::getRight() const
{
    return m_right;
}
