/***************************************************************************
                          csavemessage.h  -  description
                             -------------------
    begin                : Sun  22 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CSAVEMESSAGE_H
#define CSAVEMESSAGE_H

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"

#include "ivisitor.h"
#include <string>

class CSaveMessage :
    public utils::CMessage,
    public IVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CSaveMessage);
public:
    CSaveMessage();
    virtual utils::int32_t GetMessageId() const;
    virtual std::string ToString() const;
    virtual void visit(CVisitor* visitor);
};

#endif // CSAVEMESSAGE_H
