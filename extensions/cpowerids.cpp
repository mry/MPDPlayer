/***************************************************************************
                         cpowerids.cpp - description
                         -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cpowerids.h"
#include "extensions/enumerationutils.h"

ENPowerSwitch& operator++(ENPowerSwitch& f)
{
    switch (f)
    {
      case ENPowerSwitch::PS_PREAMP: f = ENPowerSwitch::PS_AMP;     break;
      case ENPowerSwitch::PS_AMP:    f = ENPowerSwitch::PS_DISPLAY; break;
      case ENPowerSwitch::PS_DISPLAY:f = ENPowerSwitch::PS_OUT1;    break;
      case ENPowerSwitch::PS_OUT1:   f = ENPowerSwitch::PS_OUT2;    break;
      case ENPowerSwitch::PS_OUT2:   f = ENPowerSwitch::PS_OUT3;    break;
      case ENPowerSwitch::PS_OUT3:   f = ENPowerSwitch::PS_OUT4;    break;
      case ENPowerSwitch::PS_OUT4:   f = ENPowerSwitch::PS_INVAL;   break;
      case ENPowerSwitch::PS_INVAL:  f = ENPowerSwitch::PS_INVAL;   break;
    }
    return f;
}
