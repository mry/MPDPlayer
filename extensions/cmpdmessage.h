/***************************************************************************
                          cmpdmessage.h  -  description
                             -------------------
    begin                : Tue April 11 2016
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CMPDMESSAGE_H
#define CMPDMESSAGE_H

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"
#include <vector>
#include <string>

#include "ivisitor.h"



class CMPDMessage  :
        public utils::CMessage,
        public IVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CMPDMessage);

public:
    typedef enum
    {
        MPD_CMD_PLAY_e   = 0,
        MPD_CMD_STOP_e   = 1,
        MPD_CMD_PAUSE_e  = 2,
        MPD_CMD_NEXT_e   = 3,
        MPD_CMD_PREV_e   = 4,
        MPD_CMD_RESUME_e = 5
    } MPD_COMMANDS_e;

public:
    CMPDMessage() = default;
    virtual utils::int32_t GetMessageId() const;
    virtual std::string ToString() const;
    virtual void visit(CVisitor* visitor);

    void setStop();
    void setPlay(std::string& playlist, std::string& song);
    void setNext();
    void setPrev();
    void resume();
    void pause();

    MPD_COMMANDS_e getCommand() const;

    std::string getPlaylist() const;
    std::string getSong() const;

private:
    MPD_COMMANDS_e m_command;
    std::string m_playlist;
    std::string m_song;
};

#endif // CMPDMESSAGE_H
