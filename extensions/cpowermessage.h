/***************************************************************************
                          cpowermessage.h  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CPOWERMESSAGE_H
#define CPOWERMESSAGE_H

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"
#include <vector>
#include <string>

#include "cpowerids.h"
#include "ivisitor.h"

class CPowerMessage :
    public utils::CMessage,
    public IVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CPowerMessage);

public:
    CPowerMessage();
    virtual utils::int32_t GetMessageId() const;
    virtual std::string ToString() const;
    virtual void visit(CVisitor* visitor);

    void setOn(std::vector<ENPowerSwitch> on);
    std::vector<ENPowerSwitch> getOn() const;

private:
    std::vector<ENPowerSwitch> m_vecOutputsOn;
};

#endif // CPOWERMESSAGE_H
