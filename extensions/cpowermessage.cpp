/***************************************************************************
                          cpowermessage.cpp  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cmessageids.h"
#include "cpowermessage.h"
#include "cvisitor.h"

#include <sstream>

/***********************************************************************//**
 @method : CPowerMessage
 @comment: constructor
 ***************************************************************************/
CPowerMessage::CPowerMessage()
{
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
utils::int32_t CPowerMessage::GetMessageId() const
{
    return MESSAGE_POWER_e;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
std::string CPowerMessage::ToString() const
{
    std::ostringstream output;
    output << "CPowerMessage";
    return output.str();
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void CPowerMessage::visit(CVisitor* visitor)
{
    visitor->visitPowerMessage(this);
}

/***********************************************************************//**
 @method : setOn
 @comment: set vector for on
 @param  : on vector for on
 ***************************************************************************/
void CPowerMessage::setOn(std::vector<ENPowerSwitch> on)
{
    m_vecOutputsOn = on;
}

/***********************************************************************//**
 @method : getOn
 @comment: get vector for on
 @return : vector for on
 ***************************************************************************/
std::vector<ENPowerSwitch> CPowerMessage::getOn() const
{
    return m_vecOutputsOn;
}
