/***************************************************************************
                          cselectormessage.cpp  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cselectormessage.h"

#include "cmessageids.h"
#include "cvisitor.h"

#include <sstream>

/***********************************************************************//**
 @method : CSelectorMessage
 @comment: constructor
 ***************************************************************************/
CSelectorMessage::CSelectorMessage() :
    m_selector(0)
{
}

/***********************************************************************//**
 @method : setSelector
 @comment: set selector
 @aram   : selector
 ***************************************************************************/
void CSelectorMessage::setSelector(utils::uint8_t selector)
{
    m_selector = selector;
}

/***********************************************************************//**
 @method : getSelector
 @comment: get selector
 @return : selector
 ***************************************************************************/
utils::uint8_t CSelectorMessage::getSelector() const
{
    return m_selector;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
utils::int32_t CSelectorMessage::GetMessageId() const
{
  return MESSAGE_SELECTOR_e;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
std::string CSelectorMessage::ToString() const
{
    std::ostringstream output;
    output << "CSelectorMessage";
    return output.str();
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void CSelectorMessage::visit(CVisitor* visitor)
{
    visitor->visitSelectorMessage(this);
}


