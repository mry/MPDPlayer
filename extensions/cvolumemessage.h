/***************************************************************************
                          cvolumemessage.h  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CVOLUMEMESSAGE_H
#define CVOLUMEMESSAGE_H

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"
#include <string>

#include "ivisitor.h"

class CVolumeMessage :
    public utils::CMessage,
    public IVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CVolumeMessage);

public:
    CVolumeMessage();
    virtual utils::int32_t GetMessageId() const;
    virtual std::string ToString() const;
    virtual void visit(CVisitor* visitor);

    void setVolume(utils::uint8_t left, utils::uint8_t right);
    utils::uint8_t getLeft() const;
    utils::uint8_t getRight() const;

private:
    utils::uint8_t m_left;
    utils::uint8_t m_right;
};

#endif // CVOLUMEMESSAGE_H
