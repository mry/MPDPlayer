/***************************************************************************
                          cmessageids.h  -  description
                          -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CMESSAGEIDS_H
#define CMESSAGEIDS_H

#include "rfw/utils/cmessage.h"

typedef enum
{
    MESSAGE_VOLUME_e  = utils::MESSAGE_TYPE_e::EVENT_LAST_e,
    MESSAGE_POWER_e,
    MESSAGE_SELECTOR_e,
    MESSAGE_SAVE_e,
    MESSAGE_MPD_e
} MPD_MESSAGE_TYPE_e;


#endif // CMESSAGEIDS_H
