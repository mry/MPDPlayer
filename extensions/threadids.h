/***************************************************************************
                          threadids.h - description
                          -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef THREADIDS_H
#define THREADIDS_H

namespace threadIds
{
    enum
    {
        MPDMain = 1,
        MPDTest = 2,
        MPDVdc  = 3,
        MPDWeb  = 4
    };
};

#endif // THREADIDS_H
