/***************************************************************************
                          cselectormessage.h  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CSELECTORMESSAGE_H
#define CSELECTORMESSAGE_H

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"

#include "ivisitor.h"
#include <string>

class CSelectorMessage :
    public utils::CMessage,
    public IVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CSelectorMessage);

public:
    CSelectorMessage();
    virtual utils::int32_t GetMessageId() const;
    virtual std::string ToString() const;
    virtual void visit(CVisitor* visitor);

    void setSelector(utils::uint8_t selector);
    utils::uint8_t getSelector() const;

private:
    utils::uint8_t m_selector;
};

#endif // CSELECTORMESSAGE_H
