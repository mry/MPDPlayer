/***************************************************************************
                          cmpdmessage.h  -  description
                             -------------------
    begin                : Tue April 11 2016
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cmpdmessage.h"

#include "cmessageids.h"
#include "cmpdmessage.h"
#include "cvisitor.h"

#include <sstream>

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
utils::int32_t CMPDMessage::GetMessageId() const
{
    return MESSAGE_MPD_e;
}
/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
std::string CMPDMessage::ToString() const
{
    std::ostringstream output;
    output << "CMPDMessage";
    return output.str();
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void CMPDMessage::visit(CVisitor* visitor)
{
    visitor->visitMPDMessage(this);
}

/***********************************************************************//**
 @method : setStop
 @comment: set stop command
 ***************************************************************************/
void CMPDMessage::setStop()
{
    m_command = MPD_CMD_STOP_e;
}

void CMPDMessage::setNext()
{
    m_command = MPD_CMD_NEXT_e;
}

void CMPDMessage::setPrev()
{
    m_command = MPD_CMD_PREV_e;
}

void CMPDMessage::resume()
{
    m_command = MPD_CMD_RESUME_e;
}

void CMPDMessage::pause()
{
    m_command = MPD_CMD_PAUSE_e;
}

/***********************************************************************//**
 @method : setPlay
 @comment: set play command
 @param  : playlist playlist by name
 @param  : song song by name
 ***************************************************************************/
void CMPDMessage::setPlay(std::string& playlist, std::string& song)
{
    m_command = MPD_CMD_PLAY_e;
    m_playlist = playlist;
    m_song = song;
}

/***********************************************************************//**
 @method : getCommand
 @comment: get command
 @return : command
 ***************************************************************************/
CMPDMessage::MPD_COMMANDS_e CMPDMessage::getCommand() const
{
    return m_command;
}

/***********************************************************************//**
 @method : getPlaylist
 @comment: get playlist
 @return : playlist
 ***************************************************************************/
std::string CMPDMessage::getPlaylist() const
{
    return m_playlist;
}

/***********************************************************************//**
 @method : getSong
 @comment: get song
 @return : song
 ***************************************************************************/
std::string CMPDMessage::getSong() const
{
    return m_song;
}
