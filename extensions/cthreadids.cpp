/***************************************************************************
                          cthreadids.cpp  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/ctypes.h"

class CThreadIds {
  /// this class can not be copied
  UNCOPYABLE(CThreadIds);

public:
  CThreadIds();
  ~CThreadIds();

  enum{
    THREAD_CAM           = 1,
    THREAD_MIDDLEWARE    = 2,
    THREAD_ZOOM          = 3,
    THREAD_CAMERACONTROL = 4,
    THREAD_LIFT          = 5,
    THREAD_INPUT         = 6,
    THREAD_STM_HORIZ     = 7,
    THREAD_STM_VERTIC    = 8,
    THREAD_MIDDLEWARE_CB = 9,
  };
};


