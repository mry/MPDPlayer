/***************************************************************************
                          powercontroller.h  -  description
                             -------------------
    begin                : Thu Jan 19 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef POWERCONTROLLER_H
#define POWERCONTROLLER_H

#include "iwebcontroller.h"

class PowerController :
    public IWebController
{
public:
    PowerController(IAccess* access);
    virtual ~PowerController() = default;
    virtual void setup();
    void setPower(Mongoose::Request &request, Mongoose::StreamResponse &response);
    void setPowerOutput(Mongoose::Request &request, Mongoose::StreamResponse &response);
    void getPower(Mongoose::Request &request, Mongoose::StreamResponse &response);


};

#endif // POWERCONTROLLER_H
