/***************************************************************************
                          webserver.cpp  -  description
                             -------------------
    begin                : Thu Oct 6 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "webserver.h"
#include "extensions/threadids.h"
#include <mongoose/Server.h>

#include "mpdwrapper/mpdstatemachine.h"

#include "cvisitorwebserver.h"
#include "extensions/ivisitor.h"

#include "rfw/utils/cmessage.h"

/***********************************************************************//**
  @method : WebServer
  @comment: constructor
  @param  : documentRoot main directory
  @param  : sceneDescription interface
***************************************************************************/
WebServer::WebServer(std::string& documentRoot,
                     boost::shared_ptr<SceneDescriptionItf> sceneDesc) :
    utils::CThread(threadIds::MPDWeb),
    m_documentRoot(documentRoot),
    m_sceneDescription(sceneDesc)
{
}

/***********************************************************************//**
  @method :  ~WebServer
  @comment:  destructor
***************************************************************************/
WebServer::~WebServer()
{
    m_webController.clear();
}

/***********************************************************************//**
  @method :  getMPDConnector
  @comment:  get mpd connector
  @return:   MPDConnector
***************************************************************************/
MPDConnector& WebServer::getMPDConnector()
{
    return m_connector;
}

/***********************************************************************//**
  @method :  getSceneDescriptionItf
  @comment:  get scene description interface
  @return :  interface for scene
***************************************************************************/
boost::shared_ptr<SceneDescriptionItf>  WebServer::getSceneDescriptionItf()
{
    return m_sceneDescription;
}

/***********************************************************************//**
  @method :  registerWebControllerItf
  @comment:  register a web controller
  @param  :  webctrl controller
***************************************************************************/
void WebServer::registerWebControllerItf(std::shared_ptr<IWebController> webCtrl)
{
    m_webController.push_back(webCtrl);
}

/***********************************************************************//**
  @method :  Run
  @comment:  thread rum method
  @param  :  args input parameters
  @param  :  output parameters
***************************************************************************/
void* WebServer::Run(void * args)
{
    Mongoose::Server server(8090, m_documentRoot.c_str()); // port 8090

    // setup
    for(auto controller : m_webController) {
        server.registerController(controller.get());
    }

    MPDStateMachine stm(m_connector, m_sceneDescription);
    server.start(false);
    while(IsRunning()) {
        server.process(200);
        usleep(200000);
        stm.processTimeEvent(server.getWebSockets());

        while(GetPendingMessages()) {
            utils::smartMessagePtr pMsg = ReceiveMessage();
            IVisitor* pVisit =dynamic_cast<IVisitor*>(pMsg.get());
            CVisitorWebserver visitor(this);
            pVisit->visit(&visitor);
        }
    }
    server.stop();
    return nullptr;
}
