/***************************************************************************
                          powercontroller.cpp  -  description
                             -------------------
    begin                : Thu Jan 19 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "iaccess.h"
#include "powercontroller.h"

#include<iostream>
#include<string>
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "rfw/utils/cconverter.h"

using namespace rapidjson;
using namespace Mongoose;

/***********************************************************************//**
  @method : constructor
  @comment:
  @oaram  : access interface
***************************************************************************/
PowerController::PowerController(IAccess* access):
    IWebController(access)
{
}

/***********************************************************************//**
  @method : setup
  @comment: register handlers
***************************************************************************/
void PowerController::setup()
{
    addRoute("GET", "/setPower", PowerController,  setPower);
    addRoute("GET", "/getPower", PowerController,  getPower);
    addRoute("GET", "/setPowerOutput", PowerController,  setPowerOutput);
}

/***********************************************************************//**
  @method : setPowerOn
  @comment: handler: e.g. http://127.0.0.1:8090/setPowerOn?power=2
            range 0..127
  @param  : request
  @param  : response
***************************************************************************/
void PowerController::setPower(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    std::string power = request.get("power", "0");
    int numPower = utils::converter::strToIntDef(power, 0);

    if (numPower >= 128) // hack
    {
        numPower = 127;
    }
    getAccessItf()->getSceneDescriptionItf()->setPower(numPower);

    numPower = getAccessItf()->getSceneDescriptionItf()->getPower();
    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("power");
    writer.Uint(numPower);
    writer.EndObject();
    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : setPowerOutput
  @comment: handler: e.g. http://127.0.0.1:8090/setPowerOutput?channel=1&state=0
            range 0..127
  @param  : request
  @param  : response
***************************************************************************/
void PowerController::setPowerOutput(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    std::string channel = request.get("channel", "0");
    int numChannel = utils::converter::strToIntDef(channel, 0);
    if (numChannel > 7) {
        numChannel = 7;
    }

    std::string state = request.get("state", "0");
    int numState = utils::converter::strToIntDef(state, 0);
    if (numState > 0) {
        numState = 1;
    }

    int numPower = getAccessItf()->getSceneDescriptionItf()->getPower();
    if (numState) {
        numPower |=     (1 << numChannel);
    } else {
        int temp = 0xff^(1 << numChannel);
        numPower &= temp;
    }
    getAccessItf()->getSceneDescriptionItf()->setPower(numPower);

    numPower = getAccessItf()->getSceneDescriptionItf()->getPower();
    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("power");
    writer.Uint(numPower);
    writer.EndObject();
    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : getPower
  @comment: handler: e.g. http://127.0.0.1:8090/getPower
  @param  : request
  @param  : response
***************************************************************************/
void PowerController::getPower(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    int numPower = getAccessItf()->getSceneDescriptionItf()->getPower();
    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("power");
    writer.Uint(numPower);
    writer.EndObject();
    response << s.GetString() << std::endl;
}
