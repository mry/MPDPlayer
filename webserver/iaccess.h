#ifndef IACCESS_H
#define IACCESS_H

#include <vector>
#include <boost/shared_ptr.hpp>
#include "vdc/scenedescription.h"

class MPDConnector;

class IAccess
{
public:
    IAccess() = default;
    virtual ~IAccess() = default;

    virtual MPDConnector& getMPDConnector() = 0;
    virtual boost::shared_ptr<SceneDescriptionItf>  getSceneDescriptionItf() = 0;
};

#endif // IACCESS_H
