/***************************************************************************
                          webserverfactory.h  -  description
                             -------------------
    begin                : Sat April 8 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef WEBSERVERFACTORY_H
#define WEBSERVERFACTORY_H

#include "webserver/webserver.h"
#include "webserver/scenecontroller.h"
#include "webserver/volumecontroller.h"
#include "webserver/channelcontroller.h"
#include "webserver/powercontroller.h"
#include "webserver/mpdcontroller.h"

class WebServerFactory
{
public:
    WebServerFactory(boost::shared_ptr<SceneDescriptionItf> sceneDesc,
                     std::string& documentRoot);

    void start();
    void stop();

private:
    void initialize();

private:
    boost::shared_ptr<SceneDescriptionItf> m_sceneDescription;
    std::shared_ptr<SceneController> m_sceneController;
    std::shared_ptr<VolumeController> m_volumeController;
    std::shared_ptr<ChannelController> m_channelController;
    std::shared_ptr<PowerController> m_powerController;
    std::shared_ptr<MpdController>m_mpdController;

    WebServer m_server;
};

#endif // WEBSERVERFACTORY_H
