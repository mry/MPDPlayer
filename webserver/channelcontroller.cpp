/***************************************************************************
                          channelcontroller.cpp  -  description
                             -------------------
    begin                : Thu Oct 13 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "iaccess.h"
#include "channelcontroller.h"

#include<iostream>
#include<string>
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "rfw/utils/cconverter.h"


using namespace rapidjson;
using namespace Mongoose;

/***********************************************************************//**
  @method : constructor
  @comment:
  @oaram  : access interface
***************************************************************************/
ChannelController::ChannelController(IAccess* access) :
        IWebController(access)
{
}

/***********************************************************************//**
  @method : setup
  @comment: register handlers
***************************************************************************/
void ChannelController::setup()
{
    addRoute("GET", "/setChannel", ChannelController,  setChannel);
    addRoute("GET", "/getChannel", ChannelController,  getChannel);
}

/***********************************************************************//**
  @method : setChannel
  @comment: handler: e.g. http://127.0.0.1:8090/setChannel?channel=2
  @param  : request
  @param  : response
***************************************************************************/
void ChannelController::setChannel(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    std::string channel = request.get("channel", "0");
    int numChannel = utils::converter::strToIntDef(channel, 0);

    if (numChannel > 4) // hack
    {
        numChannel = 4;
    }

    getAccessItf()->getSceneDescriptionItf()->setChannel(numChannel);

    numChannel = getAccessItf()->getSceneDescriptionItf()->getChannel();

    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("channel");
    writer.Uint(numChannel);
    writer.EndObject();

    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : setChannel
  @comment: handler: e.g. http://127.0.0.1:8090/getChannel
  @param  : request
  @param  : response
***************************************************************************/
void ChannelController::getChannel(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    int numChannel = getAccessItf()->getSceneDescriptionItf()->getChannel();

    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("channel");
    writer.Uint(numChannel);
    writer.EndObject();

    response << s.GetString() << std::endl;
}

