/***************************************************************************
                          volumecontroller.h  -  description
                             -------------------
    begin                : Thu Oct 13 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VOLUMECONTROLLER_H
#define VOLUMECONTROLLER_H

#include "iwebcontroller.h"

class VolumeController :
    public IWebController
{
public:
    VolumeController(IAccess* access);
    virtual ~VolumeController() = default;
    virtual void setup();
    void setVolume(Mongoose::Request &request, Mongoose::StreamResponse &response);
    void getVolume(Mongoose::Request &request, Mongoose::StreamResponse &response);
};

#endif // VOLUMECONTROLLER_H
