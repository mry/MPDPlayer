/***************************************************************************
                          cvisitormpdthread.h  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cvisitorwebserver.h"
#include "extensions/cmpdmessage.h"

#include "mpdwrapper/mpdsong.h"

#include "rfw/utils/clogger.h"
#include "mpdwrapper/mpdplayer.h"
#include "mpdwrapper/mpdqueue.h"
#include "mpdwrapper/mpdsong.h"

/***********************************************************************//**
  @method : CVisitorWebserver
  @comment: constructor
  @param  : access itf
***************************************************************************/
CVisitorWebserver::CVisitorWebserver(IAccess* access) :
    m_access(access)
{
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void CVisitorWebserver::visitMPDMessage(CMPDMessage* pMsg)
{
    if (pMsg->getCommand() == CMPDMessage::MPD_CMD_PLAY_e) {

        if (!MPDQueue::removeAll(m_access->getMPDConnector())) {
            ERR("Failed to remove queue");
        }
        std::string playlist = pMsg->getPlaylist();
        std::string song = pMsg->getSong();
        if (!MPDQueue::load(m_access->getMPDConnector(), playlist)) {
            ERR("Failed to load playlist");
            return;
        }
        if (MPDSong::playSong(m_access->getMPDConnector(), song) < 0) {
            WARN("Failed to play song. Start playlist");
            MPDPlayer::runPlay(m_access->getMPDConnector());
        }
        INFO("Start playing");
    } else if (pMsg->getCommand() == CMPDMessage::MPD_CMD_STOP_e) {
        if(!MPDPlayer::runStop(m_access->getMPDConnector())) {
            ERR("Failed to stop stream");
        }
    } else if (pMsg->getCommand() == CMPDMessage::MPD_CMD_PAUSE_e) {
        if(!MPDPlayer::runPause(m_access->getMPDConnector(),true)) {
            ERR("Failed set pause");
        }
    } else if (pMsg->getCommand() == CMPDMessage::MPD_CMD_NEXT_e) {
        if(!MPDPlayer::runNext(m_access->getMPDConnector())) {
            ERR("Failed set next");
        }
    } else if (pMsg->getCommand() == CMPDMessage::MPD_CMD_PREV_e) {
        if(!MPDPlayer::runPrevious(m_access->getMPDConnector())) {
            ERR("Failed set previous");
        }
    } else if (pMsg->getCommand() == CMPDMessage::MPD_CMD_RESUME_e) {
        if(!MPDPlayer::runPause(m_access->getMPDConnector(), false)) {
            ERR("Failed set pause");
        }
    }
}

