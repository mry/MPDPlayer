/***************************************************************************
                          volumecontroller.cpp  -  description
                             -------------------
    begin                : Thu Oct 13 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "iaccess.h"
#include "volumecontroller.h"

#include <iostream>
#include <string>
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "rfw/utils/clogger.h"
#include "rfw/utils/cconverter.h"

using namespace rapidjson;
using namespace Mongoose;

/***********************************************************************//**
  @method : constructor
  @comment:
  @oaram  : access interface
***************************************************************************/
VolumeController::VolumeController(IAccess* access):
    IWebController(access)
{
}

/***********************************************************************//**
  @method : setup
  @comment: register handlers
***************************************************************************/
void VolumeController::setup()
{
    addRoute("GET", "/setVolume", VolumeController,  setVolume);
    addRoute("GET", "/getVolume", VolumeController,  getVolume);
}

/***********************************************************************//**
  @method : setVolume
  @comment: handler: e.g. http://127.0.0.1:8090/setVolume?volume=2
  @param  : request
  @param  : response
***************************************************************************/
void VolumeController::setVolume(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    std::string volume = request.get("volume", "0");
    int numVolume = utils::converter::strToIntDef(volume, 0);
    if (numVolume > 100) {
        numVolume = 100;
    }
    getAccessItf()->getSceneDescriptionItf()->setVolume(numVolume);

    numVolume = getAccessItf()->getSceneDescriptionItf()->getVolume();
    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("volume");
    writer.Uint(numVolume);
    writer.EndObject();
    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : setVolume
  @comment: handler: e.g. http://127.0.0.1:8090/getVolume
  @param  : request
  @param  : response
***************************************************************************/
void VolumeController::getVolume(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    int numVolume = getAccessItf()->getSceneDescriptionItf()->getVolume();

    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("volume");
    writer.Uint(numVolume);
    writer.EndObject();

    response << s.GetString() << std::endl;
}
