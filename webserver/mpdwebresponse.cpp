/***************************************************************************
                          mpdwebresponse.h  -  description
                             -------------------
    begin                : Fri May 12 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mpdwebresponse.h"

#include <sstream>

#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"
#include "rfw/utils/clogger.h"
#include "rfw/utils/cconverter.h"

using namespace rapidjson;

/***********************************************************************//**
  @method : createErrorMessage
  @comment: create a json error message
  @param  : status
  @return : error message in json format
***************************************************************************/
std::string MpdWebResponse::createErrorMessage(std::string& status)
{
    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    {
        writer.Key("type");
        writer.String("error");

        writer.Key("data");
        writer.String(status.c_str());
    }
    writer.EndObject();
    DEBUG2(s.GetString());
    return s.GetString();
}

/***********************************************************************//**
  @method : createOutput
  @comment: create output json output message
  @param  : output vector of MPDOut objects
  @param  : outputname type outputs or outputnames
  @return : json message
***************************************************************************/
std::string MpdWebResponse::createOutput( std::vector <MPDOut>& output, bool outputnames)
{
    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("type");
    if (outputnames) {
        writer.String("outputnames");
    } else {
        writer.String("outputs");
    }
    writer.Key("data");
    writer.StartArray();
    for (auto out: output) {
        writer.Key(utils::converter::intToString(out.id).c_str());
        if (outputnames) {
            writer.String(out.name.c_str());
        } else {
            writer.Bool(out.enabled);
        }
    }
    writer.EndArray();
    writer.EndObject();
    DEBUG2(s.GetString());
    return s.GetString();
}

/***********************************************************************//**
  @method : emitError
  @comment: emit error status
  @param  : socket
  @param  : status
***************************************************************************/
void MpdWebResponse::emitError(Mongoose::WebSocket& websocket, std::string& status)
{
    websocket.send(createErrorMessage(status));
}

/***********************************************************************//**
  @method : emitError
  @comment: emit error status
  @param  : sockets
  @param  : status
***************************************************************************/
void MpdWebResponse::emitError(Mongoose::WebSockets& websocket, std::string& status)
{
    websocket.clean();
    websocket.sendAll(createErrorMessage(status));
}

/***********************************************************************//**
  @method : emitStatus
  @comment: emit status of mpd
  @param  : sockets
  @param  : status
***************************************************************************/
void MpdWebResponse::emitStatus(Mongoose::WebSockets& sockets, MStatus& status)
{
    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("type");
    writer.Key("state");
    writer.Key("data");
    {
        writer.StartObject();

        writer.Key("state");
        writer.Uint(status.state);

        writer.Key("volume");
        writer.Int(status.volume);

        writer.Key("repeat");
        writer. Uint(status.repeat);

        writer.Key("single");
        writer.Uint(status.single);

        writer.Key("crossfade");
        writer.Uint(status.crossfade);

        writer.Key("consume");
        writer.Uint(status.consume);

        writer.Key("random");
        writer.Uint(status.random);

        writer.Key("songpos");
        writer.Uint(status.songPos);

        writer.Key("elapsedTime");
        writer.Uint(status.elapsedTime);

        writer.Key("totalTime");
        writer.Uint(status.totalTime);

        writer.Key("currentsongid");
        writer.Uint(status.songId);
        writer.EndObject();
    }

    writer.EndObject();

    sockets.clean();
    sockets.sendAll(s.GetString());
}

/***********************************************************************//**
  @method : emitQueue
  @comment: emit queue data
  @param  : websocket
  @param  : output vector with queue data
***************************************************************************/
void MpdWebResponse::emitQueue(Mongoose::WebSocket& websocket, std::vector<MPDQueueData>& output)
{
    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("type");
    writer.String("queue");

    writer.Key("data");
    writer.StartArray();
    for (auto out: output) {
        writer.StartObject();

        writer.Key("id");
        writer.Uint(out.id);

        writer.Key("pos");
        writer.Uint(out.position);

        writer.Key("duration");
        writer.Uint(out.duration);

        writer.Key("title");
        writer.String(out.title.c_str());

        writer.EndObject();
    }
    writer.EndArray();
    writer.EndObject();

    DEBUG2(s.GetString());
    websocket.send(s.GetString());
}

/***********************************************************************//**
  @method : emitOutput
  @comment: emit search result
  @param  : websocket
  @param  : output vector with outputs
  @param  : outputnames true: set names
***************************************************************************/
void MpdWebResponse::emitOutput(Mongoose::WebSocket& websocket, std::vector <MPDOut>& output, bool outputnames)
{
    websocket.send(createOutput(output, outputnames));
}

/***********************************************************************//**
  @method : emitOutputs
  @comment: emit search result
  @param  : websockets
  @param  : output vector with outputs
  @param  : outputnames true: set names
***************************************************************************/
void MpdWebResponse::emitOutput(Mongoose::WebSockets& websocket, std::vector <MPDOut>& output, bool outputnames)
{
    websocket.clean();
    websocket.sendAll(createOutput(output, outputnames));
}

/***********************************************************************//**
  @method : emitSearch
  @comment: emit search result
  @param  : websocket
  @param  : vecInfo search result
***************************************************************************/
void MpdWebResponse::emitSearch(Mongoose::WebSocket& websocket, std::vector<MPDSongInfo>& vecInfo)
{
    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("type");
    writer.String("search");

    writer.Key("data");
    writer.StartArray();
    for (auto out: vecInfo) {
        writer.StartObject();

        writer.Key("type");
        writer.String("song");

        writer.Key("uri");
        writer.String(out.uri.c_str());

        writer.Key("duration");
        writer.Uint(out.duration);

        writer.Key("title");
        writer.String(out.title.c_str());

        writer.EndObject();
    }
    writer.EndArray();
    writer.EndObject();

    DEBUG2(s.GetString());
    websocket.send(s.GetString());
}

/***********************************************************************//**
  @method : emitBrowse
  @comment: emit browse data to websocket
  @param  : websocket
  @param  : vecData browse data
  @param  : entitiy count number of entities
***************************************************************************/
void  MpdWebResponse::emitBrowse(Mongoose::WebSocket& websocket,
                std::vector<MPDBrowseData>&  vecData,
                unsigned int entity_count,
                std::string type)
{
    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();

    writer.Key("type");
    //writer.String("browse");
    writer.String(type.c_str());

    writer.Key("data");

    writer.StartArray();

    for (auto data: vecData)
    {
        writer.StartObject();

        switch (data.type) {

            case BTSong:
            writer.Key("type");
            writer.String("song");
            writer.Key("uri");
            writer.String(data.uri.c_str());
            writer.Key("duration");
            writer.Uint(data.duration);
            writer.Key("title");
            writer.String(data.title.c_str());
            break;

        case BTDirectory:
            writer.Key("type");
            writer.String("directory");
            writer.Key("dir");
            writer.String(data.directory.c_str());
            break;

        case BTPlaylist:
            writer.Key("type");
            writer.String("playlist");
            writer.Key("plist");
            writer.String(data.directory.c_str());
            break;
        }
        writer.EndObject();
    }

    writer.StartObject();
    writer.Key("type");
    writer.String("wrap");
    writer.Key("count");
    writer.Uint(entity_count);
    writer.EndObject();

    writer.EndArray();
    writer.EndObject();

    DEBUG2(s.GetString());
    websocket.send(s.GetString());
}


/***********************************************************************//**
  @method : emitCurrentSong
  @comment: emit the current song
  @param  : websocket
  @param  : position
  @param  : title
  @param  : artist
  @param  : album
***************************************************************************/
void  MpdWebResponse::emitCurrentSong(Mongoose::WebSockets& websocket,
                                      unsigned int position,
                                      std::string& title,
                                      std::string& artist,
                                      std::string& album)
{
  StringBuffer s;
  Writer<StringBuffer> writer(s);

  writer.StartObject();

  writer.Key("type");
  writer.String("song_change");

  writer.Key("data");
  {
    writer.StartObject();

    writer.Key("pos");
    writer.Uint(position);

    writer.Key("title");
    writer.String(title.c_str());

    writer.Key("artist");
    writer.String(artist.c_str());

    writer.Key("album");
    writer.String(album.c_str());

    writer.EndObject();
  }
  writer.EndObject();
  DEBUG2(s.GetString());

  websocket.clean();
  websocket.sendAll(s.GetString());
}
