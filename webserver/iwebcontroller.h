/***************************************************************************
                          iwebcontroller.h  -  description
                             -------------------
    begin                : Thu Oct 6 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef WEBCONTROLLERITF_H
#define WEBCONTROLLERITF_H

#include <mongoose/WebController.h>
#include "vdc/scenedescription.h"

class IAccess;

class IWebController :
    public Mongoose::WebController
{
public:
    IWebController(IAccess* access) :
        m_access(access)
    {}

    virtual ~IWebController() = default;
    virtual void setup() = 0;

protected:
    IAccess* getAccessItf() {
        return m_access;
    }

private:
    IAccess* m_access;
};

#endif
