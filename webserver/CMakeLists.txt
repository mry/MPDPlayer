#***************************************************************************
#                          CMakeLists.txt
#                          
#                          CMake for project mpdwrapper
#                           -------------------
#    begin                : THU SEP 8 2016
#    copyright            : (C) 2016 by mry
#    email                : mry@hispeed.ch
#
#***************************************************************************

cmake_minimum_required (VERSION 2.8) 

file(GLOB webserver_SRC
    "*.h"
    "*.cpp"
)

include_directories (
        ${CMAKE_SOURCE_DIR}
        ${RFW_INCLUDE_DIR}) 

add_library(webserver STATIC ${webserver_SRC})
