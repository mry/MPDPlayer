/***************************************************************************
                          mpdcontroller.cpp  -  description
                             -------------------
    begin                : Mon March 6 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "iaccess.h"
#include "mpdcontroller.h"

#include <iostream>
#include <string>
#include <functional>

#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "rfw/utils/clogger.h"

#include "rfw/utils/cconverter.h"

#include "extensions/cstringhelper.h"

#include "mpdwrapper/mpdconnector.h"
#include "mpdwrapper/mpdplayer.h"
#include "mpdwrapper/mpddatabase.h"
#include "mpdwrapper/mpdoutput.h"
#include "mpdwrapper/mpdplaylist.h"
#include "mpdwrapper/mpdqueue.h"
#include "mpdwrapper/mpdsong.h"

#include "webserver/mpdwebresponse.h"

using namespace rapidjson;
using namespace Mongoose;

/***********************************************************************//**
  @method : constructor
  @comment:
  @oaram  : access interface
***************************************************************************/
MpdController::MpdController(IAccess* access) :
    IWebController(access)
{
}

void MpdController::addWebsocketRoute(std::string data, MpdController::Function func)
{
    m_functionMap[data] = func;
}

/***********************************************************************//**
  @method : setup
  @comment: register handlers
***************************************************************************/
void MpdController::setup()
{
    using namespace std::placeholders;

    addWebsocketRoute("MPD_API_GET_QUEUE",        std::bind(&MpdController::getQueue,       this, _1, _2));
    addWebsocketRoute("MPD_API_GET_BROWSE",       std::bind(&MpdController::getBrowse,      this, _1, _2));
    addWebsocketRoute("MPD_API_GET_PLAYLIST",     std::bind(&MpdController::getBrowsePlaylist, this, _1, _2));
    addWebsocketRoute("MPD_API_GET_MPDHOST",      std::bind(&MpdController::getMpdhost,     this, _1, _2));
    addWebsocketRoute("MPD_API_ADD_TRACK",        std::bind(&MpdController::addTrack,       this, _1, _2));
    addWebsocketRoute("MPD_API_ADD_PLAY_TRACK",   std::bind(&MpdController::addPlayTrack,   this, _1, _2));
    addWebsocketRoute("MPD_API_ADD_PLAYLIST",     std::bind(&MpdController::addPlaylist,    this, _1, _2));
    addWebsocketRoute("MPD_API_PLAY_TRACK",       std::bind(&MpdController::playTrack,      this, _1, _2));
    addWebsocketRoute("MPD_API_SAVE_QUEUE",       std::bind(&MpdController::saveQueue,      this, _1, _2));
    addWebsocketRoute("MPD_API_DELETE_PLAYLIST",  std::bind(&MpdController::deletePlaylist, this, _1, _2));
    addWebsocketRoute("MPD_API_RM_TRACK",         std::bind(&MpdController::rmTrack,        this, _1, _2));
    addWebsocketRoute("MPD_API_RM_ALL",           std::bind(&MpdController::rmAll,          this, _1, _2));
    addWebsocketRoute("MPD_API_SEARCH",           std::bind(&MpdController::search,         this, _1, _2));
    addWebsocketRoute("MPD_API_SET_VOLUME",       std::bind(&MpdController::setVolume,      this, _1, _2));
    addWebsocketRoute("MPD_API_SET_PAUSE",        std::bind(&MpdController::setPause,       this, _1, _2));
    addWebsocketRoute("MPD_API_SET_PLAY",         std::bind(&MpdController::setPlay,        this, _1, _2));
    addWebsocketRoute("MPD_API_SET_STOP",         std::bind(&MpdController::setStop,        this, _1, _2));
    addWebsocketRoute("MPD_API_SET_SEEK",         std::bind(&MpdController::setSeek,        this, _1, _2));
    addWebsocketRoute("MPD_API_SET_NEXT",         std::bind(&MpdController::setNext,        this, _1, _2));
    addWebsocketRoute("MPD_API_SET_PREV",         std::bind(&MpdController::setPrev,        this, _1, _2));
    addWebsocketRoute("MPD_API_SET_MPDHOST",      std::bind(&MpdController::setMpdHost,     this, _1, _2));
    addWebsocketRoute("MPD_API_SET_MPDPASS",      std::bind(&MpdController::setMpdPass,     this, _1, _2));
    addWebsocketRoute("MPD_API_UPDATE_DB",        std::bind(&MpdController::updateDb,       this, _1, _2));
    addWebsocketRoute("MPD_API_GET_OUTPUTS",      std::bind(&MpdController::getOutputs,     this, _1, _2));
    addWebsocketRoute("MPD_API_TOGGLE_OUTPUT",    std::bind(&MpdController::toggleOutputs,  this, _1, _2));
    addWebsocketRoute("MPD_API_TOGGLE_RANDOM",    std::bind(&MpdController::toggleRandom,   this, _1, _2));
    addWebsocketRoute("MPD_API_TOGGLE_CONSUME",   std::bind(&MpdController::toggleConsume,  this, _1, _2));
    addWebsocketRoute("MPD_API_TOGGLE_SINGLE",    std::bind(&MpdController::toggleSingle,   this, _1, _2));
    addWebsocketRoute("MPD_API_TOGGLE_CROSSFADE", std::bind(&MpdController::toggleCrossfade,this, _1, _2));
    addWebsocketRoute("MPD_API_TOGGLE_REPEAT",    std::bind(&MpdController::toggleRepeat,   this, _1, _2));
    addWebsocketRoute("MPD_API_CREATE_PLAYLIST",  std::bind(&MpdController::createPlaylist, this, _1, _2));
}

/***********************************************************************//**
  @method : getQueue
  @comment: push queue data
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::getQueue(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("getQueue");
    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }
    unsigned int uint_buf = utils::converter::strToUInt(elements[1]);

    std::vector<MPDQueueData> output;
    try {
        output = MPDOutput::getQueue(getAccessItf()->getMPDConnector(), uint_buf);
    } catch (std::runtime_error& err) {
        std::string errMsg (err.what());
        MpdWebResponse::emitError(*websocket, errMsg);
        return;
    }

    MpdWebResponse::emitQueue(*websocket, output);
}

/***********************************************************************//**
  @method : getBrowse
  @comment: push browse data
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::getBrowse(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("getBrowse");
    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 3) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }

    unsigned int offset = utils::converter::strToInt(elements[1]);
    std::string  path = elements[2];

    unsigned int entity_count;
    std::vector<MPDBrowseData> vecData = MPDDatabase::browseAll(
                getAccessItf()->getMPDConnector(),
                path,
                offset,
                entity_count);

    MpdWebResponse::emitBrowse(*websocket, vecData, entity_count, "browse");
}

/***********************************************************************//**
  @method : getBrowsePlaylist
  @comment: push all playlist entries
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::getBrowsePlaylist(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("getBrowsePlaylist");
    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }

    unsigned int offset = utils::converter::strToInt(elements[1]);
    std::string  path = elements[2];

    unsigned int entity_count;
    std::vector<MPDBrowseData> vecData = MPDDatabase::browsePlaylist(
                getAccessItf()->getMPDConnector(),
                path,
                offset,
                entity_count);

    MpdWebResponse::emitBrowse(*websocket, vecData, entity_count,"sceneBrowse");
}

// NO
void MpdController::getMpdhost(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("getMpdhost");

}

/***********************************************************************//**
  @method : addTrack
  @comment: add track to queue
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::addTrack(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("addTrack");

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }

    if(!MPDQueue::add(getAccessItf()->getMPDConnector(), elements[1])) {
        INFO("addTrack failed.");
    }
}

/***********************************************************************//**
  @method : addPlayTrack
  @comment: add play track
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::addPlayTrack(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("addPlayTrack");

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }

    MPDQueue::add_id(getAccessItf()->getMPDConnector(), elements[1]);
}

/***********************************************************************//**
  @method : addPlaylist
  @comment: add play list to queue
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::addPlaylist(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("addPlaylist");

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }

    MPDQueue::load(getAccessItf()->getMPDConnector(), elements[1]);
}


/***********************************************************************//**
  @method : playTrack
  @comment: play track
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::playTrack(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("playTrack");

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }
    unsigned int track = utils::converter::strToInt(elements[1]);

    if(!MPDPlayer::runPlayId(getAccessItf()->getMPDConnector(), track)) {
        std::string result("failed");
        MpdWebResponse::emitError(*websocket, result);
    }
}

/***********************************************************************//**
  @method : saveQueue
  @comment: save queue to given name
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::saveQueue(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("saveQueue");

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }

    MPDPlayList::runSave(getAccessItf()->getMPDConnector(), elements[1]);
}

/***********************************************************************//**
  @method : deletePlaylist
  @comment: delete play list
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::deletePlaylist(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("deletePlaylist");

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }

    if(!MPDPlayList::deletePlaylist(getAccessItf()->getMPDConnector(), elements[1])) {
        std::string result("failed delete playlist");
        MpdWebResponse::emitError(*websocket, result);
    }
}

/***********************************************************************//**
  @method : rmTrack
  @comment: remove track
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::rmTrack(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("rmTrack");

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }

    unsigned int uint_buf = utils::converter::strToUInt(elements[1]);
    if (!MPDQueue::deleteID(getAccessItf()->getMPDConnector(), uint_buf)) {
        std::string result = "command failed";
        MpdWebResponse::emitError(*websocket, result);
    }
}

/***********************************************************************//**
  @method : rmAll
  @comment: remove all tracks
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::rmAll(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("rmAll");

    if (!MPDQueue::removeAll(getAccessItf()->getMPDConnector())) {
        std::string result = "command failed";
        MpdWebResponse::emitError(*websocket, result);
    }
}

/***********************************************************************//**
  @method : search
  @comment: search song
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::search(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("search");

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }

    std::vector<MPDSongInfo> vecInfo = MPDSong::search(getAccessItf()->getMPDConnector(), elements[1]);
    MpdWebResponse::emitSearch(*websocket, vecInfo);
}

/***********************************************************************//**
  @method : setVolume
  @comment: set volume
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::setVolume(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("setVolume");

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }
    unsigned int uint_buf = utils::converter::strToUInt(elements[1]);
    if (uint_buf > 100) uint_buf = 100;

    getAccessItf()->getSceneDescriptionItf()->setVolume(uint_buf);
}

/***********************************************************************//**
  @method : setPause
  @comment: set pause
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::setPause(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("setPause");

    if (!MPDPlayer::runTogglePause(getAccessItf()->getMPDConnector())) {
        std::string result("failed");
        MpdWebResponse::emitError(*websocket, result);
    }
}

/***********************************************************************//**
  @method : setPlay
  @comment: set play
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::setPlay(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("setPlay");

    if(!MPDPlayer::runPlay(getAccessItf()->getMPDConnector())) {
        std::string result("failed");
        MpdWebResponse::emitError(*websocket, result);
    }
}

/***********************************************************************//**
  @method : setStop
  @comment: set stop
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::setStop(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("setStop");

    if(!MPDPlayer::runStop(getAccessItf()->getMPDConnector())) {
        std::string result("failed");
        MpdWebResponse::emitError(*websocket, result);
    }
}

/***********************************************************************//**
  @method : setSeek
  @comment: set seek
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::setSeek(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("setSeek");

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 3) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }

    unsigned int songId = utils::converter::strToUInt(elements[1]);
    unsigned int par1 = utils::converter::strToUInt(elements[2]);

    if(!MPDPlayer::runSeekId(getAccessItf()->getMPDConnector(), songId, par1)) {
        std::string result("can not set position");
        MpdWebResponse::emitError(*websocket, result);
    }
}

/***********************************************************************//**
  @method : setNext
  @comment: set next song
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::setNext(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("setNext");

    if(!MPDPlayer::runNext(getAccessItf()->getMPDConnector())) {
        std::string result("failed");
        MpdWebResponse::emitError(*websocket, result);
    }
}

/***********************************************************************//**
  @method : setPrev
  @comment: set previou song
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::setPrev(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("setPrev");

    if(!MPDPlayer::runPrevious(getAccessItf()->getMPDConnector())) {
        std::string result("failed");
        MpdWebResponse::emitError(*websocket, result);
    }
}

// NO
void MpdController::setMpdHost(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("setMpdHost");

}

// NO
void MpdController::setMpdPass(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("setMpdPass");

}

/***********************************************************************//**
  @method : updateDb
  @comment: update database
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::updateDb(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("updateDb");

    std::string empty;
    if (!MPDDatabase::updateDB(getAccessItf()->getMPDConnector(), empty)) {
        std::string result("failed");
        MpdWebResponse::emitError(*websocket, result);
    }
}

/***********************************************************************//**
  @method : getOutputs
  @comment: get outputs
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::getOutputs(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("getOutputs:" << data);

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }

    bool outputNames = (strcmp(elements[1].c_str(),"outputnames") == 0);
    std::vector <MPDOut> output = MPDOutput::getOutputs(getAccessItf()->getMPDConnector());
    MpdWebResponse::emitOutput(*websocket, output, outputNames);
}

/***********************************************************************//**
  @method : toggleOutputs
  @comment: toggle outputs
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::toggleOutputs(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("toggleOutputs");

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 3) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }
    unsigned int uint_buf = utils::converter::strToUInt(elements[1]);
    unsigned int enable = utils::converter::strToUInt(elements[2]);

    if (enable) {
        if (!MPDOutput::enableOutput(getAccessItf()->getMPDConnector(), uint_buf)) {
            std::string result("failed");
            MpdWebResponse::emitError(*websocket, result);
        }
    } else {
        if (!MPDOutput::disableOutput(getAccessItf()->getMPDConnector(), uint_buf)) {
            std::string result("failed");
            MpdWebResponse::emitError(*websocket, result);
        }
    }
}

/***********************************************************************//**
  @method : toggleRandom
  @comment: toggle random
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::toggleRandom(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("toggleRandom");

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }

    unsigned int uint_buf = utils::converter::strToUInt(elements[1]);
    if (!MPDPlayer::runRandom(getAccessItf()->getMPDConnector(), uint_buf)) {
        std::string result("failed");
        MpdWebResponse::emitError(*websocket, result);
    }
}

/***********************************************************************//**
  @method : toggleConsume
  @comment: toggle consume
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::toggleConsume(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("toggleConsume");

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }

    unsigned int uint_buf = utils::converter::strToUInt(elements[1]);
    if (!MPDPlayer::runConsume(getAccessItf()->getMPDConnector(), uint_buf)) {
        std::string result("failed");
        MpdWebResponse::emitError(*websocket, result);
    }
}

/***********************************************************************//**
  @method : toggleSingle
  @comment: toggle single play
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::toggleSingle(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("toggleSingle");

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }

    unsigned int uint_buf = utils::converter::strToUInt(elements[1]);
    if (!MPDPlayer::runSingle(getAccessItf()->getMPDConnector(), uint_buf)) {
        std::string result("failed");
        MpdWebResponse::emitError(*websocket, result);
    }
}

/***********************************************************************//**
  @method : toggleCrossfade
  @comment: toggle crossfade
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::toggleCrossfade(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("toggleCrossfade");

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }

    unsigned int uint_buf = utils::converter::strToUInt(elements[1]);
    if (!MPDPlayer::runCrossfade(getAccessItf()->getMPDConnector(), uint_buf)) {
        std::string result("failed");
        MpdWebResponse::emitError(*websocket, result);
    }
}

/***********************************************************************//**
  @method : toggleRepeat
  @comment: toggle repeat
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::toggleRepeat(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("toggleRepeat");

    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 2) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }

    unsigned int uint_buf = utils::converter::strToUInt(elements[1]);
    if (!MPDPlayer::runRepeat(getAccessItf()->getMPDConnector(), uint_buf)) {
        std::string result("failed");
        MpdWebResponse::emitError(*websocket, result);
    }
}

/***********************************************************************//**
  @method : createPlaylist
  @comment: create playlist
  @param  : websocket
  @param  : data unfiltered query string
***************************************************************************/
void MpdController::createPlaylist(Mongoose::WebSocket* websocket, std::string data)
{
    DEBUG2("createPlaylist");
    std::vector<std::string> elements = StringHelper::split(data, ',');
    if (elements.size() < 3) {
        std::string result("input parameter fail");
        MpdWebResponse::emitError(*websocket, result);
        return;
    }
    if (!MPDPlayList::runPlaylistAdd(getAccessItf()->getMPDConnector(), elements[1], elements[2])) {
        std::string result("failed");
        MpdWebResponse::emitError(*websocket, result);
    }
}

/***********************************************************************//**
  @method : webSocketReady
  @comment: web socket ready
  @param  : websocket
***************************************************************************/
void MpdController::webSocketReady(WebSocket *websocket)
{
//    DEBUG2("webSocketReady: " << websocket->getRequest().getUrl());
}

/***********************************************************************//**
  @method : webSocketData
  @comment: dispatcher
  @param  : websocket
  @param  : data as identifier. If item found, call function
***************************************************************************/
void MpdController::webSocketData(WebSocket *websocket, std::string data)
{
    DEBUG1("[recv] " << data);

    auto it = m_functionMap.find(data);
    if (it != m_functionMap.end()) {
        it->second(websocket, data);
    } else {
        std::vector<std::string> x = StringHelper::split(data, ',');
        it = m_functionMap.find(x[0]);
        if (it != m_functionMap.end()) {
            it->second(websocket, data);
        } else {
            WARN("webSocketData 2nd attempt. Handler not found for " << data);
        }
    }
}
