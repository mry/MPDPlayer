/***************************************************************************
                          mpdwebresponse.h  -  description
                             -------------------
    begin                : Fri May 12 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MPDWEBRESPONSE_H
#define MPDWEBRESPONSE_H

#include "mpdwrapper/mpdconnector.h"
#include "mpdwrapper/mpdstatus.h"
#include "mpdwrapper/mpdoutput.h"
#include "mpdwrapper/mpdsong.h"
#include "mpdwrapper/mpddatabase.h"
#include <mongoose/Server.h>


class MpdWebResponse
{
public:
    MpdWebResponse() = delete;
    ~MpdWebResponse() = delete;

private:
    static std::string createErrorMessage(std::string& status);
    static std::string createOutput(std::vector <MPDOut>& output, bool outputnames);

public:
    static void emitError (Mongoose::WebSocket& websocket, std::string& status);
    static void emitError (Mongoose::WebSockets& websocket, std::string& status);
    static void emitStatus(Mongoose::WebSockets& sockets, MStatus& status);
    static void emitQueue (Mongoose::WebSocket& websocket, std::vector<MPDQueueData>& output);
    static void emitOutput(Mongoose::WebSocket& websocket, std::vector <MPDOut>& output, bool outputnames);
    static void emitOutput(Mongoose::WebSockets& websocket, std::vector <MPDOut>& output, bool outputnames);
    static void emitSearch(Mongoose::WebSocket& websocket, std::vector<MPDSongInfo>& vecInfo);
    static void emitBrowse(Mongoose::WebSocket& websocket, std::vector<MPDBrowseData>&  vecData,
                           unsigned int entity_count, std::string type);
    static void emitCurrentSong(Mongoose::WebSockets& websocket,
                          unsigned int position,
                          std::string& title,
                          std::string& artist,
                          std::string& album);
};

#endif // MPDWEBRESPONSE_H
