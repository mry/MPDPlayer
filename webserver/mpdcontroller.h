/***************************************************************************
                          mpdcontroller.h  -  description
                             -------------------
    begin                : Mon March 6 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MPDCONTROLLER_H
#define MPDCONTROLLER_H

#include "iwebcontroller.h"

#include <mongoose/WebController.h>
#include <mongoose/WebSockets.h>

#include "mpdwrapper/mpdoutput.h"
#include "mpdwrapper/mpdsong.h"
#include "mpdwrapper/mpddatabase.h"

class WebServer;


class MpdController :
    public IWebController
{
public:
    MpdController(IAccess* access);
    virtual ~MpdController() = default;

    virtual void setup();
    virtual void webSocketReady(Mongoose::WebSocket *websocket);
    virtual void webSocketData(Mongoose::WebSocket *websocket, std::string data);

private:
    typedef std::function<void(Mongoose::WebSocket* websocket, std::string data)> Function;

    void addWebsocketRoute(std::string data, Function func);

    void getQueue(Mongoose::WebSocket* websocket, std::string data);
    void getBrowse(Mongoose::WebSocket* websocket, std::string data);
    void getBrowsePlaylist(Mongoose::WebSocket* websocket, std::string data);
    void getMpdhost(Mongoose::WebSocket* websocket, std::string data);
    void addTrack(Mongoose::WebSocket* websocket, std::string data);
    void addPlayTrack(Mongoose::WebSocket* websocket, std::string data);
    void addPlaylist(Mongoose::WebSocket* websocket, std::string data);
    void playTrack(Mongoose::WebSocket* websocket, std::string data);
    void saveQueue(Mongoose::WebSocket* websocket, std::string data);
    void deletePlaylist(Mongoose::WebSocket* websocket, std::string data);
    void rmTrack(Mongoose::WebSocket* websocket, std::string data);
    void rmAll(Mongoose::WebSocket* websocket, std::string data);
    void search(Mongoose::WebSocket* websocket, std::string data);
    void setVolume(Mongoose::WebSocket* websocket, std::string data);
    void setPause(Mongoose::WebSocket* websocket, std::string data);
    void setPlay(Mongoose::WebSocket* websocket, std::string data);
    void setStop(Mongoose::WebSocket* websocket, std::string data);
    void setSeek(Mongoose::WebSocket* websocket, std::string data);
    void setNext(Mongoose::WebSocket* websocket, std::string data);
    void setPrev(Mongoose::WebSocket* websocket, std::string data);
    void setMpdHost(Mongoose::WebSocket* websocket, std::string data);
    void setMpdPass(Mongoose::WebSocket* websocket, std::string data);
    void updateDb(Mongoose::WebSocket* websocket, std::string data);
    void getOutputs(Mongoose::WebSocket* websocket, std::string data);
    void toggleOutputs(Mongoose::WebSocket* websocket, std::string data);
    void toggleRandom(Mongoose::WebSocket* websocket, std::string data);
    void toggleConsume(Mongoose::WebSocket* websocket, std::string data);
    void toggleSingle(Mongoose::WebSocket* websocket, std::string data);
    void toggleCrossfade(Mongoose::WebSocket* websocket, std::string data);
    void toggleRepeat(Mongoose::WebSocket* websocket, std::string data);

    void createPlaylist(Mongoose::WebSocket* websocket, std::string data);

private:
    std::map<std::string, Function> m_functionMap;
};

#endif // MPDCONTROLLER_H
