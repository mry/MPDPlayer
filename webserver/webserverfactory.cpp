/***************************************************************************
                          webserverfactory.cpp  -  description
                             -------------------
    begin                : Sat April 8 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "webserverfactory.h"
#include <memory>

/***********************************************************************//**
  @method :  WebServerFactory
  @comment:  constructor. factory for webserver.
  @param  :  sceneDesc
  @param  :  documentRoot
***************************************************************************/
WebServerFactory::WebServerFactory(boost::shared_ptr<SceneDescriptionItf> sceneDesc,
                                   std::string& documentRoot) :
    m_sceneDescription(sceneDesc),
    m_server (documentRoot, sceneDesc)
{
    initialize();
}

/***********************************************************************//**
  @method :  initialize
  @comment:  initialize factory
***************************************************************************/
void WebServerFactory::initialize()
{
    m_sceneController   = std::make_shared<SceneController>(&m_server);
    m_volumeController  = std::make_shared<VolumeController>(&m_server);
    m_channelController = std::make_shared<ChannelController>(&m_server);
    m_powerController   = std::make_shared<PowerController>(&m_server);
    m_mpdController     = std::make_shared<MpdController>(&m_server);

    m_server.registerWebControllerItf(m_sceneController);
    m_server.registerWebControllerItf(m_volumeController);
    m_server.registerWebControllerItf(m_channelController);
    m_server.registerWebControllerItf(m_powerController);
    m_server.registerWebControllerItf(m_mpdController);
}

/***********************************************************************//**
  @method :  start
  @comment:  start thread
***************************************************************************/
void WebServerFactory::start()
{
    m_server.StartThread();
}

/***********************************************************************//**
  @method :  stop
  @comment:  stop thread
***************************************************************************/
void WebServerFactory::stop()
{
    m_server.StopThread();
}
