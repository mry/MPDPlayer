/***************************************************************************
                          channelcontroller.h  -  description
                             -------------------
    begin                : Thu Oct 13 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef CHANNELCONTROLLER_H
#define CHANNELCONTROLLER_H

#include "iwebcontroller.h"


class ChannelController :
    public IWebController
{
public:
    ChannelController(IAccess* access);
    virtual ~ChannelController() = default;
    virtual void setup();
    void setChannel(Mongoose::Request &request, Mongoose::StreamResponse &response);
    void getChannel(Mongoose::Request &request, Mongoose::StreamResponse &response);
};

#endif // CHANNELCONTROLLER_H
