/***************************************************************************
                          scenecontroller.cpp  -  description
                             -------------------
    begin                : Fri Oct 7 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "iaccess.h"
#include "scenecontroller.h"

#include<iostream>
#include<string>
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"

#include "rfw/utils/clogger.h"
#include "rfw/utils/cconverter.h"

using namespace rapidjson;
using namespace Mongoose;

/***********************************************************************//**
  @method : SceneController
  @comment: constructor
  @oaram  : access interface
***************************************************************************/
SceneController::SceneController(IAccess* access):
    IWebController(access)
{
}

/***********************************************************************//**
  @method : setup
  @comment: register handlers
***************************************************************************/
void SceneController::setup()
{
    addRoute("GET", "/setSceneValues", SceneController, setSceneValues);
    addRoute("GET", "/getSceneValues", SceneController, getSceneValues);
    addRoute("GET", "/setMPDSceneValues", SceneController, setSceneMPDValues);
    addRoute("GET", "/getMPDSceneValues", SceneController, getSceneMPDValues);
    addRoute("GET", "/setActiveScene", SceneController, setActiveScene);
    addRoute("GET", "/getAllScenes",   SceneController, getAllScenes);
    addRoute("GET", "/getActiveScene", SceneController, getActiveScene);
}

/***********************************************************************//**
  @method : setSceneValues
  @comment: handler: e.g. http://127.0.0.1:8090/setSceneValues?scene=2&volume=20&channel=0&power=2
  @param  : request
  @param  : response
***************************************************************************/
void SceneController::setSceneValues(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    std::string scene   = request.get("scene", "-1");
    std::string volume  = request.get("volume", "0");
    std::string channel = request.get("channel", "0");
    std::string power   = request.get("power", "0");

    int numvolume =   utils::converter::strToIntDef(volume, 0);
    int numscene =    utils::converter::strToIntDef(scene, -1);
    int numchannel =  utils::converter::strToIntDef(channel, 0);
    int numpower =    utils::converter::strToIntDef(power, 0);

    INFO("scene:" << numscene <<
         " volume:" << numvolume <<
         " channel:" << numchannel <<
         " power:" << numpower);

    getAccessItf()->getSceneDescriptionItf()->setScene(numscene, numvolume, numchannel, numpower);

    SceneDescription desc = getAccessItf()->getSceneDescriptionItf()->getScene(numscene);
    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("scene");
    writer.Uint(numscene);
    writer.Key("volume");
    writer.Uint(desc.m_volume);
    writer.Key("power");
    writer.Uint(desc.m_power);
    writer.Key("channel");
    writer.Uint(desc.m_channel);
    writer.EndObject();
    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : getSceneValues
  @comment: handler: e.g. http://127.0.0.1:8090/getSceneValues?scene=2
  @param  : request
  @param  : response
***************************************************************************/
void SceneController::getSceneValues(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    std::string scene  = request.get("scene", "-1");
    int numscene = utils::converter::strToIntDef(scene, -1);
    SceneDescription desc = getAccessItf()->getSceneDescriptionItf()->getScene(numscene);

    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("scene");
    writer.Uint(numscene);
    writer.Key("volume");
    writer.Uint(desc.m_volume);
    writer.Key("power");
    writer.Uint(desc.m_power);
    writer.Key("channel");
    writer.Uint(desc.m_channel);
    writer.EndObject();

    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : setSceneMPDValues
  @comment: handler: e.g. http://127.0.0.1:8090/setMPDSceneValues?scene=2&playlist="hello"&song="test"&command=2
  @param  : request
  @param  : response
***************************************************************************/
void SceneController::setSceneMPDValues(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    std::string scene     = request.get("scene", "-1");
    std::string playlist  = request.get("playlist", "0");
    std::string song      = request.get("song", "0");
    std::string command   = request.get("command", "0");

    int numscene          = utils::converter::strToIntDef(scene, -1);
    int numCommand        = utils::converter::strToIntDef(command, 0);

    INFO("scene:"     << numscene <<
         " command:"  << numCommand <<
         " playlist:" << playlist <<
         " song:"     << song);

    getAccessItf()->getSceneDescriptionItf()->setPlayerScene(numscene, numCommand, playlist, song);

    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("scene");
    writer.Uint(numscene);
    writer.Key("command");
    writer.Uint(numCommand);
    writer.Key("playlist");
    writer.String(playlist.c_str());
    writer.Key("song");
    writer.String(song.c_str());
    writer.EndObject();

    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : getSceneMPDValues
  @comment: handler: e.g. http://127.0.0.1:8090/getMPDSceneValues?scene=2
  @param  : request
  @param  : response
***************************************************************************/
void SceneController::getSceneMPDValues(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    std::string scene  = request.get("scene", "-1");
    int numscene = utils::converter::strToIntDef(scene, -1);

    SceneDescription desc = getAccessItf()->getSceneDescriptionItf()->getScene(numscene);

    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("scene");
    writer.Int(numscene);
    writer.Key("command");
    writer.Uint(desc.m_mpdCommand);
    writer.Key("playlist");
    writer.String(desc.m_playlist.c_str());
    writer.Key("song");
    writer.String(desc.m_song.c_str());
    writer.EndObject();

    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : getAllScenes
  @comment: handler: e.g. http://127.0.0.1:8090/getAllScenes
  @param  : request
  @param  : response
***************************************************************************/
void SceneController::getAllScenes(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    std::vector<SceneDescription> desc = getAccessItf()->getSceneDescriptionItf()->getAllScenes();

    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("scenes");

    writer.StartArray();
    for (utils::uint32_t i = 0; i < desc.size(); ++i) {
        writer.StartObject();

        writer.Key("scene");
        writer.Uint(i);

        writer.Key("volume");
        writer.Int(desc[i].m_volume);

        writer.Key("power");
        writer.Uint(desc[i].m_power);

        writer.Key("channel");
        writer.Uint(desc[i].m_channel);

        writer.Key("command");
        writer.Uint(desc[i].m_mpdCommand);

        writer.Key("playlist");
        writer.String(desc[i].m_playlist.c_str());

        writer.Key("song");
        writer.String(desc[i].m_song.c_str());

        writer.EndObject();
    }

    writer.EndArray();
    writer.EndObject();

    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : getActiveScene
  @comment: handler: e.g. http://127.0.0.1:8090/getActiveScene
  @param  : request
  @param  : response
***************************************************************************/
void SceneController::getActiveScene(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    utils::int32_t numscene = getAccessItf()->getSceneDescriptionItf()->getActiveScene();
    SceneDescription desc = getAccessItf()->getSceneDescriptionItf()->getScene(numscene);

    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("scene");
    writer.Uint(numscene);
    writer.Key("volume");
    writer.Uint(desc.m_volume);
    writer.Key("power");
    writer.Uint(desc.m_power);
    writer.Key("channel");
    writer.Uint(desc.m_channel);
    writer.EndObject();

    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : setActiveScene
  @comment: handler: e.g. http://127.0.0.1:8090/setActiveScene?scene=2
  @param  : request
  @param  : response
***************************************************************************/
void SceneController::setActiveScene(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    std::string scene  = request.get("scene", "-1");
    int numscene = utils::converter::strToIntDef(scene, -1);
    getAccessItf()->getSceneDescriptionItf()->setActiveScene(numscene);

    SceneDescription desc = getAccessItf()->getSceneDescriptionItf()->getScene(numscene);
    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("scene");
    writer.Uint(numscene);
    writer.Key("volume");
    writer.Uint(desc.m_volume);
    writer.Key("power");
    writer.Uint(desc.m_power);
    writer.Key("channel");
    writer.Uint(desc.m_channel);
    writer.EndObject();

    response << s.GetString() << std::endl;
}
