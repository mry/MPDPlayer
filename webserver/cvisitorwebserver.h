/***************************************************************************
                          cvisitorwebserver.h  -  description
                             -------------------
    begin                : Thu April 11 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CVISITORWEBSERVER_H
#define CVISITORWEBSERVER_H

#include "rfw/utils/ctypes.h"
#include "extensions/cvisitor.h"
#include "iaccess.h"

class CVisitorWebserver :
    public CVisitor
{
public:
    CVisitorWebserver(IAccess* access);
    virtual ~CVisitorWebserver() = default;
    virtual void visitMPDMessage(CMPDMessage* pMsg);

private:
    IAccess* m_access;
};

#endif // CVISITORWEBSERVER_H
